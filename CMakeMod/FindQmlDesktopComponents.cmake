## this is a simple script that try find Qml Desktop Components
## in Qml is used "import QtDesktop 0.1"

## source codes at: http://qt.gitorious.org/qt-components/desktop

# Find dependencies, if not already found
if (NOT DEFINED QT_INCLUDE_DIR)
	message(STATUS "Qt hasn't been found yet. Looking...")
	find_package(Qt4 COMPONENTS QtCore QtGui QtDeclarative REQUIRED)
endif()

if (QT_QTDECLARATIVE_FOUND AND ${QTVERSION} VERSION_GREATER 4.7.2)
	#message(SYSTEM " QT_IMPORTS_DIR: ${QT_IMPORTS_DIR}")
	find_path(QML_DESKTOP_COMPONENTS_PATH QtDesktop
			PATHS ${QT_IMPORTS_DIR}
			NO_DEFAULT_PATH)

	if (QML_DESKTOP_COMPONENTS_PATH)
		message(SYSTEM " Qml Desktop Components found")
		set(QML_DESKTOP_COMPONENTS ON)
	else()
		IF (QmlDesktopComponents_FIND_REQUIRED)
			message(SEND_ERROR "Qml Desktop Components not found! Its sources are at http://qt.gitorious.org/qt-components/desktop")
		ELSE (QmlDesktopComponents_FIND_REQUIRED)
			message(STATUS "Qml Desktop Components not found! Its sources are at http://qt.gitorious.org/qt-components/desktop")
		ENDIF(QmlDesktopComponents_FIND_REQUIRED)
    endif()
endif()
