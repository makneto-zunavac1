## this is a simple script that try find QtQst-QmlSink

## source codes at: http://cgit.collabora.com/git/qtgst-qmlsink.git/

# Find dependencies, if not already found
if (NOT DEFINED QT_INCLUDE_DIR)
	message(STATUS "Qt hasn't been found yet. Looking...")
	find_package(Qt4 COMPONENTS QtCore QtGui QtDeclarative REQUIRED)
endif()

#if (QT_QTDECLARATIVE_FOUND AND ${QTVERSION} VERSION_GREATER 4.7.2)
	#message(SYSTEM " QT_IMPORTS_DIR: ${QT_IMPORTS_DIR}")
	
	# Find the main library
	#find_package(PkgConfig)
	
	PKG_CHECK_MODULES(QMLSINK REQUIRED qt-gst-qml-sink)
	# --   found qt-gst-qml-sink, version 0.0.1
	
	#message(STATUS "var ${QMLSINK_CFLAGS_OTHER}")
	#message(STATUS "var ${QMLSINK_LIBRARIES}")
	#message(STATUS "var ${QMLSINK_FOUND}")
	
	if (QMLSINK_FOUND)
		message(SYSTEM " QtQst-QmlSink found")
		set(QT_GST_QML_SINK ON)
	else()
		message(SEND_ERROR "QtQst-QmlSink not found! Its sources are at git://git.collabora.co.uk/git/qtgst-qmlsink.git")
    endif()
#endif()
