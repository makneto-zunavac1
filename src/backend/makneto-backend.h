/* 
 * File:   makneto-backend.h
 * Author: karry
 *
 * Created on 23. říjen 2011, 21:04
 */

#ifndef MAKNETO_BACKEND_H
#define	MAKNETO_BACKEND_H

// MAKNETO_LIB is defined id CmakeList.txt when we build makneto library
#ifdef MAKNETO_LIB
# define MAKNETO_EXPORT Q_DECL_EXPORT
#else
# define MAKNETO_EXPORT Q_DECL_IMPORT
#endif

#endif	/* MAKNETO_BACKEND_H */

