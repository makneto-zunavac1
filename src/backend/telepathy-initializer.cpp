/*
 * Telepathy initializer initializes telepathy and is inspired by
 * TelepathyQt4Yell project
 *
 * Copyright (C) 2011 Vojta Kulicka <vojtechkulicka@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <TelepathyQt4/Types>
#include <TelepathyQt4/Global>
#include <TelepathyQt4/Constants>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/ContactManager>
#include <TelepathyQt4/PendingOperation>
#include <TelepathyQt4/PendingReady>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/StreamedMediaChannel>
#include <TelepathyQt4/FileTransferChannel>
#include <TelepathyQt4/ChannelClassSpecList>

#include "telepathy-client.h"
#include "telepathy-initializer.h"
#include "accounts-model.h"
#include <TelepathyQt4/IncomingFileTransferChannel>
#include <TelepathyQt4/OutgoingFileTransferChannel>

namespace MaknetoBackend {

  TelepathyInitializer::TelepathyInitializer(QObject *parent)
  : QObject(parent),
  m_connections(0) {

    Tp::registerTypes();
    Tp::enableDebug(false);
    Tp::enableWarnings(true);
  }

  void TelepathyInitializer::createClient(QString name) {

    m_clientName = name;

    // check QDBus connection
    if (!QDBusConnection::sessionBus().isConnected())
      qWarning() << "TelepathyInitializer: session bus is not connected!" << QDBusConnection::sessionBus().lastError();

    //create AccountManager
    Tp::Features connFeatures;
    Tp::Features accountFeatures;

    //account factory
    Tp::AccountFactoryPtr accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
      Tp::Features()
      << Tp::Account::FeatureCore
      << Tp::Account::FeatureAvatar
      << Tp::Account::FeatureCapabilities
      );

    //connection factory
    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(
      QDBusConnection::sessionBus(),
      Tp::Features() << Tp::Connection::FeatureSelfContact
      << Tp::Connection::FeatureCore
      << Tp::Connection::FeatureSimplePresence
      << Tp::Connection::FeatureRoster
      << Tp::Connection::FeatureRosterGroups
      );

    //channel factory
    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());

    Tp::Features textFeatures = Tp::Features() << Tp::TextChannel::FeatureCore
      << Tp::TextChannel::FeatureMessageQueue
      << Tp::TextChannel::FeatureMessageSentSignal
      << Tp::TextChannel::FeatureChatState
      << Tp::TextChannel::FeatureMessageCapabilities;

    Tp::Features callFeatures = Tp::Features() << Tp::StreamedMediaChannel::FeatureCore
      << Tp::StreamedMediaChannel::FeatureStreams;

    Tp::Features incomingFileTransferFeatures = Tp::Features() << Tp::IncomingFileTransferChannel::FeatureCore;
    Tp::Features outgoingFileTransferFeatures = Tp::Features() << Tp::OutgoingFileTransferChannel::FeatureCore;

    channelFactory->addFeaturesForTextChats(textFeatures);
    channelFactory->addFeaturesForTextChatrooms(textFeatures);
    channelFactory->addFeaturesForStreamedMediaCalls(callFeatures);
    channelFactory->addFeaturesForIncomingFileTransfers(incomingFileTransferFeatures);
    channelFactory->addFeaturesForOutgoingFileTransfers(outgoingFileTransferFeatures);

    //contact factory	
    Tp::ContactFactoryPtr contactFactory = Tp::ContactFactory::create(
      Tp::Features() << Tp::Contact::FeatureAlias
      << Tp::Contact::FeatureAvatarData
      << Tp::Contact::FeatureCapabilities
      << Tp::Contact::FeatureSimplePresence);

    m_accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
      accountFactory,
      connectionFactory,
      channelFactory,
      contactFactory);

    connect(m_accountManager->becomeReady(),
      SIGNAL(finished(Tp::PendingOperation*)),
      SLOT(onAccountManagerReady(Tp::PendingOperation*)));
  }

  void TelepathyInitializer::onAccountManagerReady(Tp::PendingOperation *op) {
    if (op->isError()) {
      qWarning() << "TelepathyInitializer: Error occured making AccountManager ready" << op->errorName() << op->errorMessage();
      return;
    }

    foreach(Tp::AccountPtr account, m_accountManager->allAccounts()) {

      if (!account->isEnabled())
        continue;

      connect(account->becomeReady(),
        SIGNAL(finished(Tp::PendingOperation *)),
        SLOT(onAccountReady(Tp::PendingOperation*)));

      m_connections++;
    }

  }

  void TelepathyInitializer::onAccountReady(Tp::PendingOperation *op) {
    if (op->isError()) {
      qWarning() << "TelepathyInitializer: Error occured making Account ready";
      return;
    }

    Tp::PendingReady *pr = qobject_cast<Tp::PendingReady *>(op);
    Tp::AccountPtr account = Tp::AccountPtr::dynamicCast(pr->object());

    if (!account->connection().isNull()) {

      connect(account->connection()->becomeReady(),
        SIGNAL(finished(Tp::PendingOperation *)),
        SLOT(onConnectionReady(Tp::PendingOperation *)));
    } else {
      m_connections--;
      checkFinished();
    }
    qDebug() << "TelepathyInitializer: Account ready: " << account->uniqueIdentifier();
  }

  void TelepathyInitializer::onConnectionReady(Tp::PendingOperation *op) {
    if (op->isError()) {
      qWarning() << "TelepathyInitializer: Error occured making Connection ready";
      return;
    }

    Tp::PendingReady *pr = qobject_cast<Tp::PendingReady *>(op);
    Tp::ConnectionPtr connection = Tp::ConnectionPtr::dynamicCast(pr->object());

    Q_ASSERT(connection);

    m_connections--;
    checkFinished();
  }

  void TelepathyInitializer::checkFinished() {
    qDebug() << "TelepathyInitializer: Connections ready: " << m_connections;
    if (m_connections > 0)
      return;

    //create accounts model
    AccountsModel *accountsModel = new AccountsModel(m_accountManager);

    //Create client
    //ChannelClassSpec says what channel is the client interested in and is looked at by channel dispatcher 
    TelepathyClient *client = TelepathyClient::Instance(Tp::ChannelClassSpecList()
      << Tp::ChannelClassSpec::textChat()
      << Tp::ChannelClassSpec::unnamedTextChat()
      << Tp::ChannelClassSpec::textChatroom()
      << Tp::ChannelClassSpec::streamedMediaCall()
      << Tp::ChannelClassSpec::streamedMediaAudioCall()
      << Tp::ChannelClassSpec::incomingFileTransfer()
      << Tp::ChannelClassSpec::outgoingFileTransfer()
      << Tp::ChannelClassSpec::streamedMediaVideoCall()
      << Tp::ChannelClassSpec::streamedMediaVideoCallWithAudio(),
      m_accountManager,
      accountsModel,
      m_clientName,
      this
      );

    //register client
    m_cr = Tp::ClientRegistrar::create(m_accountManager);
    Tp::AbstractClientPtr handler(client);
    m_cr->registerClient(handler, m_clientName);

    //create account model and set it to the client
    qDebug() << "TelepathyInitializer: Finished";
    emit finished(client);
  }

}
#include "telepathy-initializer.moc"
