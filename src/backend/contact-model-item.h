/*
 * Contacts model item, represents a contact in the contactlist tree
 * This file is based on TelepathyQt4Yell Models
 *
 * Copyright (C) 2010 Collabora Ltd. <http://www.collabora.co.uk/>
 * Copyright (C) 2011 Martin Klapetek <martin dot klapetek at gmail dot com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef TELEPATHY_CONTACT_MODEL_ITEM_H
#define TELEPATHY_CONTACT_MODEL_ITEM_H

#include <TelepathyQt4/Types>
#include <TelepathyQt4/Account>
#include <QtCore/QVariant> //needed for declare metatype

#include "tree-node.h"
#include "makneto-backend.h"


namespace MaknetoBackend {

	class MAKNETO_EXPORT ContactModelItem : public TreeNode
	{
		Q_OBJECT
		Q_DISABLE_COPY(ContactModelItem)

	public:
		ContactModelItem(const Tp::ContactPtr &contact, const Tp::AccountPtr &account);
		virtual ~ContactModelItem();

		Q_INVOKABLE virtual QVariant data(int role) const;
		Q_INVOKABLE virtual bool setData(int role, const QVariant &value);

		Tp::ContactPtr contact() const;

	public Q_SLOTS:
		void onChanged();

	private:
		struct Private;
		friend struct Private;
		Private *mPriv;
	};

	
}

Q_DECLARE_METATYPE(MaknetoBackend::ContactModelItem*);


#endif // TELEPATHY_CONTACT_MODEL_ITEM_H
