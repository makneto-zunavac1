/*
 * Telepathy client is a class for communication via Telepathy
 *
 * Copyright (C) 2011 Vojta Kulicka <vojtechkulicka@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <TelepathyQt4/ChannelClassSpecList>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/ContactManager>
#include <TelepathyQt4/Contact>
#include <TelepathyQt4/PendingReady>
#include <TelepathyQt4/PendingContacts>
#include <TelepathyQt4/PendingOperation>
#include <TelepathyQt4/PendingChannel>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/StreamedMediaChannel>
#include <TelepathyQt4/FileTransferChannel>
#include <TelepathyQt4/Message>

#include <QSet>
#include <QString>

#include "telepathy-client.h"
#include "accounts-model.h"
#include "session.h"


namespace MaknetoBackend {

  TelepathyClient* TelepathyClient::m_instance = NULL;

  TelepathyClient* TelepathyClient::Instance() {
    return m_instance;
  }

  TelepathyClient* TelepathyClient::Instance(const Tp::ChannelClassSpecList &channelFilter, Tp::AccountManagerPtr AM, AccountsModel *accountsModel, const QString &clientName, QObject *parent) {
    if (!m_instance) {
      m_instance = new TelepathyClient(channelFilter, AM, accountsModel, clientName, parent);
    }
    return m_instance;
  }

  TelepathyClient::TelepathyClient(const Tp::ChannelClassSpecList &channelFilter, Tp::AccountManagerPtr AM, AccountsModel *accountsModel, const QString &clientName, QObject *parent)
  : QObject(parent),
  AbstractClientHandler(channelFilter),
  m_AM(AM),
  m_accountsModel(accountsModel),
  m_sessions(new QMap<QString, Session *>()),
  clientName(clientName){
    
    qDebug() << "TelepathyClient: registered with preferred handler " << getPreferredHandler();
  }

  TelepathyClient::~TelepathyClient() {
    delete m_sessions;
    deleteLater();
  }

  //HANDLER CLIENT CODE START

  bool TelepathyClient::bypassApproval() const {
    return false;
  }

  void TelepathyClient::handleChannels(
    const Tp::MethodInvocationContextPtr<> & context,
    const Tp::AccountPtr & account,
    const Tp::ConnectionPtr & connection,
    const QList< Tp::ChannelPtr > & channels,
    const QList< Tp::ChannelRequestPtr > & requestsSatisfied,
    const QDateTime & userActionTime,
    const Tp::AbstractClientHandler::HandlerInfo & handlerInfo) {

    Q_UNUSED(connection);
    Q_UNUSED(requestsSatisfied);
    Q_UNUSED(userActionTime);
    Q_UNUSED(handlerInfo);

    Tp::TextChannelPtr textChannel;
    Tp::StreamedMediaChannelPtr streamedMediaChannel;
    Tp::FileTransferChannelPtr fileTransferChannel;
    //Tp::ContactPtr contact;
    Session *session;
    QString sessionName = QString();
    bool sessionExists = false;
    bool isMUC = false;

    qDebug() << "TelepathyClient: Handling channels";
    /* checking if the session already exists is important in case that the users already chat via text
     * channel and one asks for audio channel. In that case we look at existing session by name which is
     * users id and if such session exists we add the channel type to that session. We do not have to 
     * check if the incoming channel is of different type than the one already in session for we would 
     * not get it from channel dispatcher
     */
    
    /* FIXME: 
     *  - in many method we expect that we have text channel always. but this is wrong. we can get incoming 
     *    media channel without previous text channel
     *  - we don't wait while new text channel is ready and don't check if some errors occurs while channel initialization. 
     *    this is bad! this can produce unexpected results and weird bugs
     */

    foreach(const Tp::ChannelPtr & channel, channels) {

      //the channel pointer that is not null is the type of channel that came.
      //No other type of channel can come than the ones defined by ChannelClassSpec
      //upon registration of the handler
      textChannel = Tp::TextChannelPtr::dynamicCast(channel);
      streamedMediaChannel = Tp::StreamedMediaChannelPtr::dynamicCast(channel);
      fileTransferChannel = Tp::FileTransferChannelPtr::dynamicCast(channel);

      //See if it is a MUC or one-to-one channel and who is the channel to
      uint targetHandle;
      Tp::ContactPtr contact;
      if (channel->targetHandleType() == Tp::HandleTypeContact) {

        targetHandle = channel->targetHandle();
        contact = account->connection()->contactManager()->lookupContactByHandle(targetHandle);
        if (contact)
          sessionName = Session::sessionName(account, contact);
        else
          sessionName = Session::sessionNameForChannel(account, channel);
        qDebug() << "TelepathyClient: session name for contact"<< (contact? contact->id(): "NULL") << ":"<<sessionName;
        Q_ASSERT(sessionName != QString());
      } else if (channel->targetHandleType() == Tp::HandleTypeRoom) {

        qDebug() << "TelepathyClient: Incoming channel is an MUC.";
        targetHandle = channel->targetHandle();
        isMUC = true;
        sessionName = Session::sessionName(account, contact);
      }
      qDebug() << "TelepathyClient: Channel to: " << channel->targetId();

      //check if session with the contact already exists
      if (m_sessions->contains(sessionName)) {
        sessionExists = true;
        session = m_sessions->value(sessionName);
      }

      //Depending of what kind of channel it is and whether it exists or not perform appropriate action
      //text chat and chatroom
      if (textChannel) {
        qDebug() << "TelepathyClient: Text channel came";
        if (sessionExists) {
          //TODO see if the channel is not already set
          emit sessionCreated(session, true, false);
          isMUC ? session->onTextChannelReady(textChannel, true)
            : session->onTextChannelReady(textChannel, false);
        } else {
          qDebug() << "TelepathyClient: Creating new session with text.";
          isMUC ? session = new Session(textChannel, Session::SessionTypeMuc, this)
            : session = new Session(textChannel, Session::SessionTypeText, this);
          m_sessions->insert(sessionName, session);
          emit sessionCreated(session, false, false);
          isMUC ? session->onTextChannelReady(textChannel, true)
            : session->onTextChannelReady(textChannel, false);
        }
      }//Audio and video calls 
      else if (streamedMediaChannel) {

        qDebug() << "TelepathyClient: Media channel came";
        qDebug() << "TelepathyClient: streamChannel is valid: " << streamedMediaChannel->isValid()
          << "   is ready: " << streamedMediaChannel->isReady(Tp::Features() << Tp::StreamedMediaChannel::FeatureCore << Tp::StreamedMediaChannel::FeatureStreams);
        qDebug() << "TelepathyClient: streams: " << streamedMediaChannel->streams().size();

        // get session type from available streams
        Session::SessionType sessionType = Session::SessionTypeNone;
        foreach(const Tp::StreamedMediaStreamPtr &stream,  streamedMediaChannel->streams()){
          qDebug() << "   stream" << stream->id() << ":" << stream->objectName() << "type" << (stream->type() == Tp::MediaStreamTypeVideo? "video" : "audio");
          
          if (sessionType != Session::SessionTypeVideo){ // only one stream can be video and whole session is video type...
            sessionType = ((int)stream->type()) == ((int)Tp::MediaStreamTypeVideo) ? 
              Session::SessionTypeVideo : Session::SessionTypeAudio;
          }
        }
        if (streamedMediaChannel->streams().size() == 0){
          qWarning() << "TelepathyClient: Something is wrong. We don't have streams in this channel. Don't create Session.";
        }else{
          if (sessionExists) {
            emit sessionCreated(session, true, false);
            session->onIncomingCallChannel(streamedMediaChannel, sessionType);
          } else {
            //TODO here should be a check what streams does the incoming call carry // TODO remove this todo :)
            session = new Session(streamedMediaChannel, sessionType, this);
            m_sessions->insert(sessionName, session);
            emit sessionCreated(session, false, false);
            session->onIncomingCallChannel(streamedMediaChannel, sessionType);
          }
        }
      } else if (fileTransferChannel) {

        qDebug() << "TelepathyClient: File transfer came";
        qDebug() << "TelepathyClient: File transfer is valid: " << fileTransferChannel->isValid()
          << "   is ready: " << fileTransferChannel->isReady(Tp::FileTransferChannel::FeatureCore);

        if (sessionExists) {
          emit sessionCreated(session, true, false);
          session->onFileTransfer(fileTransferChannel);
        } else { //TODO this cas easily be changed
          qWarning() << "TelepathyClient: No existing session with the contact from whom file transfer came, ignoring";
        }
      }
    }

    qDebug() << "TelepathyClient: Handling incoming channels from account: " << account->nickname() << " has finished.";

    context->setFinished();
  }

  //HANDLER CLIENT CODE END 

  void TelepathyClient::onSessionRequested(const QModelIndex& contactItem, MaknetoBackend::Session::SessionType chatType) {
    qDebug() << "TelepathyClient: New session requested";

    QString sessionName = sessionIdforIndex(contactItem);
    Session *session = findSession(sessionName);
    //session exists check if the requested chatType is already set or not
    if (session) {
      //If the flag is not set, add the capability
      if (!session->getSessionType().testFlag(chatType)) {
        session->onAddCapability(chatType);
      }
      emit sessionCreated(session, true, true);
    }//session does not exist, create new one
    else {
      //create new session, which starts whatever type of communication based on chatType. Once the channel for that is ready it will 
      //cause handleChannels method to be invoked and the GUI will be sent signal the session is ready
      session = new Session(m_accountsModel->accountForContactIndex(contactItem), m_accountsModel->contactForIndex(contactItem), chatType); // FIXME: howto get MUC name here?
      m_sessions->insert(session->getUniqueName(), session);
      emit sessionCreated(session, false, true);
    }
  }

  void TelepathyClient::onSessionRequested(const QString &accountId, const QString &contactId, MaknetoBackend::Session::SessionType sessionType){
    QModelIndex accountIndex = m_accountsModel->contactIndexForId(accountId, contactId);
    if (!accountIndex.isValid()){
      qWarning() << "TelepathyClient::onSessionRequested: coudl not found contact"<< contactId << "for account" << accountId;
      return;
    }
    onSessionRequested(accountIndex, sessionType);
  }
  
  void TelepathyClient::onMediaChannelReady(Tp::PendingOperation*) {

  }

  AccountsModel* TelepathyClient::accountsModel() {

    return m_accountsModel;
  }

  Tp::AccountManagerPtr TelepathyClient::accountManager() {

    return m_AM;
  }

  //TODO probably delete - deprecated
  void TelepathyClient::onNewTextChatRoomRequested(const QModelIndex &accountIndex, const QString &chatRoomName) {

    //see if the session already exists
    Session *session = findSession(chatRoomName);
    if (session) {
      emit sessionAlreadyExists(session);
    }//session does not exist create a text chatroom channel
    else {
      m_accountsModel->accountForIndex(accountIndex)->ensureTextChatroom(chatRoomName,
        QDateTime::currentDateTime(),
        getPreferredHandler());
    }
  }

  QString TelepathyClient::getPreferredHandler() {
    return "org.freedesktop.Telepathy.Client." + clientName;
  }

  void TelepathyClient::setAccountPresence(QModelIndex accountIndex, int presence, const QString &statusMsg) {
    //TODO handle statusMsg
    Tp::AccountPtr account = m_accountsModel->accountForIndex(accountIndex);

    //TODO connect to pending operation to be able to signal an unsuccesful request
    switch (presence) {
      case Available:
        account->setRequestedPresence(Tp::Presence::available(statusMsg));
        break;
      case Away:
        account->setRequestedPresence(Tp::Presence::away(statusMsg));
        break;
      case Busy:
        account->setRequestedPresence(Tp::Presence::busy(statusMsg));
        break;
      case Invisible:
        account->setRequestedPresence(Tp::Presence::hidden(statusMsg));
        break;
      case Offline:
        account->setRequestedPresence(Tp::Presence::offline());
        break;
      default:
        qDebug() << "Unsuported presence type requested";
    }

    qDebug() << "set presence for account " << account->uniqueIdentifier() << " " << presence << " " << statusMsg;
  }

  Session* TelepathyClient::findSession(const QString &sessionName) {
    if (m_sessions->contains(sessionName))
      return m_sessions->value(sessionName);
    else
      return NULL;
  }

  QString TelepathyClient::sessionIdforIndex(const QModelIndex &index) {
    Tp::AccountPtr account = m_accountsModel->accountForContactIndex(index);
    Tp::ContactPtr contact = m_accountsModel->contactForIndex(index);
    //return account->uniqueIdentifier() + "/" + contact->id();
    return Session::sessionName(account, contact);
  }

  Tp::AccountPtr TelepathyClient::getAccountforChannel(const Tp::ChannelPtr &channel) {
    Tp::ConnectionPtr connection = channel->connection();
    Tp::AccountPtr account;

    foreach(account, m_AM->allAccounts()) {
      //find account which was the channel requested through
      if (account->connection() == connection)
        return account;
    }
    return account;
  }

}

#include "telepathy-client.moc"
