/*
 * Accounts and contacts model
 * This file is based on TelepathyQt4Yell Models
 *
 * Copyright (C) 2010 Collabora Ltd. <http://www.collabora.co.uk/>
 * Copyright (C) 2011 Vojta Kulicka <vojtechkulicka@gmail.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <TelepathyQt4/ContactManager>
#include <TelepathyQt4/PendingReady>

#include "accounts-model.h"
#include "accounts-model-item.h"
#include "contact-model-item.h"


namespace MaknetoBackend {

  struct AccountsModel::Private {

    Private(const Tp::AccountManagerPtr & am)
    : mAM(am) {
    }

    TreeNode * node(const QModelIndex & index) const;

    Tp::AccountManagerPtr mAM;
    TreeNode *mTree;
  };

  TreeNode *AccountsModel::Private::node(const QModelIndex &index) const {
    TreeNode *node = reinterpret_cast<TreeNode *> (index.internalPointer());
    return node ? node : mTree;
  }

  AccountsModel::AccountsModel(const Tp::AccountManagerPtr &am, QObject *parent)
  : QAbstractItemModel(parent),
  mPriv(new AccountsModel::Private(am)) {
    mPriv->mTree = new TreeNode;
    connect(mPriv->mTree,
      SIGNAL(changed(TreeNode*)),
      SLOT(onItemChanged(TreeNode*)));

    connect(mPriv->mTree,
      SIGNAL(childrenAdded(TreeNode*, QList<TreeNode*>)),
      SLOT(onItemsAdded(TreeNode*, QList<TreeNode*>)));

    connect(mPriv->mTree,
      SIGNAL(childrenRemoved(TreeNode*, int, int)),
      SLOT(onItemsRemoved(TreeNode*, int, int)));

    foreach(Tp::AccountPtr account, mPriv->mAM->allAccounts()) {
      AccountsModelItem *item = new AccountsModelItem(account);
      connect(item, SIGNAL(connectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)),
        this, SIGNAL(accountConnectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)));
      mPriv->mTree->addChild(item);
    }

    connect(mPriv->mAM.data(),
      SIGNAL(newAccount(Tp::AccountPtr)),
      SLOT(onNewAccount(Tp::AccountPtr)));

    QHash<int, QByteArray> roles;

    // general roles
    roles[ItemRole] = "item";
    roles[IdRole] = "id";

    // account roles
    roles[ValidRole] = "valid";
    roles[EnabledRole] = "enabled";
    roles[ConnectionManagerNameRole] = "connectionManager";
    roles[ProtocolNameRole] = "protocol";
    roles[DisplayNameRole] = "displayName";
    roles[IconRole] = "icon";
    roles[NicknameRole] = "nickname";
    roles[ConnectsAutomaticallyRole] = "connectsAutomatically";
    roles[ChangingPresenceRole] = "changingPresence";
    roles[AutomaticPresenceRole] = "automaticStatus";
    roles[AutomaticPresenceTypeRole] = "automaticStatusType";
    roles[AutomaticPresenceStatusMessageRole] = "automaticStatusMessage";
    roles[CurrentPresenceRole] = "status";
    roles[CurrentPresenceTypeRole] = "statusType";
    roles[CurrentPresenceStatusMessageRole] = "statusMessage";
    roles[RequestedPresenceRole] = "requestedStatus";
    roles[RequestedPresenceTypeRole] = "requestedStatusType";
    roles[RequestedPresenceStatusMessageRole] = "requestedStatusMessage";
    roles[ConnectionStatusRole] = "connectionStatus";
    roles[ConnectionStatusReasonRole] = "connectionStatusReason";

    // contact roles
    roles[ManagerIdRole] = "managerId";
    roles[AliasRole] = "aliasName";
    roles[AvatarRole] = "avatar";
    roles[PresenceStatusRole] = "presenceStatus";
    roles[PresenceTypeRole] = "presenceType";
    roles[PresenceMessageRole] = "presenceMessage";
    roles[SubscriptionStateRole] = "subscriptionState";
    roles[PublishStateRole] = "publishState";
    roles[BlockedRole] = "blocked";
    roles[GroupsRole] = "groups";
    roles[TextChatCapabilityRole] = "textChat";
    roles[MediaCallCapabilityRole] = "mediaCall";
    roles[AudioCallCapabilityRole] = "audioCall";
    roles[VideoCallCapabilityRole] = "videoCall";
    roles[UpgradeCallCapabilityRole] = "upgradeCall";
    roles[FileTransferCapabilityRole] = "fileTransfer";
    setRoleNames(roles);
  }

  AccountsModel::~AccountsModel() {
    delete mPriv->mTree;
    delete mPriv;
  }

  void AccountsModel::onNewAccount(const Tp::AccountPtr &account) {
    AccountsModelItem *accountNode = new AccountsModelItem(account);

    connect(accountNode, SIGNAL(connectionStatusChanged(QString, int)),
      this, SIGNAL(accountConnectionStatusChanged(QString, int)));

    onItemsAdded(mPriv->mTree, QList<TreeNode *>() << accountNode);
  }

  void AccountsModel::onItemChanged(TreeNode *node) {
    QModelIndex accountIndex = index(node);
    emit dataChanged(accountIndex, accountIndex);
  }

  void AccountsModel::onItemsAdded(TreeNode *parent, const QList<TreeNode *> &nodes) {
    if (nodes.isEmpty()){
      qWarning() << "AccountsModel: attempt insert empty nodes list";
      return;
    }
    
    QModelIndex parentIndex = index(parent);
    int currentSize = rowCount(parentIndex);
    qDebug() << "AccountsModel: items adding" <<currentSize << ( currentSize + nodes.size() - 1);
    beginInsertRows(parentIndex, currentSize, currentSize + nodes.size() - 1);

    foreach(TreeNode *node, nodes) {
      parent->addChild(node);
    }
    endInsertRows();

    emit accountCountChanged();
  }

  void AccountsModel::onItemsRemoved(TreeNode *parent, int first, int last) {
    QModelIndex parentIndex = index(parent);
    //QList<TreeNode *> removedItems;
    qDebug() << "AccountsModel: items removing" << first << "" << last;
    beginRemoveRows(parentIndex, first, last);
    for (int i = last; i >= first; i--) {
      parent->childAt(i)->remove();
    }
    endRemoveRows();

    emit accountCountChanged();
  }

  int AccountsModel::accountCount() const {
    return mPriv->mTree->size();
  }

  QObject *AccountsModel::accountItemForId(const QString &id) const {
    for (int i = 0; i < mPriv->mTree->size(); ++i) {
      AccountsModelItem *item = qobject_cast<AccountsModelItem*>(mPriv->mTree->childAt(i));
      if (!item) {
        continue;
      }

      if (item->data(IdRole) == id) {
        return item;
      }
    }

    return 0;
  }

  QObject *AccountsModel::contactItemForId(const QString &accountId, const QString &contactId) const {
    AccountsModelItem *accountItem = qobject_cast<AccountsModelItem*>(accountItemForId(accountId));
    if (!accountItem) {
      return 0;
    }

    for (int i = 0; i < accountItem->size(); ++i) {
      ContactModelItem *item = qobject_cast<ContactModelItem*>(accountItem->childAt(i));
      if (!item) {
        continue;
      }

      if (item->data(IdRole) == contactId) {
        return item;
      }
    }

    return 0;
  }

  QModelIndex AccountsModel::contactIndexForId(const QString &accountId, const QString &contactId) const {
    QModelIndex root = QModelIndex();
    int rows = rowCount(root);
    for (int row = 0; row < rows; row++) {
      QModelIndex accountIndex = index(row, 0, root);
      Tp::AccountPtr account = accountForIndex(accountIndex);
      if (!account || account->uniqueIdentifier() != accountId)
        continue;

      int rows2 = rowCount(accountIndex);
      for (int cotnactRow = 0; cotnactRow < rows2; cotnactRow++) {
        QModelIndex contactIndex = index(cotnactRow, 0, accountIndex);
        Tp::ContactPtr contact = contactForIndex(contactIndex);
        if (contact && contact->id() == contactId)
          return contactIndex;
      }
    }
    return QModelIndex();
  }

  int AccountsModel::columnCount(const QModelIndex &parent) const {
    return 1;
  }

  int AccountsModel::rowCount(const QModelIndex &parent) const {
    return mPriv->node(parent)->size();
  }

  QVariant AccountsModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
      return QVariant();
    }
    if (role == Qt::SizeHintRole) {
      return QVariant(QSize(24, 24));
    }
    return mPriv->node(index)->data(role);
  }

  Tp::AccountPtr AccountsModel::accountForIndex(const QModelIndex &index) const {
    TreeNode *accountNode = mPriv->node(index);
    AccountsModelItem *item = qobject_cast<AccountsModelItem *>(accountNode);
    if (item) {
      return item->account();
    } else {
      qWarning() << "could not find account index " << index.row();
      return Tp::AccountPtr();
    }
  }

  Tp::ContactPtr AccountsModel::contactForIndex(const QModelIndex& index) const {
    TreeNode *contactNode = mPriv->node(index);
    ContactModelItem *item = qobject_cast<ContactModelItem *>(contactNode);
    if (item) {
      return item->contact();
    } else {
      return Tp::ContactPtr();
    }
  }

  Tp::AccountPtr AccountsModel::accountForContactIndex(const QModelIndex& index) const {
    TreeNode *contactNode = mPriv->node(index);
    AccountsModelItem *item = qobject_cast<AccountsModelItem*>(contactNode->parent());
    if (item) {
      return item->account();
    } else {
      return Tp::AccountPtr();
    }
  }

  Qt::ItemFlags AccountsModel::flags(const QModelIndex &index) const {
    if (index.isValid()) {
      return Qt::ItemIsEnabled;
    }

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
  }

  bool AccountsModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (index.isValid()) {
      mPriv->node(index)->setData(role, value);
    }

    return false;
  }

  QModelIndex AccountsModel::index(int row, int column, const QModelIndex &parent) const {
    TreeNode *parentNode = mPriv->node(parent);
    return createIndex(row, column, row >= parentNode->size() ? NULL : parentNode->childAt(row));
  }

  QModelIndex AccountsModel::index(TreeNode *node) const {
    if (node->parent()) {
      return createIndex(node->parent()->indexOf(node), 0, node);
    } else {
      return QModelIndex();
    }
  }

  QModelIndex AccountsModel::parent(const QModelIndex &index) const {
    if (!index.isValid()) {
      return QModelIndex();
    }

    TreeNode *currentNode = mPriv->node(index);
    if (currentNode->parent()) {
      return AccountsModel::index(currentNode->parent());
    } else {
      // no parent: return root node
      return QModelIndex();
    }
  }

  QVariant AccountsModel::data(int row, int role) {

    return data(index(row, 0, QModelIndex()), role);
  }
}

#include "accounts-model.moc"
