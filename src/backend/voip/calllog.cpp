/*
    Copyright (C) 2009  George Kiagiadakis <kiagiadakis.george@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "calllog.h"
#include <QtGui/QPlainTextEdit>
//#include <KLocalizedString>
//#include <KDebug>
#include <QDebug>

namespace MaknetoBackend {

CallLog::CallLog(QPlainTextEdit *logView, QObject *parent)
    : QObject(parent), m_logView(logView), m_errorLogged(false)
{
    qRegisterMetaType<CallLog::LogType>();
}

void CallLog::logMessage(CallLog::LogType type, const QString & message)
{
    QString modifiedMessage;
    switch(type) {
    case Information:
        modifiedMessage = message;
        break;
    case Warning:
        modifiedMessage = tr("Warning: %1").arg( message );
        break;
    case Error:
        modifiedMessage = tr("Error: %1").arg( message );
        m_errorLogged = true;
        break;
    default:
        Q_ASSERT(false);
    }

    if ( m_logView ) {
        m_logView->appendPlainText(modifiedMessage);
        if ( type == Error ) {
            emit notifyUser();
        }
    } else {
        qDebug() << "LOG:" << modifiedMessage;
    }
}

}

#include "calllog.moc"
