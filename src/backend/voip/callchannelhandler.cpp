/*
    Copyright (C) 2009 George Kiagiadakis <kiagiadakis.george@gmail.com>
    Copyright (C) 2010 Collabora Ltd. <info@collabora.co.uk>
      @author George Kiagiadakis <george.kiagiadakis@collabora.co.uk>

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation; either version 2.1 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "callchannelhandler_p.h"
#include "element-factory/deviceelementfactory.h"
#include <QGlib/Connect> 
#include <QGst/Init>
#include <QGst/ElementFactory>
#include <QGst/Bus>
#include <QGst/GhostPad>
#include <QGst/Message>
#include <QGst/Fraction>
#include <TelepathyQt4/Connection>

#include <QDesktopServices>
#include <QDebug>

#include "sleep.h"

namespace QGlib {

  template <> struct GetTypeImpl<GList*> {

    inline operator Type() {
      return fs_codec_list_get_type();
    }
  };
}

namespace MaknetoBackend {

  CallChannelHandler::CallChannelHandler(const Tp::StreamedMediaChannelPtr & channel, QObject *parent)
  : QObject(parent), d(new CallChannelHandlerPrivate(this)) {
    d->init(channel);
    qDebug() << "CallChannelHandler: Created";
  }

  CallChannelHandler::~CallChannelHandler() {
    delete d;
  }

  QList<CallParticipant*> CallChannelHandler::participants() const {
    return d->participants();
  }

  void CallChannelHandler::hangup(const QString & message) {
    qDebug() << "CallChannelHandler: Hang up";
    Q_UNUSED(message);
    //no reason to handle the return value. invalidated() will be emited...
    d->channel()->hangupCall();
  }

  void CallChannelHandler::accept() {
    qDebug() << "CallChannelHandler: accept";
    d->accept();
  }

  CallChannelHandlerPrivate::~CallChannelHandlerPrivate() {
    qDebug() << "CallChannelHandler: desctructor";

    if (m_pipeline)
      m_pipeline->setState(QGst::StateNull);
    if (m_participantData[Myself]->audioBin)
      m_participantData[Myself]->audioBin->setState(QGst::StateNull);
    if (m_participantData[Myself]->videoBin)
      m_participantData[Myself]->videoBin->setState(QGst::StateNull);
    if (m_participantData[RemoteContact]->audioBin)
      m_participantData[RemoteContact]->audioBin->setState(QGst::StateNull);
    if (m_participantData[RemoteContact]->videoBin)
      m_participantData[RemoteContact]->videoBin->setState(QGst::StateNull);
    if (m_audioOutputDevice)
      m_audioOutputDevice->setState(QGst::StateNull);
    if (m_audioInputDevice)
      m_audioInputDevice->setState(QGst::StateNull);

  }

  void CallChannelHandlerPrivate::init(const Tp::StreamedMediaChannelPtr & channel) {
    qDebug() << "CallChannelHandler: Init";
    m_channel = channel;
    m_participantData[Myself] = new ParticipantData(this, Myself);
    m_participantData[RemoteContact] = new ParticipantData(this, RemoteContact);

    connect(m_channel.data(),
      SIGNAL(invalidated(Tp::DBusProxy*, QString, QString)),
      SLOT(onChannelInvalidated(Tp::DBusProxy*, QString, QString)));

  }

  void CallChannelHandlerPrivate::accept() {
    //initialize gstreamer
    static bool gstInitDone = false;
    if (!gstInitDone) {
      QGst::init();
      gstInitDone = true;
    }
    m_pipeline = QGst::Pipeline::create();
    m_pipeline->setState(QGst::StatePlaying);

    TfChannel *tfChannel = Tp::createFarsightChannel(m_channel);
    Q_ASSERT(tfChannel);
    m_tfChannel = QGlib::ObjectPtr::wrap(reinterpret_cast<GObject*> (tfChannel), false);

    /* Set up the telepathy farsight channel */
    QGlib::connect(m_tfChannel, "closed",
      this, &CallChannelHandlerPrivate::onTfChannelClosed);
    QGlib::connect(m_tfChannel, "session-created",
      this, &CallChannelHandlerPrivate::onSessionCreated);
    QGlib::connect(m_tfChannel, "stream-created",
      this, &CallChannelHandlerPrivate::onStreamCreated);
    QGlib::connect(m_tfChannel, "stream-get-codec-config",
      this, &CallChannelHandlerPrivate::onStreamGetCodecConfig);
    QGlib::connect(m_pipeline->bus(), "message",
      this, &CallChannelHandlerPrivate::onBusMessage);

    m_channel->acceptCall();
  }

  Tp::ContactPtr CallChannelHandlerPrivate::contactOfParticipant(Who who) const {
    if (who == Myself) {
      return m_channel->groupSelfContact();
    } else {
      Tp::Contacts contacts = m_channel->groupContacts();
      Tp::ContactPtr contact;

      Q_FOREACH(const Tp::ContactPtr & c, contacts) {
        if (c != m_channel->groupSelfContact()) {
          contact = c;
        }
      }
      if (!contact) {
        qWarning() << "CallChannelHandlerPrivate: No remote contact available, returning NULL!";
      }
      return contact;
    }
  }

  void CallChannelHandlerPrivate::onChannelInvalidated(Tp::DBusProxy *proxy,
    const QString & errorName,
    const QString & errorMessage) {
    Q_UNUSED(proxy);
    qDebug() << "CallChannelHandlerPrivate: Channel invalidated:" << errorName << errorMessage;

    if (errorName != "org.freedesktop.Telepathy.Error.Cancelled" // local hangup
      && errorName != "org.freedesktop.Telepathy.Error.Terminated") // remote hangup
      emit q->error(errorMessage);

    Q_EMIT q->callEnded(errorMessage);
  }

  void CallChannelHandlerPrivate::onSessionCreated(QGst::ElementPtr & conference) {
    qDebug() << "CallChannelHandler: Session created";
    qDebug();
    m_pipeline->bus()->addSignalWatch();
    m_conference = conference;
    m_pipeline->add(conference);
    conference->setState(QGst::StatePlaying);
  }

  void CallChannelHandlerPrivate::onTfChannelClosed() {
    qDebug() << "CallChannelHandler: channel closed, stopping pipeline";
    m_pipeline->bus()->removeSignalWatch();
    m_pipeline->setState(QGst::StateNull);
    // FIXME: is ok, emit callEnded here?
    Q_EMIT q->callEnded(QString());
  }

  void CallChannelHandlerPrivate::onBusMessage(const QGst::MessagePtr & message) {
    TfChannel *c = reinterpret_cast<TfChannel*> (static_cast<GObject*> (m_tfChannel));
    tf_channel_bus_message(c, message);
  }

  void CallChannelHandlerPrivate::onStreamCreated(const QGlib::ObjectPtr & stream) {
    qDebug() << "CallChannelHandler: Stream created";

    QGlib::connect(stream, "request-resource", this,
      &CallChannelHandlerPrivate::onRequestResource, QGlib::PassSender);
    QGlib::connect(stream, "src-pad-added", this,
      &CallChannelHandlerPrivate::onSrcPadAdded, QGlib::PassSender);
    QGlib::connect(stream, "free-resource", this,
      &CallChannelHandlerPrivate::onFreeResource, QGlib::PassSender);
    QGlib::connect(stream, "closed", this,
      &CallChannelHandlerPrivate::onStreamClosed, QGlib::PassSender);

    bool audio = stream->property("media-type").get<uint > () == Tp::MediaStreamTypeAudio;
    qDebug() << "CallChannelHandler: Opening" << (audio ? "audio" : "video") << "input device";

    QGst::ElementPtr src = audio ? DeviceElementFactory::makeAudioCaptureElement()
      : DeviceElementFactory::makeVideoCaptureElement();
    if (!src) {
      qDebug() << "CallChannelHandler: Could not initialize audio input device";
      Q_EMIT q->error(audio ? tr("The audio input device could not be initialized")
        : tr("The video input device could not be initialized"));
      return;
    }

    if (!(audio ? createAudioBin(m_participantData[Myself])
      : createVideoBin(m_participantData[Myself], true))) {
      qDebug() << "CallChannelHandler: Could not create some of the required elements.";
      Q_EMIT q->error(tr("Could not create some of the required elements. "
        "Please check your GStreamer installation."));
      src->setState(QGst::StateNull); //cleanly dispose the src element
      return;
    }

    (audio ? m_audioInputDevice : m_videoInputDevice) = src;

    QGst::BinPtr & bin = audio ? m_participantData[Myself]->audioBin
      : m_participantData[Myself]->videoBin;

    m_pipeline->add(src);
    m_pipeline->add(bin);
    src->link(bin);

    //set everything to playing state
    bin->setState(QGst::StatePlaying);
    src->setState(QGst::StatePlaying);

    bin->getStaticPad("src")->link(stream->property("sink-pad").get<QGst::PadPtr > ());

    //create the participant if not already there
    if (!m_participants.contains(Myself)) {
      m_participants[Myself] = new CallParticipant(m_participantData[Myself], q);
      Q_EMIT q->participantJoined(m_participants[Myself]);
    }

    if (audio) {
      Q_EMIT m_participants[Myself]->audioStreamAdded(m_participants[Myself]);
    } else {
      Q_EMIT m_participants[Myself]->videoStreamAdded(m_participants[Myself]);
    }
  }

  GList *CallChannelHandlerPrivate::onStreamGetCodecConfig() {
    qDebug() << "CallChannelHandler: Get codec config";

    //QString codecsFile = KStandardDirs::locate("data", "makneto/codec-preferences.ini");
    QString codecsFile = QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "makneto/codec-preferences.ini";
    if (!codecsFile.isEmpty()) {
      qDebug() << "CallChannelHandler: Reading codec preferences from" << codecsFile;
      return fs_codec_list_from_keyfile(QFile::encodeName(codecsFile), NULL);
    } else {
      qDebug() << "CallChannelHandler: Could not find codec file";
      return NULL;
    }
  }

  bool CallChannelHandlerPrivate::onRequestResource(const QGlib::ObjectPtr & stream, uint direction) {
    bool audio = stream->property("media-type").get<uint > () == Tp::MediaStreamTypeAudio;
    qDebug() << "CallChannelHandler: Request" << (audio ? "Audio" : "Video") << "resource for" << (direction == Tp::MediaStreamDirectionSend ? "sending" : "receiving");

    switch (direction) {
      case Tp::MediaStreamDirectionSend:
      {
        QGst::BinPtr & bin = audio ? m_participantData[Myself]->audioBin
          : m_participantData[Myself]->videoBin;
        if (!bin) {
          return false;
        }
        break;
      }
      case Tp::MediaStreamDirectionReceive:
      {
        qDebug() << "CallChannelHandler: Opening" << (audio ? "audio" : "video") << "output device";
        qDebug() << "CallChannelHandler: Request resource - MediaStreamDirectionReceive";
        if (audio) {
          QGst::ElementPtr element = DeviceElementFactory::makeAudioOutputElement();
          if (!element) {
            qDebug() << "CallChannelHandler: Request resource - Could not create audio output";
            Q_EMIT q->error(tr("The audio output device could not be initialized"));
            return false;
          }
          m_audioOutputDevice = element;
        }

        //create the participant if not already there
        if (!m_participants.contains(RemoteContact)) {
          m_participants[RemoteContact] = new CallParticipant(m_participantData[RemoteContact], q);
          Q_EMIT q->participantJoined(m_participants[RemoteContact]);
        }

        if (!(audio ? createAudioBin(m_participantData[RemoteContact])
          : createVideoBin(m_participantData[RemoteContact], false))) {
          qDebug() << "CallChannelHandler: Request resource - Could not create some of the audio elements";
          Q_EMIT q->error(tr("Could not create some of the required elements. "
            "Please check your GStreamer installation."));

          //destroy the audio output element, since we could not create a bin to connect it to
          if (audio) {
            m_audioOutputDevice->setState(QGst::StateNull);
            m_audioOutputDevice.clear();
          }
          return false;
        }

        if (audio) {
          m_pipeline->add(m_audioOutputDevice);
          m_pipeline->add(m_participantData[RemoteContact]->audioBin);
          m_participantData[RemoteContact]->audioBin->link(m_audioOutputDevice);
          m_audioOutputDevice->setState(QGst::StatePlaying);
          m_participantData[RemoteContact]->audioBin->setState(QGst::StatePlaying);
        } else {
          m_pipeline->add(m_participantData[RemoteContact]->videoBin);
          m_participantData[RemoteContact]->videoBin->setState(QGst::StatePlaying);
        }

        m_participantData[RemoteContact]->mutex.lock();
        if (!m_participantData[RemoteContact]->preparedPad.isNull()) {
          qWarning() << "CallChannelHandler: We have prepared pad already, linking";
          QGst::BinPtr & bin = audio ? m_participantData[RemoteContact]->audioBin
            : m_participantData[RemoteContact]->videoBin;
          m_participantData[RemoteContact]->preparedPad->link(bin->getStaticPad("sink"));
          m_participantData[RemoteContact]->preparedPad = QGst::PadPtr();
        }
        m_participantData[RemoteContact]->mutex.unlock();

        if (audio) {
          Q_EMIT m_participants[RemoteContact]->audioStreamAdded(m_participants[RemoteContact]);
        } else {
          Q_EMIT m_participants[RemoteContact]->videoStreamAdded(m_participants[RemoteContact]);
        }

        break;
      }
      default:
        Q_ASSERT(false);
    }

    return true;
  }

  /* WARNING this slot is called in a different thread. */
  void CallChannelHandlerPrivate::onSrcPadAdded(const QGlib::ObjectPtr & stream, QGst::PadPtr & pad) {
    bool audio = stream->property("media-type").get<uint > () == Tp::MediaStreamTypeAudio;
    QGst::BinPtr & bin = audio ? m_participantData[RemoteContact]->audioBin
      : m_participantData[RemoteContact]->videoBin;

    qDebug() << "CallChannelHandler: " << (audio ? "Audio" : "Video") << "src pad added";

    m_participantData[RemoteContact]->mutex.lock();
    if (bin.isNull()) {
      qWarning() << "CallChannelHandler: " << (audio ? "Audio" : "Video") << "RemoteContact bin is null!";
      // FIXME: add prepared pad to participant data and connect when TP requestign resources...
      m_participantData[RemoteContact]->preparedPad = pad;
      m_participantData[RemoteContact]->mutex.unlock();
      return;
    }

    pad->link(bin->getStaticPad("sink"));
    m_participantData[RemoteContact]->mutex.unlock();
  }

  void CallChannelHandlerPrivate::onFreeResource(const QGlib::ObjectPtr & stream, uint direction) {
    bool audio = stream->property("media-type").get<uint > () == Tp::MediaStreamTypeAudio;
    Who who = direction == Tp::MediaStreamDirectionSend ? Myself : RemoteContact;

    QGst::BinPtr & bin = audio ? m_participantData[who]->audioBin
      : m_participantData[who]->videoBin;
    QGst::ElementPtr & element = audio ? (who == Myself ? m_audioInputDevice : m_audioOutputDevice)
      : m_videoInputDevice;

    qDebug() << "CallChannelHandlerPrivate: Closing" << (audio ? "audio" : "video")
      << ((who == Myself) ? "input" : "output") << "device";

    /* FIXME
     * start UGLY HACK for avoid QtGstreamer crash. 
     * 
     * Bin should change state for its children, but it is asyncronous 
     * and I don't know how to synchronize it. So, We use this nasty loop 
     * and try set NULL state for all children manualy...
     */
    int counter = 0;
    while (!tryStopElements(audio, bin, element, who)) {
      qWarning() << "CallChannelHandlerPrivate: Set elements to NULL state failed! Try wait a bit...";
      Sleep::msleep(100); // FIXME: this is very bad way... find howto properly clear pipe...
      if (counter == 10) {
        qWarning() << "CallChannelHandlerPrivate: Stopping QGst elements failed" << counter << "x, give up. Prapare to crash :(";
        break;
      }
      counter++;
    }

    counter = 0;
    while (m_pipeline->bus()->hasPendingMessages()) {
      qWarning() << "CallChannelHandlerPrivate: pipeline bus is not empty";
      m_pipeline->enableAutoClock();
      Sleep::msleep(100); // FIXME: this is very bad way... find howto properly clear pipe...
      if (counter == 10) {
        qWarning() << "CallChannelHandlerPrivate: Waiting for empty GST bus failed" << counter << "x, give up. Prapare to crash :(";
        break;
      }
      counter++;
    }
    if (!m_pipeline->bus()->hasPendingMessages()) {
      qDebug() << "CallChannelHandlerPrivate: pipeline bus is empty";
    } else {
      qWarning() << "CallChannelHandlerPrivate: pipeline bus is not empty";
    }
    // end of UGLY HACK


    m_pipeline->remove(bin);
    if (audio || who != RemoteContact) {
      m_pipeline->remove(element);
    }

    if (audio) {
      Q_EMIT m_participants[who]->audioStreamRemoved(m_participants[who]);
    } else {
      Q_EMIT m_participants[who]->videoStreamRemoved(m_participants[who]);
    }

    //drop references to destroy the elements
    if (audio) {
      m_participantData[who]->volumeControl.clear();
    } else {
      m_participantData[who]->colorBalanceControl.clear();
      //m_participantData[who]->videoWidget.data()->setVideoSink(QGst::ElementPtr());
      m_participantData[who]->videoOutElemPtr.clear(); // FIXME: it is right?
    }
    bin.clear();
    if (audio || who != RemoteContact) {
      element.clear();
    }

    //remove the participant if it has no other stream on it
    if ((audio && !m_participants[who]->hasVideoStream()) ||
      (!audio && !m_participants[who]->hasAudioStream())) {
      CallParticipant *p = m_participants.take(who);
      Q_EMIT q->participantLeft(p);
      p->deleteLater();
    }
  }

  bool CallChannelHandlerPrivate::tryStopElements(bool audio, QGst::BinPtr &bin, QGst::ElementPtr & element, Who who) {
    // test for NULL state    
    if (audio || who != RemoteContact) {
      if (!tryStopElement(element))
        return false;
    }

    if (audio) {
      if (!tryStopElement(m_participantData[who]->volumeControl.dynamicCast<QGst::Element > (), bin))
        return false;
    } else {
      if (!tryStopElement(m_participantData[who]->colorBalanceControl.dynamicCast<QGst::Element > (), bin))
        return false;
    }

    // test bin children for NULL state
    QGst::ChildProxyPtr childProxy = bin.dynamicCast<QGst::ChildProxy > ();
    if (!childProxy)
      qDebug() << "CallChannelHandlerPrivate: cast to ChildProxy failed";

    if (childProxy) {
      for (uint i = 0; i < childProxy->childrenCount();) {
        qDebug() << "CallChannelHandlerPrivate:" << bin->name() << "has" << childProxy->childrenCount() << "children. try stop" << i;
        QGst::ElementPtr element = childProxy->childByIndex(i).dynamicCast<QGst::Element > ();
        if (!element) {
          i++;
          continue;
        }
        if (!tryStopElement(element, bin))
          return false;
      }
    }
    if (!tryStopElement(bin))
      return false;
    return true;
  }

  bool CallChannelHandlerPrivate::tryStopElement(const QGst::ElementPtr &element, QGst::BinPtr bin) {
    qDebug() << "CallChannelHandlerPrivate: try stop element" << element->name() << "...";
    if (!element->setState(QGst::StateNull)) {
      qWarning() << "CallChannelHandlerPrivate: set state failed!";
      return false;
    }

    QGst::State state, pending;
    if (element->getState(&state, &pending, 1e9) != QGst::StateChangeSuccess) {
      qWarning() << "CallChannelHandlerPrivate: getting state for control failed!";
      return false;
    }
    qDebug() << "CallChannelHandlerPrivate:     current state: " << state << " pending:" << pending;
    if (state != QGst::StateNull) {
      qWarning() << "CallChannelHandlerPrivate:     current state is not null!";
      return false;
    }
    if (!bin.isNull()) {
      bin->remove(element);
    }
    return true;
  }

  void CallChannelHandlerPrivate::onStreamClosed(const QGlib::ObjectPtr & stream) {
    qDebug();

    bool audio = stream->property("media-type").get<uint > () == Tp::MediaStreamTypeAudio;
    QGst::BinPtr & bin = audio ? m_participantData[Myself]->audioBin
      : m_participantData[Myself]->videoBin;

    /* For some reason telepathy-farsight does not emit the free-resource signal for the
     * input resources, so we have to simulate this emission here. For future compatibility
     * (in case this gets fixed in tp-farsight), we check if the bin is null or not before
     * simulating the emission. If it is null, it means the signal was emitted earlier.
     */
    if (bin) {
      onFreeResource(stream, Tp::MediaStreamDirectionSend);
    }
  }

  bool CallChannelHandlerPrivate::createAudioBin(QExplicitlySharedDataPointer<ParticipantData> & data) {
    qDebug() << "CallChannelHandler: Create audio bin for " << (data->who == CallChannelHandlerPrivate::RemoteContact ? "remote" : "myself");

    QGst::BinPtr bin = QGst::Bin::create();
    QGst::ElementPtr volume = QGst::ElementFactory::make("volume");
    QGst::ElementPtr audioConvert = QGst::ElementFactory::make("audioconvert");
    QGst::ElementPtr audioResample = QGst::ElementFactory::make("audioresample");

    if (!bin || !volume || !audioConvert || !audioResample) {
      qWarning() << "Could not make some of the audio bin elements!";
      return false;
    }

    if (!(data->volumeControl = volume.dynamicCast<QGst::StreamVolume > ())) {
      qWarning() << "Could not cast volume element to GstStreamVolume!";
      return false;
    }
    data->audioBin = bin;

    bin->add(volume);
    bin->add(audioConvert);
    bin->add(audioResample);

    bin->addPad(QGst::GhostPad::create(volume->getStaticPad("sink"), "sink"));
    volume->link(audioConvert);
    audioConvert->link(audioResample);
    bin->addPad(QGst::GhostPad::create(audioResample->getStaticPad("src"), "src"));
    return true;
  }

  bool CallChannelHandlerPrivate::createVideoBin(QExplicitlySharedDataPointer<ParticipantData> & data, bool isVideoSrc) {
    qDebug() << "CallChannelHandler: Create video bin for " << (data->who == CallChannelHandlerPrivate::RemoteContact ? "remote" : "myself");

    QGst::BinPtr bin = QGst::Bin::create();
    QGst::ElementPtr videoBalance = QGst::ElementFactory::make("videobalance");
    QGst::ElementPtr colorspace = QGst::ElementFactory::make("ffmpegcolorspace");
    QGst::ElementPtr videoscale = QGst::ElementFactory::make("videoscale");
    QGst::ElementPtr videosink = DeviceElementFactory::makeVideoOutputElement();

    if (!bin || !videoBalance || !colorspace || !videoscale || !videosink) {
      qDebug() << "Could not make some of the video bin elements";
      return false;
    }

    videosink->setProperty("force-aspect-ratio", true); //FIXME should be externally configurable

    bin->add(videoBalance);
    bin->add(colorspace);
    bin->add(videoscale);
    bin->add(videosink);

    /* if we are creating the bin for the video source, we create the following pipeline:
     * (srcPad) ! videomaxrate (optional) ! videobalance ! ffmpegcolorspace ! videoscale ! capsfilter !
     * postproc_tmpnoise (optional) ! tee ! -> queue ! ffmpegcolorspace ! videoscale ! qwidgetvideosink
     *                                      -> queue ! (sinkPad)
     * else we just create a simple:
     * (srcPad) ! videobalance ! ffmpegcolorspace ! videoscale ! qwidgetvideosink
     *
     * videomaxrate is optional because it is in gst-plugins-bad and similarly, postproc_tmpnoise
     * is optional because it is in gst-plugins-ffmpeg.
     */
    if (isVideoSrc) {
      QGst::ElementPtr videomaxrate = QGst::ElementFactory::make("videomaxrate");
      QGst::ElementPtr colorspace2 = QGst::ElementFactory::make("ffmpegcolorspace");
      QGst::ElementPtr videoscale2 = QGst::ElementFactory::make("videoscale");
      QGst::ElementPtr capsfilter = QGst::ElementFactory::make("capsfilter");
      QGst::ElementPtr postproc = QGst::ElementFactory::make("postproc_tmpnoise");
      QGst::ElementPtr tee = QGst::ElementFactory::make("tee");
      QGst::ElementPtr queue = QGst::ElementFactory::make("queue");
      QGst::ElementPtr queue2 = QGst::ElementFactory::make("queue");

      if (!colorspace2 || !videoscale2 || !capsfilter || !tee || !queue || !queue2) {
        qDebug() << "Could not make some of the video bin elements";
        return false;
      }

      try {
        // TODO: queue size should be configurable
        queue->setProperty("max-size-time", 500l * 1000000l); // in nanoseconds (500ms)
        //queue->setProperty("leaky", (int)GST_QUEUE_LEAK_DOWNSTREAM); // leak odest frames
        
        queue2->setProperty("max-size-time", 500l * 1000000l); // in nanoseconds (500ms)
        //queue2->setProperty("leaky", (int)GST_QUEUE_LEAK_DOWNSTREAM); // leak odest frames
        
      } catch (const std::logic_error & error) {
        qDebug() << "FIXME: Element queue does not support max-size-time property";
      }

      QGst::Structure capsStruct("video/x-raw-yuv");
      capsStruct.setValue("width", 320);
      capsStruct.setValue("height", 240);

      /* videomaxrate is optional, since it is not always available */
      if (videomaxrate) {
        bin->add(videomaxrate);
      } else {
        qDebug() << "NOT using videomaxrate";
        capsStruct.setValue("framerate", QGst::Fraction(15, 1));
      }

      QGst::CapsPtr caps = QGst::Caps::createEmpty();
      caps->appendStructure(capsStruct);
      capsfilter->setProperty("caps", caps);

      bin->add(colorspace2);
      bin->add(videoscale2);
      bin->add(capsfilter);
      if (postproc) {
        bin->add(postproc);
      }
      bin->add(tee);
      bin->add(queue);
      bin->add(queue2);

      if (videomaxrate) {
        bin->addPad(QGst::GhostPad::create(videomaxrate->getStaticPad("sink"), "sink"));
        videomaxrate->link(videoBalance);
      } else {
        bin->addPad(QGst::GhostPad::create(videoBalance->getStaticPad("sink"), "sink"));
      }
      videoBalance->link(colorspace2);
      colorspace2->link(videoscale2);
      videoscale2->link(capsfilter);
      if (postproc) {
        capsfilter->link(postproc);
        postproc->link(tee);
      } else {
        qDebug() << "NOT using postproc_tmpnoise";
        capsfilter->link(tee);
      }

      //video preview branch
      tee->getRequestPad("src%d")->link(queue->getStaticPad("sink"));
      queue->link(colorspace);

      //src pad for fsconference branch
      tee->getRequestPad("src%d")->link(queue2->getStaticPad("sink"));
      bin->addPad(QGst::GhostPad::create(queue2->getStaticPad("src"), "src"));
    } else {
      bin->addPad(QGst::GhostPad::create(videoBalance->getStaticPad("sink"), "sink"));
      videoBalance->link(colorspace);
    }

    colorspace->link(videoscale);
    videoscale->link(videosink);

    if (!(data->colorBalanceControl = videoBalance.dynamicCast<QGst::ColorBalance > ())) {
      qDebug() << "Could not cast videobalance element to GstColorBalance";
      return false;
    }
    data->videoBin = bin;

    //data->videoWidget = new QGst::Ui::VideoWidget;
    //data->videoWidget.data()->setVideoSink(videosink);
    data->videoOutElemPtr = videosink;

    return true;
  }

} //end namespace MaknetoLib

#include "callchannelhandler.moc"
#include "callchannelhandler_p.moc"
