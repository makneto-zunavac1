/*
    Copyright (C) 2009  George Kiagiadakis <kiagiadakis.george@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef VIDEOBALANCEWIDGET_H
#define VIDEOBALANCEWIDGET_H

#include <QtGui/QWidget>
#include "../makneto-backend.h"

class MAKNETO_EXPORT VideoBalanceWidget : public QWidget
{
    Q_OBJECT
public:
    VideoBalanceWidget(QWidget *parent = 0);
    virtual ~VideoBalanceWidget();

    void setVideoBalanceControl(QObject *control);

private slots:
    void onSliderValueChanged(int value);

private:
    struct Private;
    Private *const d;
};

#endif
