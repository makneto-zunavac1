/* 
 * File:   Sleep.h
 * Author: karry
 *
 * Created on 13. říjen 2011, 19:37
 */


#ifndef SLEEP_H
#define	SLEEP_H

#include "../makneto-backend.h"

class MAKNETO_EXPORT Sleep {
public:
  static void msleep(unsigned long msecs);
};

#endif	/* SLEEP_H */

