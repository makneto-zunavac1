/* 
 * File:   Sleep.cpp
 * Author: karry
 * 
 * Created on 13. říjen 2011, 19:37
 */

#include <QWaitCondition>
#include <QMutex>

#include "sleep.h"

void Sleep::msleep(unsigned long msecs) {
  QMutex mutex;
  mutex.lock();

  QWaitCondition waitCondition;
  waitCondition.wait(&mutex, msecs);

  mutex.unlock();
}
