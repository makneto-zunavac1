/*
 * This file is based on Collabora example
 *  
 * Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef ELEMENTFACTORY_H
#define	ELEMENTFACTORY_H

#include <QtCore/QObject>
#include <QGst/Global>

namespace MaknetoBackend {

  class IElementFactory {
  public:
    virtual QGst::ElementPtr makeAudioCaptureElement() = 0;
    virtual QGst::ElementPtr makeAudioOutputElement() = 0;
    virtual QGst::ElementPtr makeVideoCaptureElement() = 0;
    virtual QGst::ElementPtr makeVideoOutputElement() = 0;
    virtual inline ~IElementFactory(){}
  //protected:
    //virtual IElementFactory();
  };

}


#endif	/* ELEMENTFACTORY_H */

