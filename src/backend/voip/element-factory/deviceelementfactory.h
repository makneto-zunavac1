/*
 * This file is based on Collabora example
 *  
 * Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef DEVICEELEMENTFACTORY_H
#define DEVICEELEMENTFACTORY_H

#include <QGst/Global>
#include "elementfactory.h"

namespace MaknetoBackend {

  class DeviceElementFactory {
  public:
    static QGst::ElementPtr makeAudioCaptureElement();
    static QGst::ElementPtr makeAudioOutputElement();
    static QGst::ElementPtr makeVideoCaptureElement();
    static QGst::ElementPtr makeVideoOutputElement();

    static void installFactory(IElementFactory* factory);
    static void destroyAllFactories();
  private:
    static IElementFactory *getInstalledFactory();

    static IElementFactory *factoryInstance;
  };

}
#endif // DEVICEELEMENTFACTORY_H
