/*
 * This file is based on Collabora example
 *  
 * Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QGst/Bin>
#include <QGst/ElementFactory>

#include "defaultelementfactory.h"

namespace MaknetoBackend {

  /**
   * for testing, try command 
   *    gst-launch autoaudiosrc ! audioconvert ! audioresample ! alsasink 
   *                                           ! lame bitrate=192 ! filesink location=test.mp3
   * 
   * @return 
   */
  QGst::ElementPtr DefaultElementFactory::makeAudioCaptureElement() {
    QGst::ElementPtr element;

    /*
    // UNCOMENT FOR DEBUG ... test device (1kHz sinus)
    element = tryElement("audiotestsrc");
    if (element) {
      return element;
    }else{
      qWarning() << "audiotestsrc failed!";
    }
     */

    //allow overrides from the application's configuration file
    element = tryOverrideForKey("audiosrc");
    if (element) {
      return element;
    }

    //use gconf on non-kde environments
    if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
      element = tryElement("gconfaudiosrc");
      if (element)
        return element;
    }

    //for kde environments try to do what phonon does:
    //first try pulseaudio,
    element = tryElement("pulsesrc");
    if (element) {
      return element;
    }

    //as a last resort, try gstreamer's autodetection
    element = tryElement("autoaudiosrc");
    return element;
  }

  QGst::ElementPtr DefaultElementFactory::makeAudioOutputElement() {
    QGst::ElementPtr element;

    /*
    // UNCOMENT FOR DEBUG ... test output to file
      element = tryFileElement("filesink", "sink.out");
      if (element) {
        return element;
      }
     */


    //allow overrides from the application's configuration file
    element = tryOverrideForKey("audiosink");
    if (element) {
      return element;
    }

    //use gconf on non-kde environments
    if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
      element = tryElement("gconfaudiosink");
      if (element) {
        element->setProperty("profile", 2 /*chat*/);
        return element;
      }
    }

    //for kde environments try to do what phonon does:
    //first try pulseaudio,
    element = tryElement("pulsesink");
    if (element) {
      //TODO set category somehow...
      return element;
    }

    //as a last resort, try gstreamer's autodetection
    element = tryElement("autoaudiosink");
    return element;
  }

  QGst::ElementPtr DefaultElementFactory::makeVideoCaptureElement() {
    QGst::ElementPtr element;

    //allow overrides from the application's configuration file
    element = tryOverrideForKey("videosrc");
    if (element) {
      return element;
    }

    //use gconf on non-kde environments
    if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
      element = tryElement("gconfvideosrc");
      if (element)
        return element;
    }

    //TODO implement phonon settings

    //as a last resort, try gstreamer's autodetection
    element = tryElement("autovideosrc");
    return element;
  }

  QGst::ElementPtr DefaultElementFactory::makeVideoOutputElement() {
    return QGst::ElementFactory::make("qwidgetvideosink"); //FIXME not always the best sink
  }

  QGst::ElementPtr DefaultElementFactory::tryFileElement(const char *name, const QString & file) {
    QGst::ElementPtr element = QGst::ElementFactory::make(name);
    if (!element) {
      qDebug() << "DefaultElementFactory: Could not make element" << name;
      return element;
    }

    if (!file.isEmpty()) {
      try {
        element->setProperty("location", file);
      } catch (const std::logic_error & error) {
        qDebug() << "DefaultElementFactory: FIXME: Element" << name << "doesn't support string location property";
      }
    }

    if (!element->setState(QGst::StateReady)) {
      qDebug() << "DefaultElementFactory: Element" << name << "with device string" << file << "doesn't want to become ready";
      return QGst::ElementPtr();
    }

    qDebug() << "DefaultElementFactory: Using element" << name << "with device string" << file;
    return element;
  }

  QGst::ElementPtr DefaultElementFactory::tryElement(const char *name, const QString & device) {
    QGst::ElementPtr element = QGst::ElementFactory::make(name);
    if (!element) {
      qDebug() << "DefaultElementFactory: Could not make element" << name;
      return element;
    }

    if (!device.isEmpty()) {
      try {
        element->setProperty("device", device);
      } catch (const std::logic_error & error) {
        qDebug() << "DefaultElementFactory: FIXME: Element" << name << "doesn't support string device property";
      }
    }

    if (!element->setState(QGst::StateReady)) {
      qDebug() << "DefaultElementFactory: Element" << name << "with device string" << device << "doesn't want to become ready";
      return QGst::ElementPtr();
    }

    qDebug() << "DefaultElementFactory: Using element" << name << "with device string" << device;
    return element;
  }

  QGst::ElementPtr DefaultElementFactory::tryOverrideForKey(const char *keyName) {
    QGst::ElementPtr element;
    return element;
  }

}

#include "defaultelementfactory.moc"

