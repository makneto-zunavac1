/*
 * This file is based on Collabora example
 *  
 * Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 * 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "deviceelementfactory.h"
#include "elementfactory.h"
#include "defaultelementfactory.h"
#include <QGst/Bin>
#include <QGst/ElementFactory>

namespace MaknetoBackend {
  
  IElementFactory *DeviceElementFactory::factoryInstance = NULL;
  
  QGst::ElementPtr DeviceElementFactory::makeAudioCaptureElement() {
    QGst::ElementPtr element = getInstalledFactory()->makeAudioCaptureElement();
    return element;
  }

  QGst::ElementPtr DeviceElementFactory::makeAudioOutputElement() {
    QGst::ElementPtr element = getInstalledFactory()->makeAudioOutputElement();
    return element;
  }

  QGst::ElementPtr DeviceElementFactory::makeVideoCaptureElement() {
    QGst::ElementPtr element = getInstalledFactory()->makeVideoCaptureElement();
    return element;
  }
  
  QGst::ElementPtr DeviceElementFactory::makeVideoOutputElement() {
    QGst::ElementPtr element = getInstalledFactory()->makeVideoOutputElement();
    return element;
  }  
  
  void DeviceElementFactory::installFactory(IElementFactory* factory) {
    if (factoryInstance)
      delete factoryInstance;
    factoryInstance = factory;
  }

  void DeviceElementFactory::destroyAllFactories() {
    if (factoryInstance)
      delete factoryInstance;
  }

  IElementFactory *DeviceElementFactory::getInstalledFactory() {
    if (!factoryInstance){
      factoryInstance = new DefaultElementFactory();
    }
    return factoryInstance;
  }
}