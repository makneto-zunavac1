/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Vojta Kulicka <vojtechkulicka@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#include <TelepathyQt4/ChannelRequest>
#include <TelepathyQt4/PendingChannelRequest>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/StreamedMediaChannel>
#include <TelepathyQt4/Message>
#include <TelepathyQt4/PendingSendMessage>
#include <TelepathyQt4/Contact>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/ReferencedHandles>

#include <QString>

#include "session.h"
#include "voip/callchannelhandler.h"
#include "telepathy-client.h"
#include <TelepathyQt4/FileTransferChannel>
#include <TelepathyQt4/IncomingFileTransferChannel>
#include <TelepathyQt4/OutgoingFileTransferChannel>

using namespace MaknetoBackend;

//Constructor for sessions created as result of an incoming channel

// FIXME: we should do something if own account gone to offline...
Session::Session(const Tp::ChannelPtr &channel, SesstionTypes chatType, QObject *parent)
: QObject(parent) {
  m_contacts = new QSet<Tp::ContactPtr > ();
  m_account = TelepathyClient::Instance()->getAccountforChannel(channel);

  m_chatType = (SessionTypeNone | chatType);

  m_mediaChannel.reset();
  m_textChannel.reset();
  m_fileTransferChannel.reset();
  m_callChannelHandler = NULL;
  m_pendingCallRequest = NULL;

  m_myContact = channel->groupSelfContact();

  //set the peer's contactPtr
  if (m_chatType != SessionTypeMuc) {
    //for one-to-one chat contains two contacts, me and peer
    //for calls there is just the peer contact
    Tp::Contacts contacts = channel->groupContacts();

    foreach(Tp::ContactPtr contact, contacts) {
      if (contact == channel->groupSelfContact())
        continue;
      else {
        m_contact = contact;
        qDebug() << "Session: " << contact->alias() << " contactId: " << contact->id();
      }
    }
    if (!m_contact) {
      qWarning() << "Session: start session with NULL contact! Channel has"<<contacts.size()<<"contacts";
    }

    m_sessionName = m_contact ? m_contact->alias() : "undefined";
    m_uniqueSessionName = Session::sessionName(m_account, m_contact);
  }//MUC
  else {
    Tp::Contacts contacts = channel->groupContacts();

    foreach(Tp::ContactPtr contact, contacts) {
      if (contact == channel->groupSelfContact())
        continue;
      else {
        m_contacts->insert(contact);
        qDebug() << "Session: " << contact->alias() << "   id: " << contact->id();
      }
    }
    m_sessionName = channel->targetId();
    m_uniqueSessionName = Session::sessionNameForMuc(m_account, channel->targetId());
  }

  //now is only informative
  switch (m_chatType) {

    case SessionTypeText:
      qDebug() << "Session: Text";
      break;
    case SessionTypeAudio:
      qDebug() << "Session: Audio";
      break;
    case SessionTypeVideo:
      qDebug() << "Session: Video";
      break;
    case SessionTypeMuc:
      qDebug() << "Session: MUC";
      break;
    default:
      qDebug() << "Default session type. Something is wrong";
      Q_ASSERT(false);
  }

  if (m_textChannel || m_mediaChannel)
    qDebug() << "Session: initialized";
}


//Constructor for sessions created as result of an outgoing channel

Session::Session(const Tp::AccountPtr &account, const Tp::ContactPtr &contact, SesstionTypes sessionType, QObject *parent)
: QObject(parent),
m_account(account),
m_contact(contact) {
  //most likely is not neccessary here
  m_contacts = new QSet<Tp::ContactPtr > ();
  m_myContact = account->connection()->selfContact();

  m_chatType = (SessionTypeNone | sessionType | SessionTypeText); // start text channel in any case!
  emit sessionTypeChanged();

  m_mediaChannel.reset();
  m_textChannel.reset();
  m_fileTransferChannel.reset();
  m_callChannelHandler = NULL;
  m_pendingCallRequest = NULL;

  //set the peer's contactPtr
  m_sessionName = contact->alias();
  m_uniqueSessionName = Session::sessionName(m_account, m_contact); // FIXME: how to get name for outgoing MUC here?
  qDebug() << "Session: name " << m_sessionName << " id: " << m_uniqueSessionName << "";

  if (m_chatType.testFlag(SessionTypeText))
    startTextChat();

  if (m_chatType.testFlag(SessionTypeAudio) || m_chatType.testFlag(SessionTypeVideo))
    startMediaCall(m_chatType.testFlag(SessionTypeVideo));

  if (m_chatType.testFlag(SessionTypeMuc)) {
    qDebug() << "Session: start session type MUC, TODO: create chat room";
    //startTextChatroom();
  }

  qDebug() << "Session: initialized";
}

Session::~Session() {
  if (m_callChannelHandler && (!m_mediaChannel.isNull())) // FIXME: it is correct?
    delete m_callChannelHandler;
}

void Session::onAddCapability(Session::SessionType chatType) {

  switch (chatType) {

    case SessionTypeText:
      startTextChat();
      break;
    case SessionTypeAudio:
      startMediaCall(false);
      break;
    case SessionTypeVideo:
      startMediaCall(true);
      break;
    case SessionTypeMuc:
      qDebug() << "Session: FIXME, MUC";
      //startTextChatroom();
      break;
    default:
      qDebug() << "Session: undefined session type. Something is wrong.";
      Q_ASSERT(false);
  }
  m_chatType = m_chatType | chatType;
  emit sessionTypeChanged();
}


///-----------------------------------------------------------------------------------------------------------------------
///**************************************************	Text Chat	******************************************************
///-----------------------------------------------------------------------------------------------------------------------

void Session::startTextChat() {
  qDebug() << "Session: Start text chat (one-to-one)";
  //returns PendingChannel object, which emits succeeded signal, see if it would be better
  //to wait for that instead of distributing the channel through handleChannels. 
  m_account->ensureTextChat(m_contact, QDateTime::currentDateTime(), TelepathyClient::Instance()->getPreferredHandler());
}

void Session::onMessageReceived(const Tp::ReceivedMessage &message) {
  //qDebug() << "Message received: " << message.text();
  QString sender;
  if (message.sender() && message.text() != QString()) {
    sender = message.sender()->alias();
    emit messageReceived(message.text(), sender);

    //message acknowledging causes the messages to be deleted from pendingMessageQueue
    m_textChannel->acknowledge(QList<Tp::ReceivedMessage > () << message);
  }
}

void Session::sendMessage(const QString& message) {

  if (message == QString())
    return;

  /* client app can send message before we get text channel (slot onTextChannelReady), 
   * so, we enqueue it into outgoing queue and send it later...
   */
  if (m_textChannel.isNull()) {
    qWarning() << "Sesssion: m_textChannel is not ready, adding into queue (" << _outgoingQueue.size() << ")";
    _outgoingQueue.push_back(message);
    startTextChat();
    return;
  }

  //qDebug() << "Session: Sending message: " << message;
  connect(m_textChannel->send(message), SIGNAL(finished(Tp::PendingOperation *)),
    this, SLOT(onMessageSent(Tp::PendingOperation *)));
}

void Session::onMessageSent(Tp::PendingOperation *op) {
  Tp::PendingSendMessage *pendingMessage = qobject_cast<Tp::PendingSendMessage *>(op);
  if (op->isError()) {
    qWarning() << "Session: Error sending message: " << pendingMessage->message().text()
      << "\n     Error: " << pendingMessage->errorName() << "/" << pendingMessage->errorMessage();

    /*
     * if text channel is Cancelled, try create new one and resend message
     */
    sendErrors++;
    if (sendErrors < 10 && pendingMessage->errorName() == "org.freedesktop.Telepathy.Error.Cancelled") {
      m_textChannel.reset();
      qWarning() << "Session: try resend. attempt" << sendErrors;
      sendMessage(pendingMessage->message().text());
      return;
    }

    emit sendingError(pendingMessage->message().text(), pendingMessage->errorMessage());
  } else {
    sendErrors = 0;
    emit messageSent(pendingMessage->message().text());
  }
}

void Session::onChatStateChanged(Tp::ContactPtr contact, Tp::ChannelChatState chatState) {
  if (!m_textChannel.isNull() && contact == m_textChannel->groupSelfContact()) // don't publish changes from himself
    return;

  //qDebug() << "Session: Channel chat state changed for: " << contact->alias() << "   to: " << chatState;
  //the contact alias is for MUC - I think
  switch (chatState) {

    case Tp::ChannelChatStateComposing:
      emit chatStateChanged(ChatStateComposing, contact->alias());
      break;
    case Tp::ChannelChatStateGone:
      emit chatStateChanged(ChatStateGone, contact->alias());
      break;
    case Tp::ChannelChatStateActive:
      emit chatStateChanged(ChatStateActive, contact->alias());
      break;
    case Tp::ChannelChatStateInactive:
      emit chatStateChanged(ChatStateInactive, contact->alias());
      break;
    case Tp::ChannelChatStatePaused:
      emit chatStateChanged(ChatStatePaused, contact->alias());
      break;
    default:
      qWarning() << "Session: Unknown chat state";
  }
}

void Session::changeChatState(ChatState state) {
  if (m_textChannel.isNull())
    return;

  if (!m_textChannel->hasChatStateInterface()) {
    chatStateReported = true;
    if (!chatStateReported) // change state is ivoked often, report this message only ones
      qWarning() << "Session: text channel hasn't state interface. Ignore chat state change request.";
    return;
  }

  switch (state) {
    case ChatStateComposing:
      m_textChannel->requestChatState(Tp::ChannelChatStateComposing);
      break;
    case ChatStateGone:
      m_textChannel->requestChatState(Tp::ChannelChatStateGone);
      break;
    case ChatStateActive:
      m_textChannel->requestChatState(Tp::ChannelChatStateActive);
      break;
    case ChatStateInactive:
      m_textChannel->requestChatState(Tp::ChannelChatStateInactive);
      break;
    case ChatStatePaused:
      m_textChannel->requestChatState(Tp::ChannelChatStatePaused);
      break;
    default:
      qWarning() << "Session: Unknown chat state (setter)";
  }
}


///-----------------------------------------------------------------------------------------------------------------------
///**************************************************	MUC	  ************************************************************
///-----------------------------------------------------------------------------------------------------------------------


//probably useless

void Session::startTextChatroom(const QString &chatRoomName) {
  qDebug() << "Session: Start text chat chatroom (MUC)";
  m_account->ensureTextChatroom(chatRoomName, QDateTime::currentDateTime(), TelepathyClient::Instance()->getPreferredHandler());
}

// this is method for one-to-one chat and MUC chat...

void Session::onTextChannelReady(Tp::TextChannelPtr& textChannel, bool isMUC) {
  qDebug() << "Session: Text Channel ready";
  if (m_textChannel.isNull()) {
    m_textChannel = textChannel;

    Tp::Features textFeatures = Tp::Features() << Tp::TextChannel::FeatureCore
      << Tp::TextChannel::FeatureMessageQueue
      << Tp::TextChannel::FeatureMessageSentSignal
      << Tp::TextChannel::FeatureChatState
      << Tp::TextChannel::FeatureMessageCapabilities;

    m_textChannel->becomeReady(textFeatures); // FIXME: wait while channel is not ready...

    isMUC ? m_chatType = SessionTypeMuc
      : m_chatType.operator|=(SessionTypeText);
    emit sessionTypeChanged();

    connect(m_textChannel.data(),
      SIGNAL(messageReceived(const Tp::ReceivedMessage &)),
      SLOT(onMessageReceived(const Tp::ReceivedMessage &)));

    connect(m_textChannel.data(),
      SIGNAL(chatStateChanged(Tp::ContactPtr, Tp::ChannelChatState)),
      SLOT(onChatStateChanged(Tp::ContactPtr, Tp::ChannelChatState)));

    foreach(Tp::ReceivedMessage message, m_textChannel->messageQueue()) {
      emit messageReceived(message.text(), message.sender()->alias());
      m_textChannel->acknowledge(QList<Tp::ReceivedMessage > () << message);
    }

    // flush the outgoing queue
    if (!_outgoingQueue.isEmpty())
      qDebug() << "Sesssion: m_textChannel is now ready, flushing outgoing queue " << _outgoingQueue.size();

    while (!_outgoingQueue.isEmpty()) {
      QString item = _outgoingQueue.front();
      _outgoingQueue.pop_front();
      // FIXME: add timestamp to queue item
      sendMessage(item);
    }

  }
}

bool Session::isMUC() {
  return m_chatType.testFlag(SessionTypeMuc);
}


///-----------------------------------------------------------------------------------------------------------------------
///**************************************************	VoIP	**********************************************************
///-----------------------------------------------------------------------------------------------------------------------

void Session::startMediaCall(bool video) {
  qDebug() << "Session: start" << (video ? "Video" : "Audio") << "call (" << m_uniqueSessionName << ")";

  if (m_contact.isNull()) {
    qWarning() << "Session: m_contact shared pointer is NULL !!!";
    onCallError("INTERNAL ERROR: session contact is null");
    return;
  }
  if (m_account->connection().isNull()) {
    qWarning() << "Session: m_account don't have connection !!!";
    onCallError("Account is has no connection");
    return;
  }
  if (m_pendingCallRequest != NULL) {
    qWarning() << "Session: We have another active call request. Hangup first.";
    onCallError("We have another active call request. Hangup first.");
    return;
  }

  QVariantMap request;
  request.insert(TELEPATHY_INTERFACE_CHANNEL ".ChannelType",
    TELEPATHY_INTERFACE_CHANNEL_TYPE_STREAMED_MEDIA);
  request.insert(TELEPATHY_INTERFACE_CHANNEL ".TargetHandleType", Tp::HandleTypeContact);
  request.insert(TELEPATHY_INTERFACE_CHANNEL ".TargetHandle", m_contact->handle()[0]);
  request.insert(TELEPATHY_INTERFACE_CHANNEL_TYPE_STREAMED_MEDIA ".InitialAudio", true);
  if (video)
    request.insert(TELEPATHY_INTERFACE_CHANNEL_TYPE_STREAMED_MEDIA ".InitialVideo", true);

  m_chatType |= SessionTypeAudio | SessionTypeCallPending | (video ? SessionTypeVideo : SessionTypeNone);
  emit sessionTypeChanged();

  // store pending operation for later use...
  m_pendingCallRequest = m_account->ensureChannel(request, QDateTime::currentDateTime(), TelepathyClient::Instance()->getPreferredHandler());
  connect(m_pendingCallRequest, SIGNAL(finished(Tp::PendingOperation*)), this, SLOT(callChannelRequestFinished(Tp::PendingOperation*)));
}

/**
 *  this method is just for checking errors, when ensureChannel failed for some reason
 */
void Session::callChannelRequestFinished(Tp::PendingOperation *op) {
  qDebug() << "Session: callChannelRequestFinished";
  if (op->isError() && op->errorName() != "org.freedesktop.Telepathy.Error.Cancelled") {
    qWarning() << "Session: Error occured creating call" << op->errorName() << op->errorMessage();
    onCallError(op->errorMessage());

    if (m_chatType.testFlag(SessionTypeVideo))
      m_chatType.operator^=(SessionTypeVideo);
    if (m_chatType.testFlag(SessionTypeAudio))
      m_chatType.operator^=(SessionTypeAudio);
  }

  if (m_chatType.testFlag(SessionTypeCallPending))
    m_chatType.operator^=(SessionTypeCallPending);
  emit sessionTypeChanged();
  m_pendingCallRequest = NULL;
}

void Session::onIncomingCallChannel(Tp::StreamedMediaChannelPtr& mediaChannel, SessionType mediaType) {

  qDebug() << "Session: Incoming" << (mediaType == SessionTypeVideo ? "Video" : "Audio") << "channel";

  if (!m_mediaChannel.isNull()) {
    qWarning() << "previous channel is not cleaned?!";
    return;
  }

  m_chatType.operator|=(mediaType);
  m_mediaChannel = mediaChannel;
  emit sessionTypeChanged();

  m_callChannelHandler = new CallChannelHandler(m_mediaChannel, this);

  connect(m_callChannelHandler, SIGNAL(callEnded(QString)),
    this, SLOT(onCallEnded(QString)));
  connect(m_callChannelHandler, SIGNAL(error(QString)),
    this, SLOT(onCallError(QString)));
  connect(m_callChannelHandler, SIGNAL(participantLeft(CallParticipant*)),
    this, SLOT(onCallParticipantLeft(CallParticipant*)));
  connect(m_callChannelHandler, SIGNAL(participantJoined(CallParticipant*)),
    this, SLOT(onCallParticipantJoined(CallParticipant*)));

  if (m_mediaChannel->isRequested())
    emit callReady();
  else
    emit incomingCall();
}

void Session::acceptCall() {
  qDebug() << "Session: Accept Call";
  if (m_mediaChannel.isNull()) {
    qWarning() << "Session: we can't accept call, media channel is null";
    return;
  }
  if (m_callChannelHandler == NULL) {
    qWarning() << "Session: we can't accept call, channel handler is null";
    return;
  }

  m_callChannelHandler->accept();
}

void Session::rejectCall() {
  qDebug() << "Session: Rejecting call from: " << m_contact->alias();
  if (!m_mediaChannel.isNull()) {
    m_mediaChannel->hangupCall();
    m_mediaChannel.reset();
  }
  if (m_chatType.testFlag(SessionTypeVideo))
    m_chatType.operator^=(SessionTypeVideo);
  if (m_chatType.testFlag(SessionTypeAudio))
    m_chatType.operator^=(SessionTypeAudio);
  emit sessionTypeChanged();
}

void MaknetoBackend::Session::onCallEnded(const QString &message) {
  qDebug() << "Session: Call ended" << message;
  //TODO figure out what to do with the channel
  if (m_callChannelHandler)
    m_callChannelHandler->deleteLater(); //TODO check out

  m_mediaChannel.reset();
  if (m_chatType.testFlag(SessionTypeVideo))
    m_chatType.operator^=(SessionTypeVideo);
  if (m_chatType.testFlag(SessionTypeAudio))
    m_chatType.operator^=(SessionTypeAudio);
  emit sessionTypeChanged();
  emit callEnded(message);
}

void Session::onHangup() {
  qDebug() << "Session: Hang up";
  //TODO check if the call still lasts
  if (!m_mediaChannel.isNull() && m_mediaChannel->isValid()) {
    m_callChannelHandler->hangup();
  }
  if (m_pendingCallRequest != NULL) {
    m_pendingCallRequest->cancel();
  }

  m_mediaChannel.reset();
  if (m_chatType.testFlag(SessionTypeVideo))
    m_chatType.operator^=(SessionTypeVideo);
  if (m_chatType.testFlag(SessionTypeAudio))
    m_chatType.operator^=(SessionTypeAudio);
  emit sessionTypeChanged();
}

void Session::onCallError(const QString &msg) {
  // just forward to higher layer
  qWarning() << "Session: call error:" << msg;
  emit callError(msg);
}

void Session::onCallParticipantLeft(CallParticipant *participant) {
  // FIXME: should we do something? all streams are removed already
}

void Session::onCallParticipantJoined(CallParticipant *participant) {
  connect(participant, SIGNAL(videoStreamAdded(CallParticipant*)),
    this, SLOT(onVideoStreamAdded(CallParticipant*)));
  connect(participant, SIGNAL(videoStreamRemoved(CallParticipant*)),
    this, SLOT(onVideoStreamRemoved(CallParticipant*)));
  connect(participant, SIGNAL(audioStreamAdded(CallParticipant*)),
    this, SLOT(onAudioStreamAdded(CallParticipant*)));
}

void Session::onAudioStreamAdded(CallParticipant *participant) {
  if (!participant->isMyself())
    emit remoteAudioStreamAdded();
}

void Session::onVideoStreamAdded(CallParticipant *participant) {
  if (!participant->videoOutElemPtr()) {
    qWarning() << "Session: video stream added, but we dont have output element!";
    return;
  }

  if (participant->isMyself()) {
    Q_EMIT videoPreviewAvailable(participant->videoOutElemPtr());
  } else {
    Q_EMIT videoAvailable(participant->videoOutElemPtr());
  }
}

void Session::onVideoStreamRemoved(CallParticipant *participant) {
  if (!participant->videoOutElemPtr()) {
    qWarning() << "Session: video stream added, but we dont have output element!";
    return;
  }

  if (participant->isMyself()) {
    Q_EMIT videoPreviewRemoved(participant->videoOutElemPtr());
  } else {
    Q_EMIT videoRemoved(participant->videoOutElemPtr());
  }
}


///-----------------------------------------------------------------------------------------------------------------------
///**********************************************	File Transfer	******************************************************
///-----------------------------------------------------------------------------------------------------------------------

void Session::startFileTransfer(const QString &filename) {
  if (m_chatType.testFlag(SessionTypeMuc)) {

    qDebug() << "Session: File transfer does not work with MUC";
    return;
  }
  if (!m_fileTransferChannel.isNull() && m_fileTransferChannel->state() != Tp::FileTransferStateNone) {
    qDebug() << "Session: There already is an unfinished file transfer.";
    emit fileTransferAlreadyInProgress();
    return;
  }

  m_fileTransferFile.setFileName(filename);
  QFileInfo fileInfo(m_fileTransferFile);
  Tp::FileTransferChannelCreationProperties fileTransferProperties = Tp::FileTransferChannelCreationProperties(fileInfo.fileName(),
    QString("application/octet-stream"),
    (qulonglong) fileInfo.size());
  m_account->createFileTransfer(m_contact,
    fileTransferProperties,
    QDateTime::currentDateTime(),
    TelepathyClient::Instance()->getPreferredHandler());
}

void Session::onFileTransfer(Tp::FileTransferChannelPtr& fileTransferChannel) {
  //check if there is not an active file transfer
  if (!m_fileTransferChannel.isNull() && m_fileTransferChannel->state() != Tp::FileTransferStateNone) {
    qDebug() << "Session: Incoming file transfer ignored, there is already one in progress";
    return;
  }

  m_fileTransferChannel = fileTransferChannel;
  if (m_fileTransferChannel->isRequested()) {

    if (!m_fileTransferFile.open(QIODevice::ReadOnly)) { //TODO let GUI know
      qDebug() << "Session: error opening file: " << m_fileTransferFile.fileName();
      m_fileTransferChannel->cancel();
      m_fileTransferChannel.reset();
      return;
    }
    Tp::OutgoingFileTransferChannelPtr outgoingFileTransfer = Tp::OutgoingFileTransferChannelPtr::dynamicCast(m_fileTransferChannel);
    if (outgoingFileTransfer)
      outgoingFileTransfer->provideFile(&m_fileTransferFile);

    connect(m_fileTransferChannel.data(), SIGNAL(stateChanged(Tp::FileTransferState, Tp::FileTransferStateChangeReason)),
      this, SLOT(onFileTransferStateChanged(Tp::FileTransferState, Tp::FileTransferStateChangeReason)));
    connect(m_fileTransferChannel.data(), SIGNAL(transferredBytesChanged(qulonglong)),
      this, SLOT(onFileTransferTransferredBytesChanged(qulonglong)));
    emit fileTransferCreated();
  } else {
    emit incomingFileTransfer(fileTransferChannel->fileName());
  }
}

void Session::onAcceptFileTransfer(bool accept, QString filename) {
  if (accept) {
    Tp::IncomingFileTransferChannelPtr incomingFT = Tp::IncomingFileTransferChannelPtr::dynamicCast(m_fileTransferChannel);

    if (incomingFT) {

      if (filename == QString()) {
        filename = incomingFT->fileName();
      }
      m_fileTransferFile.setFileName(filename);
      if (m_fileTransferFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Session: Could not open/create file: " << m_fileTransferFile.fileName();
      }
      incomingFT->acceptFile(0, &m_fileTransferFile); //the first number is the offset of the file

      connect(m_fileTransferChannel.data(), SIGNAL(stateChanged(Tp::FileTransferState, Tp::FileTransferStateChangeReason)),
        this, SLOT(onFileTransferStateChanged(Tp::FileTransferState, Tp::FileTransferStateChangeReason)));
      connect(m_fileTransferChannel.data(), SIGNAL(transferredBytesChanged(qulonglong)),
        SLOT(onFileTransferTransferredBytesChanged(qulonglong)));

    }
  } else {
    //return PendingOperation object - possibly check if it finished ok
    m_fileTransferChannel->cancel(); //TODO check if this is the method to call, or just wait it out - not do anything
    m_fileTransferChannel.reset();
  }
}

void Session::onFileTransferStateChanged(Tp::FileTransferState state, Tp::FileTransferStateChangeReason reason) {
  Q_UNUSED(reason);
  switch (state) {
    case Tp::FileTransferStateAccepted:
      //TODO provide file
      emit fileTransferStateChanged(fileTransferAccepted);
      break;
    case Tp::FileTransferStateCancelled:
      emit fileTransferStateChanged(fileTransferCancelled);
      m_fileTransferChannel.reset();
      m_fileTransferFile.close();
      break;
    case Tp::FileTransferStateCompleted:
      emit fileTransferStateChanged(fileTransferCompleted);
      m_fileTransferChannel.reset();
      m_fileTransferFile.close();
      break;
    case Tp::FileTransferStateOpen:
      emit fileTransferStateChanged(fileTransferOpen);
      break;
    default: qDebug() << "Session: File transfer channel in an for now unsupported state";

  }
}

QString Session::sessionNameForChannel(const Tp::AccountPtr &account, const Tp::ChannelPtr &channel) {
  Tp::Contacts allContacts = channel->groupContacts();
  if (allContacts.size() == 2) { //one-to-one chat

    foreach(Tp::ContactPtr contact, allContacts) {
      if (contact != channel->groupSelfContact())
        return sessionName(account, contact);
    }
  } else if (allContacts.size() == 1) {
    Tp::Contacts pendingContacts = channel->groupRemotePendingContacts();
    if (pendingContacts.size() == 1) {
      return (account ? account->uniqueIdentifier() : "undefinedAccount") + "/" + (pendingContacts.begin()->constData()->id());
    }
  }
  //groupChat
  return sessionNameForMuc(account, channel->targetId());
}

QString Session::sessionName(const Tp::AccountPtr &account, const Tp::ContactPtr &contact) {
  return (account ? account->uniqueIdentifier() : "undefinedAccount") + "/" + (contact ? contact->id() : "undefinedContact");
}

QString Session::sessionNameForMuc(const Tp::AccountPtr &account, const QString &mucName) {
  return (account ? account->uniqueIdentifier() : "undefinedAccount") + "/MUC/" + (mucName);
}

void Session::onFileTransferTransferredBytesChanged(qulonglong bytes) {
  qDebug() << "Session: Transferred bytes: " << bytes;
  emit fileTransferTransferredBytesChanged(bytes);
}

QString Session::getName() {
  return m_sessionName;
}

QString Session::getUniqueName() {
  return m_uniqueSessionName;
}

QString Session::getMyId() {
  return m_myContact->id();
}

QString Session::getMyNick() {
  return m_myContact->alias();
}

Session::SesstionTypes Session::getSessionType() {
  return m_chatType;
}

QString Session::getIcon() {
  if (m_contact.isNull())
    return QString();
  return m_contact->avatarData().fileName;
}


#include "session.moc"
