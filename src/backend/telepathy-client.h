/*
 * Telepathy client is a class for communication via Telepathy
 *
 * Copyright (C) 2011 Vojta Kulicka <vojtechkulicka@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef MAKNETO_TELEPATHY_CLIENT
#define MAKNETO_TELEPATHY_CLIENT

#include <QObject>
#include <QMap>

#include <TelepathyQt4/Debug>
#include <TelepathyQt4/Constants>
#include <TelepathyQt4/Types>
#include <TelepathyQt4/Global>

#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/ClientRegistrar>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/AbstractClientHandler>
#include "session.h"
#include "makneto-backend.h"

class QString;
class QModelIndex;

namespace Tp {
  class PendingOperation;
  class StreamedMediaChannel;
}

namespace MaknetoBackend {

  class AccountsModel;

  class MAKNETO_EXPORT TelepathyClient : public QObject, public Tp::AbstractClientHandler {
    Q_OBJECT;
  public:

    enum {
      Available,
      Away,
      Busy,
      Invisible,
      Offline
    };

    static TelepathyClient* Instance();
    static TelepathyClient* Instance(const Tp::ChannelClassSpecList &channelFilter,
	    Tp::AccountManagerPtr AM,
	    AccountsModel *accountsModel,
	    const QString &clientName = QString(),
	    QObject *parent = 0);

    ~TelepathyClient();

    //TELEPATHY HANDLER CLIENT CODE START 
    virtual void handleChannels(const Tp::MethodInvocationContextPtr<> &context,
	    const Tp::AccountPtr & account,
	    const Tp::ConnectionPtr & connection,
	    const QList< Tp::ChannelPtr > & channels,
	    const QList< Tp::ChannelRequestPtr > & requestsSatisfied,
	    const QDateTime & userActionTime,
	    const Tp::AbstractClientHandler::HandlerInfo & handlerInfo
	    );

    bool bypassApproval() const;
    //TELEPATHY HANDLER CLIENT CODE END 

    void setAccountManager(const Tp::AccountManagerPtr &);
    AccountsModel* accountsModel();
    Tp::AccountManagerPtr accountManager();
    QString sessionIdforIndex(const QModelIndex &);
    Tp::AccountPtr getAccountforChannel(const Tp::ChannelPtr &);
    QString getPreferredHandler();


Q_SIGNALS:
    void sessionCreated(MaknetoBackend::Session *, bool extendExisting, bool requested);
    void sessionAlreadyExists(MaknetoBackend::Session *);
    //void incomingCall(MaknetoBackend::Session *); // TODO: it is used? remove...

    public
Q_SLOTS:
    void onNewTextChatRoomRequested(const QModelIndex &, const QString &);
    void setAccountPresence(QModelIndex, int, const QString & = QString());

    void onSessionEnded(const QString &name) {
      Q_UNUSED(name);
    }//TODO
    void onMediaChannelReady(Tp::PendingOperation *);
    void onSessionRequested(const QModelIndex &, MaknetoBackend::Session::SessionType);
    void onSessionRequested(const QString &accountId, const QString &contactId, MaknetoBackend::Session::SessionType);

  private:

    TelepathyClient(const Tp::ChannelClassSpecList &channelFilter, 
	    Tp::AccountManagerPtr AM, 
	    AccountsModel *accountsModel, 
	    const QString &clientName = QString(),
	    QObject* parent = 0);
    
    Q_DISABLE_COPY(TelepathyClient);
    static TelepathyClient *m_instance;


    Session* findSession(const QString &);
    //QString sessionNameForChannel(const Tp::ChannelPtr &);

    Tp::AccountManagerPtr m_AM;
    AccountsModel *m_accountsModel;
    QMap<QString, Session *> *m_sessions;
    QString clientName;
  };
}

#endif