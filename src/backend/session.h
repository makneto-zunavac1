/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Vojta Kulicka <vojtechkulicka@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */


#ifndef MAKNETOLIB_SESSION_H
#define MAKNETOLIB_SESSION_H

#include <QtCore/QObject>
#include <QList>
#include <QFile>

#include <TelepathyQt4/Contact>
#include <TelepathyQt4/Constants>
#include <TelepathyQt4/Types>
#include <TelepathyQt4/FileTransferChannel>
#include <TelepathyQt4/PendingChannelRequest>

#include "voip/callparticipant.h"
#include "makneto-backend.h"

class QString;
class QStringList;

namespace Tp {

  class PendingOperation;
  class Message;
  class ReceivedMessage;
  class Contact;
}

namespace MaknetoBackend {

  class CallChannelHandler;

  class MAKNETO_EXPORT Session : public QObject {

    Q_OBJECT
    Q_ENUMS(SessionType FileTransferState ChatState)
    Q_PROPERTY(SesstionTypes sessionType READ getSessionType NOTIFY sessionTypeChanged)

  public:
    enum SessionType {
      SessionTypeNone = 0x0, // 	00000
      SessionTypeText = 0x1, //		00001
      SessionTypeMuc = 0x2, //		00010
      SessionTypeAudio = 0x4, //	00100
      SessionTypeVideo = 0x8,	//	01000	
      SessionTypeCallPending = 0x10 // 10000	
    };

    Q_DECLARE_FLAGS(SesstionTypes, SessionType)

    enum FileTransferState {
      fileTransferAccepted,
      fileTransferCancelled,
      fileTransferCompleted,
      fileTransferOpen
    };

    enum ChatState {
      ChatStateComposing = Tp::ChannelChatStateComposing,
      ChatStateActive = Tp::ChannelChatStateActive,
      ChatStateInactive = Tp::ChannelChatStateInactive,
      ChatStateGone = Tp::ChannelChatStateGone,
      ChatStatePaused = Tp::ChannelChatStatePaused
    };


    Session(const Tp::ChannelPtr &, SesstionTypes, QObject *parent = 0);
    Session(const Tp::AccountPtr &, const Tp::ContactPtr &, SesstionTypes, QObject *parent = 0);
    virtual ~Session();

    QString getName();
    QString getUniqueName();
    QString getMyId();
    QString getMyNick();
    QString getIcon();
    SesstionTypes getSessionType();
    bool isMUC();

    void startTextChat();
    void startMediaCall(bool);
    void startTextChatroom(const QString &);
    void startFileTransfer(const QString &);

    static QString sessionNameForChannel(const Tp::AccountPtr &, const Tp::ChannelPtr &);
    static QString sessionNameForMuc(const Tp::AccountPtr &, const QString &);
    static QString sessionName(const Tp::AccountPtr &, const Tp::ContactPtr &);

    public
Q_SLOTS:
    void onAddCapability(SessionType chatType);
    //Text messaging
    void onTextChannelReady(Tp::TextChannelPtr &textChannel, bool);
    void onMessageReceived(const Tp::ReceivedMessage &);
    void sendMessage(const QString &text);
    void onMessageSent(Tp::PendingOperation *);
    void onChatStateChanged(Tp::ContactPtr, Tp::ChannelChatState);
    void changeChatState(MaknetoBackend::Session::ChatState);

    //VoIP
    void onIncomingCallChannel(Tp::StreamedMediaChannelPtr &mediaChannel, SessionType mediaType);
    void acceptCall();
    void rejectCall();
    void onCallEnded(const QString &);
    void onHangup();
    void onCallError(const QString &);
    void callChannelRequestFinished(Tp::PendingOperation*);
    void onCallParticipantLeft(CallParticipant *participant);
    void onCallParticipantJoined(CallParticipant *participant);
    void onVideoStreamAdded(CallParticipant *participant);
    void onVideoStreamRemoved(CallParticipant *participant);
    void onAudioStreamAdded(CallParticipant *participant);
    
    //File transfer
    void onFileTransfer(Tp::FileTransferChannelPtr &fileTransferChannel);
    void onAcceptFileTransfer(bool accept, QString filename = QString());
    void onFileTransferStateChanged(Tp::FileTransferState, Tp::FileTransferStateChangeReason);
    void onFileTransferTransferredBytesChanged(qulonglong);


Q_SIGNALS:
    //text messaging
    void messageReceived(const QString &, const QString &);
    void messageSent(const QString &message); // outgoing message was successfully send
    void sendingError(const QString &message, const QString &errorMessage); // outgoing message sending failed
    void chatStateChanged(MaknetoBackend::Session::ChatState, const QString &);
    void sessionTypeChanged();

    //VoIP
    void incomingCall();
    void callReady();
    void callEnded(const QString &);
    void callError(const QString &);
    void videoPreviewAvailable(QGst::ElementPtr videoOutElemPtr);
    void videoAvailable(QGst::ElementPtr videoOutElemPtr);
    void videoPreviewRemoved(QGst::ElementPtr videoOutElemPtr);
    void videoRemoved(QGst::ElementPtr videoOutElemPtr);
    void remoteAudioStreamAdded(); // signaling that application should stop beeping

    //file transfer
    void incomingFileTransfer(const QString &filename);
    void fileTransferCreated();
    void fileTransferStateChanged(FileTransferState);
    void fileTransferAlreadyInProgress();
    void fileTransferTransferredBytesChanged(qulonglong);

  private:
    Tp::TextChannelPtr m_textChannel;
    Tp::StreamedMediaChannelPtr m_mediaChannel;
    Tp::FileTransferChannelPtr m_fileTransferChannel;
    
    Tp::AccountPtr m_account;
    Tp::ContactPtr m_myContact;
    Tp::ContactPtr m_contact;
    QSet<Tp::ContactPtr> *m_contacts;

    QFile m_fileTransferFile;
    QString m_sessionName;
    QString m_uniqueSessionName;
    CallChannelHandler *m_callChannelHandler;
    Tp::PendingChannelRequest *m_pendingCallRequest;
    SesstionTypes m_chatType;
    QList<QString> _outgoingQueue;    
    bool chatStateReported;
    int sendErrors;
  };

  Q_DECLARE_OPERATORS_FOR_FLAGS(Session::SesstionTypes)

}

#endif // MAKNETOLIB_SESSION_H
