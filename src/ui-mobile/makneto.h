/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QtCore/QObject>
#include <QApplication>
#include <QDeclarativeView>
#include <QMainWindow>

// Gstreamer and QtGstQmlSink
#include <gst/gst.h>
#include <QtGstQmlSink/qmlvideosurfacegstsink.h>
#include <QtGstQmlSink/qmlgstvideoitem.h>

// backend
#include "../backend/telepathy-initializer.h"
#include "../backend/telepathy-client.h"

#include "contacts-model-proxy.h"
#include "contacts-sort-filter-model-proxy.h"
#include "session-model.h"
#include "notifications-model.h"
#include "mobile-gst-element-factory.h"
#include "sound-notificator.h"
#include "presence-manager.h"

#ifndef MAKNETO_H
#define	MAKNETO_H

class Makneto : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(Makneto)
public:
  Makneto(QApplication *app);
  virtual ~Makneto();
  bool init();

Q_SIGNALS:
  void modelsReady();
  void videoSurfaceReady(const QString &type, const QString &sessionId, QmlPainterVideoSurface * surface);
  void videoSurfaceRemoved(const QString &type, const QString &sessionId);  

  private
Q_SLOTS:
  void onQmlStatusChanged(QDeclarativeView::Status status);
  void onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient* client);

  void onVideoPreviewAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void onVideoAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void onVideoPreviewRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void onVideoRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);  

private:
  void onViewReady();
  void showWindow();

  QApplication* _app;
  QDeclarativeView *_qmlView;
  MainWindow *_window;
  ContactsModelProxy *_contactsModel;
  ContactsSortFilterModelProxy* _contactsProxy;
  SessionModel* _sessionModel;
  NotificationsModel *_notificationsModel;
  QSystemTrayIcon *_trayIcon;
  MobileGstElementFactory *_ownDeviceFactory;
  SoundNotificator *_soundNotificator;
  PresenceManager *_presenceManager;
};

#endif	/* MAKNETO_H */

