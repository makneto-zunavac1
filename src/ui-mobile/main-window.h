/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QApplication>
#include <QCloseEvent>
#include <QMenu>

#ifndef MAINWINDOW_H
#define	MAINWINDOW_H

class MainWindow : public QMainWindow {
  Q_OBJECT
  Q_DISABLE_COPY(MainWindow)

public:
  MainWindow(QApplication *app, QObject *parent = 0);
  virtual ~MainWindow();
  void setVisibleAndActivate();

protected:
  void closeEvent(QCloseEvent *event);

private:
  void createActions();
  void toggleVisibility();

private slots:
  void iconActivated(QSystemTrayIcon::ActivationReason reason);

private:
  QApplication* _app;
  QSystemTrayIcon *_trayIcon;

  QMenu *_trayIconMenu;
  QAction *_minimizeAction;
  QAction *_maximizeAction;
  QAction *_restoreAction;
  QAction *_quitAction;
  
  QByteArray _geometry;
};

#endif	/* MAINWINDOW_H */

