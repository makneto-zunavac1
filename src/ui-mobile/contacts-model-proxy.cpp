/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

// backend
#include "../backend/telepathy-client.h"
#include "../backend/session.h"
#include "../backend/voip/element-factory/defaultelementfactory.h"
#include "../backend/voip/element-factory/deviceelementfactory.h"
#include "../backend/accounts-model-item.h"
#include "../backend/accounts-model.h"

#include "contacts-model-proxy.h"

ContactsModelProxy::ContactsModelProxy(
  QObject *parent,
  MaknetoBackend::AccountsModel* accountsModel)
: QAbstractItemModel(parent) {

  _inserting = false;
  _removing = false;


  _accountsModel = accountsModel;
  connect(_accountsModel,
    SIGNAL(accountCountChanged()),
    SLOT(onAccountModelChanged()));

  connect(_accountsModel,
    SIGNAL(rowsAboutToBeInserted(QModelIndex, int, int)),
    SLOT(onRowsAboutToBeInserted(QModelIndex, int, int)));

  connect(_accountsModel,
    SIGNAL(rowsInserted(QModelIndex, int, int)),
    SLOT(onRowsInserted(QModelIndex, int, int)));

  connect(_accountsModel,
    SIGNAL(rowsAboutToBeRemoved(QModelIndex, int, int)),
    SLOT(onRowsAboutToBeRemoved(QModelIndex, int, int)));

  connect(_accountsModel,
    SIGNAL(rowsRemoved(QModelIndex, int, int)),
    SLOT(onRowsRemoved(QModelIndex, int, int)));

  connect(_accountsModel,
    SIGNAL(dataChanged(QModelIndex, QModelIndex)),
    SLOT(onDataChanged(QModelIndex, QModelIndex)));

  setRoleNames(_accountsModel->roleNames());
  onAccountModelChanged();
}

int ContactsModelProxy::contactCount() const {
  return _indexesMapping.size();
}

QModelIndex ContactsModelProxy::index(int row, int column, const QModelIndex &parent) const {
  return createIndex(row, column, 0);
}

QModelIndex ContactsModelProxy::parent(const QModelIndex &index) const {
  // only one depth, return root node
  return QModelIndex();
}

int ContactsModelProxy::rowCount(const QModelIndex &parent) const {
  return _indexesMapping.size();
}

int ContactsModelProxy::columnCount(const QModelIndex &parent) const {
  return 1;
}

QVariant ContactsModelProxy::data(const QModelIndex &index, int role) const {
  QModelIndex foreingIndex = _indexesMapping[ index ];
  return _accountsModel->data(foreingIndex, role);
}

QVariant ContactsModelProxy::data(int row, int role) {
  return data(index(row, 0, QModelIndex()), role);
}

void ContactsModelProxy::onAccountModelChanged() {
  _indexesMapping.clear();
  _accountOffset.clear();
  int myRow = 0;
  for (int accountRow = 0; accountRow < _accountsModel->accountCount(); accountRow++) {
    QModelIndex accountIndex = _accountsModel->index(accountRow);
    _accountOffset.insert(accountIndex, myRow);
    for (int contactRow = 0; contactRow < _accountsModel->rowCount(accountIndex); contactRow++) {
      QModelIndex contactIndex = QPersistentModelIndex(_accountsModel->index(contactRow, 0, accountIndex));
      //QVariant var = _accountModel->data(contactIndex, MaknetoBackend::AccountsModel::IdRole);
      //qDebug() << "  " << var;

      QModelIndex myIndex = index(myRow, 0);
      _indexesMapping.insert(myIndex, contactIndex);
      ++myRow;
    }
  }
  emit contactCountChanged();
}

void ContactsModelProxy::onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last) {
  if (parent == QModelIndex()) // account insert... we ignore this event
    return;

  _inserting = true;
  int offset = _accountOffset.value(parent);
  last = offset + last;
  first = offset + first;

  emit beginInsertRows(QModelIndex(), first, last);
}

void ContactsModelProxy::onRowsInserted(const QModelIndex &parent, int first, int last) {
  if (!_inserting)
    return;
  _inserting = false;

  onAccountModelChanged();

  // only for debug
  int offset = _accountOffset.value(parent);
  qDebug() << "ContactsModel: contacts rows inserted" << first << last << "(+" << offset << ")";

  emit endInsertRows();
}

void ContactsModelProxy::onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last) {
  if (parent == QModelIndex()) { // accounts remove... FIXME: what we should do? remove all related contacts?
    qWarning() << "FIXME: accounts removes from model!? what we should do? remove all related contacts?";
    return;
  }

  _removing = true;
  int offset = _accountOffset.value(parent);
  last = offset + last;
  first = offset + first;
  emit beginRemoveRows(QModelIndex(), first, last);
}

void ContactsModelProxy::onRowsRemoved(const QModelIndex &parent, int first, int last) {
  if (!_removing)
    return;
  _removing = false;

  onAccountModelChanged();

  // only for debug
  int offset = _accountOffset.value(parent);
  qDebug() << "ContactsModel: contacts rows removed " << first << last << "(+" << offset << ")";

  emit endRemoveRows();
}

void ContactsModelProxy::onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight) {
  QModelIndex myFirst = _indexesMapping.key(topLeft);
  if (!myFirst.isValid())
    return;
  QModelIndex myLast = _indexesMapping.key(bottomRight);
  if (!myLast.isValid())
    return;
  emit dataChanged(myFirst, myLast);
}

#include "contacts-model-proxy.moc"
