/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDomElement>

#include "session-model-item.h"
#include "session-model.h"

#include "../backend/session.h"

struct SessionModelItem::QueueItem {

  QueueItem(const QString &text, const QString & contact) :
  text(text), contact(contact) {
  }

  QString text;
  QString contact;
};

SessionModelItem::SessionModelItem(MaknetoBackend::Session *session, QObject *parent) :
QObject(parent),
_session(session),
_visible(false) {

  connect(_session, SIGNAL(messageReceived(const QString &, const QString &)),
    SLOT(onMessageReceived(const QString &, const QString &)));
  connect(_session, SIGNAL(messageSent(const QString &)),
    SLOT(onMessageSent(const QString &)));
  connect(_session, SIGNAL(sendingError(const QString &, const QString &)),
    SLOT(onSendingError(const QString &, const QString &)));
  connect(_session, SIGNAL(sessionTypeChanged()),
    SLOT(onSessionTypeChanged()));
  connect(_session, SIGNAL(chatStateChanged(MaknetoBackend::Session::ChatState, const QString &)),
    SLOT(onChatStateChanged(MaknetoBackend::Session::ChatState, const QString &)));

  connect(_session, SIGNAL(incomingCall()),
    SLOT(onIncomingCall()));
  connect(_session, SIGNAL(callReady()),
    SLOT(onCallReady()));
  connect(_session, SIGNAL(callEnded(const QString &)),
    SLOT(onCallEnded(const QString &)));
  connect(_session, SIGNAL(callError(const QString &)),
    SLOT(onCallError(const QString &)));
  connect(_session, SIGNAL(videoPreviewAvailable(QGst::ElementPtr)),
    SLOT(onVideoPreviewAvailable(QGst::ElementPtr)));
  connect(_session, SIGNAL(videoAvailable(QGst::ElementPtr)),
    SLOT(onVideoAvailable(QGst::ElementPtr)));
  connect(_session, SIGNAL(incomingCall()),
    SLOT(onIncomingCall()));
  connect(_session, SIGNAL(videoPreviewRemoved(QGst::ElementPtr)),
    SLOT(onVideoPreviewRemoved(QGst::ElementPtr)));
  connect(_session, SIGNAL(videoRemoved(QGst::ElementPtr)),
    SLOT(onVideoRemoved(QGst::ElementPtr)));
  connect(_session, SIGNAL(remoteAudioStreamAdded()),
    SLOT(onRemoteAudioStreamAdded()));
}

SessionModelItem::~SessionModelItem() {
}

MaknetoBackend::Session *SessionModelItem::getSession() {
  return _session;
}

bool SessionModelItem::isVisible() {
  return _visible;
}

void SessionModelItem::setVisible(bool b) {
  _visible = b;
  if (_visible) { // flush queue
    while (!_queue.isEmpty()) {
      QueueItem *item = _queue.front();
      _queue.pop_front();
      // FIXME: add timestamp to queue item
      onMessageReceived(item->text, item->contact);
      delete item;
    }
  }
}

QString SessionModelItem::getId() {
  return _session->getUniqueName();
}

QVariant SessionModelItem::data(int role) {
  if (!_session)
    return QVariant();

  switch (role) {
    case SessionModel::ItemRole:
      //return QVariant(_session->getUniqueName());
      return QVariant::fromValue(this);
    case SessionModel::IdRole:
      return QVariant(_session->getUniqueName());
    case SessionModel::IconRole:
      return _session->getIcon();
    case SessionModel::TypeRole:
      return (int)_session->getSessionType();
    case SessionModel::LastMessageRole:
      return lastMessage;
    case Qt::DisplayRole:
    case SessionModel::NameRole:
      return QVariant(_session->getName());
  }
  return QVariant();
}

void SessionModelItem::onIncomingCall() {
  emit incomingCall(getId());
}

void SessionModelItem::onCallReady() {
  qDebug() << "SessionModelItem: Call Ready, accepting outgoing call...";
  _session->acceptCall();
  emit callReady(getId());
}

void SessionModelItem::onCallEnded(const QString &errorMsg) {
  qDebug() << "SessionModelItem: Call Ended";
  emit callEnded(getId());
}

void SessionModelItem::onCallError(const QString &errorMsg) {
  qDebug() << "SessionModelItem: Call Error" << errorMsg;
  emit callError(getId(), errorMsg);
}

void SessionModelItem::onVideoPreviewAvailable(QGst::ElementPtr videoOutElemPtr) {
  qDebug() << "SessionModelItem: videoPreviewAvailable";
  emit videoPreviewAvailable(getId(), videoOutElemPtr);
}

void SessionModelItem::onVideoAvailable(QGst::ElementPtr videoOutElemPtr) {
  qDebug() << "SessionModelItem: videoAvailable";
  emit videoAvailable(getId(), videoOutElemPtr);
}

void SessionModelItem::onVideoPreviewRemoved(QGst::ElementPtr videoOutElemPtr) {
  qDebug() << "SessionModelItem: videoPreviewRemoved";
  emit videoPreviewRemoved(getId(), videoOutElemPtr);
}

void SessionModelItem::onVideoRemoved(QGst::ElementPtr videoOutElemPtr) {
  qDebug() << "SessionModelItem: videoRemoved";
  emit videoRemoved(getId(), videoOutElemPtr);
}

void SessionModelItem::onRemoteAudioStreamAdded() {
  qDebug() << "SessionModelItem: audio stream added";
  emit remoteAudioStreamAdded(getId());
}

void SessionModelItem::onMessageReceived(const QString &text, const QString &contact) {
  if (!_visible) { // enqueue
    QueueItem *item = new QueueItem(text, contact);
    _queue.push_back(item);
    emit messageEnqueued(getId());
    return;
  }

  QDomDocument doc;
  doc.setContent(text);
  //QDomElement e = doc.elementsByTagNameNS("http://jabber.org/protocol/svgwb", "wb").item(0).toElement();
  QDomElement e = doc.elementsByTagName("wb").item(0).toElement();
  if (!e.isNull())
    emit whiteboardMessageReceived(getId(), text, contact);
  else {
    //TODO figure out what to do in case of MUC
    lastMessage = text;
    emit textMessageReceived(getId(), text, contact);
  }
}

void SessionModelItem::sendTextMessage(const QString &text) {
  _session->sendMessage(text);
}

void SessionModelItem::sendWhiteboardMessage(const QDomElement &wb) {
  QString message;

  QTextStream ts(&message);
  wb.save(ts, 0);
  //qDebug() << "wb:" << endl << message << endl << endl;
  //TODO check the resource
  _session->sendMessage(ts.readAll());
}

void SessionModelItem::onSessionTypeChanged() {
  MaknetoBackend::Session::SesstionTypes type = _session->getSessionType();

  // just for information...
  QString typeStr = "[";
  if (type.testFlag(MaknetoBackend::Session::SessionTypeText))
    typeStr += "Text ";
  if (type.testFlag(MaknetoBackend::Session::SessionTypeMuc))
    typeStr += "MUC ";
  if (type.testFlag(MaknetoBackend::Session::SessionTypeAudio))
    typeStr += "Audio ";
  if (type.testFlag(MaknetoBackend::Session::SessionTypeVideo))
    typeStr += "Video ";
  if (type.testFlag(MaknetoBackend::Session::SessionTypeCallPending))
    typeStr += "CallPending ";
  typeStr += "]";
  qDebug() << "SessionModelItem: session type changed " << typeStr;

  if (_visible)
    sessionTypeChanged(getId(), ((int) type));
}

void SessionModelItem::onMessageSent(const QString &message) {
  // TODO: split this signal for whiteboard and text message...
  messageSent(getId(), message);
}

void SessionModelItem::onSendingError(const QString &message, const QString &errorMessage) {
  emit sendingError(getId(), message, errorMessage);
}

void SessionModelItem::onChatStateChanged(MaknetoBackend::Session::ChatState state, const QString &contact) {
  if (_visible)
    emit chatStateChanged(getId(), state, contact);
}

void SessionModelItem::changeChatState(MaknetoBackend::Session::ChatState state) {
  _session->changeChatState(state);
}

#include "session-model-item.moc"

