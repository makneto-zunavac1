/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDeclarativeItem>
#include <QWebView>

#ifndef MYVIEW_H
#define	MYVIEW_H

class ViewGraber : public QDeclarativeItem {
  Q_OBJECT
  Q_DISABLE_COPY(ViewGraber)

  Q_PROPERTY(QDeclarativeItem *delegate READ delegate WRITE setDelegate)
public:

  ViewGraber(QDeclarativeItem *parent = 0);
  virtual ~ViewGraber();
  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

  inline QDeclarativeItem *delegate() {
    return _delegate;
  };

  inline void setDelegate(QDeclarativeItem *d) {
    _delegate = d;
  };

Q_SIGNALS:
  void snapshotTaken();

  public
Q_SLOTS:
  void takeSnapshot();
  void releaseSnapshot();


private:
  bool scheduledSnapshot;
  bool paintSnapshot;
  QDeclarativeItem *_delegate;
  QImage snapshot;
};

#endif	/* VIEWGRABER_H */

