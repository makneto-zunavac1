/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtDeclarative> 
#include <QDomElement>

#include "../backend/session.h"

#ifndef SESSIONMODELITEM_H
#define	SESSIONMODELITEM_H

class SessionModelItem : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(SessionModelItem)
  Q_PROPERTY(bool visible READ isVisible WRITE setVisible);

public:

  SessionModelItem(MaknetoBackend::Session *session, QObject *parent = 0);
  virtual ~SessionModelItem();
  MaknetoBackend::Session *getSession();
  bool isVisible();
  void setVisible(bool);

  Q_INVOKABLE QVariant data(int role);

  QString getId();

Q_SIGNALS:
  void textMessageReceived(const QString &sessionId, const QString &text, const QString &contact);
  void whiteboardMessageReceived(const QString &sessionId, const QString &text, const QString &contact);
  void messageEnqueued(const QString &sessionId);
  // VoIP
  void incomingCall(const QString &sessionId);
  void callReady(const QString &sessionId);
  void callEnded(const QString &sessionId);
  void callError(const QString &sessionId, const QString &);
  void videoPreviewAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoPreviewRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void remoteAudioStreamAdded(const QString &sessionId);

  void sessionTypeChanged(const QString &sessionId, int types);
  void messageSent(const QString &sessionId, const QString &message); 
  void sendingError(const QString &sessionId, const QString &message, const QString &errorMessage); 
  void chatStateChanged(const QString &sessionId, int chatState, const QString &contact);

  public
Q_SLOTS:
  void onMessageReceived(const QString &text, const QString &contact);
  void sendTextMessage(const QString &text);
  void sendWhiteboardMessage(const QDomElement &wb);
  void onSessionTypeChanged();
  void onMessageSent(const QString &message); // outgoing message was successfully send
  void onSendingError(const QString &message, const QString &errorMessage); // outgoing message sending failed
  void onChatStateChanged(MaknetoBackend::Session::ChatState state, const QString &contact);
  void changeChatState(MaknetoBackend::Session::ChatState state);
  
  // VoIP
  void onIncomingCall();
  void onCallReady();
  void onCallEnded(const QString &);
  void onCallError(const QString &);
  void onVideoPreviewAvailable(QGst::ElementPtr videoOutElemPtr);
  void onVideoAvailable(QGst::ElementPtr videoOutElemPtr);
  void onVideoPreviewRemoved(QGst::ElementPtr videoOutElemPtr);
  void onVideoRemoved(QGst::ElementPtr videoOutElemPtr);
  void onRemoteAudioStreamAdded();

private:
  struct QueueItem;
  friend struct QueueItem;
  QString lastMessage;

  MaknetoBackend::Session *_session;
  QList<QueueItem *> _queue;
  bool _visible;
};

Q_DECLARE_METATYPE(SessionModelItem*);

#endif	/* SESSIONMODELITEM_H */

