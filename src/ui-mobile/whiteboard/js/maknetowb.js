/***************************************************************************
 *   Copyright (C) 2011 by Lukáš Karas <lukas.karas@centrum.cz>            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

function MaknetoWhiteboard(workArea){
  this.workArea = workArea;
  this.mouseIsDown = false;
  this.connector = connector;
}

MaknetoWhiteboard.prototype.init = function(weigth, height){

  this.zoom = 1; 
  this.maxZoom = 5;
  this.minZoom = 0.5; // FIXME: compute this
  if (this.thumb){
    this.thumb.destroy();
  }
  if (this.whiteboard){
    this.whiteboard.destroy();
  }
    
  //this.thumb      = new Thumbnail( $(this.thumbArea), weigth, height, false, this);
  try{
    this.whiteboard = new Whiteboard($(this.workArea) , weigth, height, true, this, this.connector.getUID());
  }catch(e){
    console.error("error while initializing svg whiteboard: "+JSON.stringify(e));
  }

/*
  this.thumb.registerViewportCallback(this);
    
  var inst = this;
  this.workArea.scroll(function(){
    inst.viewportChanged();
  });
    
  // initial update of thumbnail viewport
  this.viewportChanged();
  */
}

MaknetoWhiteboard.prototype.zoomIn = function(){
  var z = this.zoom + 0.1;
  if (z > this.maxZoom)
    return;
  this.setZoom(z)
}

MaknetoWhiteboard.prototype.zoomOut = function(){
  var z = this.zoom - 0.1;
  if (z < this.minZoom)
    return;
  this.setZoom(z)
}

MaknetoWhiteboard.prototype.setZoom = function(z){
  //$('#zoom').html( Math.round(z*10) / 10);

  /*
  var x = (this.workArea.scrollLeft() + ((this.workArea.width() ) / 2) )/ this.zoom;
  var y = (this.workArea.scrollTop() + ((this.workArea.height() ) / 2) )/ this.zoom;
  console.log("zoom center "+x+"x"+y+" ("+this.zoom+")");
  */
  
  
  var inst = this;
  /*
  try{
  }catch(e){
    console.error("error "+JSON.stringify(e));
  }

  inst.whiteboard.svgroot.suspendRedraw(5000); 
  $(inst.whiteboard.svgroot).children("svg").each(function(e){
    this.suspendRedraw(5000);
  });
  */

  this.zoom = z; 
  this.whiteboard.setZoom(this.zoom);

  //this.whiteboard.showInCenter(x*this.zoom, y*this.zoom);
  
  var before  = new Date();
  //var callbackCount = 0;
  var callback = function(){
    console.log("zooming took "+((new Date()).getTime() - before.getTime())+"ms " );
    /*
    inst.whiteboard.svgroot.unsuspendRedraw(); 
    $(inst.whiteboard.svgroot).children("svg").each(function(e){
      this.unsuspendRedraw();
    });
    */
    inst.connector.canvasResized();
  };
  window.setTimeout(callback, 0);
  
}

/**
 * calback function from thumbnail... mouse action on thumnail can change viewport
 */
MaknetoWhiteboard.prototype.showInCenter = function(x,y){
  this.whiteboard.showInCenter(x, y);
}

/**
 * function registered on canvas scrool events. it inform thumnail that viewport was changed
 */
MaknetoWhiteboard.prototype.viewportChanged = function(){
  /*
  var x = (this.workArea.scrollLeft() / this.zoom) - this.whiteboard.canvasWidth;
  var y = (this.workArea.scrollTop()  / this.zoom) - this.whiteboard.canvasHeight;
  var w = this.workArea.width()  / this.zoom;
  var h = this.workArea.height() / this.zoom;
  
  this.thumb.viewportChanged(x,y,w,h);
  */
}
  

MaknetoWhiteboard.prototype.buildNewXmlTag = function(wb){
  var str = "<"+wb.element+" ";
  for (var attr in wb.attr){
    if (typeof wb.attr[attr] == 'function')
      continue; // workaround for Object.clone method
    
    str += attr+"='"+wb.attr[attr]+"' ";
  }
  str +=" />";
  return str;
}
MaknetoWhiteboard.prototype.buildConfigureXmlTag = function(wb){
  var str = "";
  for (var i in wb.attr){
    var attr = wb.attr[i];
    if (attr == undefined || attr.value === undefined)
      continue;
    str += "<attribute name='"+attr.name+"'>"+attr.value+"</attribute>\n";
  }
  return str;
}
/**
  * convert json data from own html whiteboard to SVG Whiteboardind XMPP extension XML
  */
MaknetoWhiteboard.prototype.buildXmlMessage = function(msg){
  var xmlMessage = "<wb xmlns='http://jabber.org/protocol/svgwb'>\n";
  if (msg.wb.type == "new"){
    xmlMessage += "<new id='"+msg.wb.id+"' version='"+msg.wb.version+"' index='"+msg.wb.index+"'>\n";
    xmlMessage += this.buildNewXmlTag(msg.wb);
    xmlMessage += "\n</new>";
  }
  if (msg.wb.type == "configure"){
    xmlMessage += "<configure target='"+msg.wb.target+"' version='"+msg.wb.version+"'>\n";
    xmlMessage += this.buildConfigureXmlTag(msg.wb);
    xmlMessage += "\n</configure>";
  }
  xmlMessage += "\n</wb>";
  return xmlMessage;
}

MaknetoWhiteboard.prototype.processLocalCommand = function(drawCommand){
  var msg = {
    type : "groupchat", 
    from : this.connector.getUID(),
    to : this.room,
    wb: drawCommand
  };
  try{
    // TODO: colect more events to one big message
    if (this.connector.sendMessage( this.buildXmlMessage(msg))){
      this.processCommand(drawCommand);
    }
  }catch(e){
    console.error("error while building, sending or processing command "+JSON.stringify(e));
  }
}

MaknetoWhiteboard.prototype.processMessage = function(message){
  if (message.type != "groupchat" || message.to != this.room || (!message.wb))
    return;
  this.processCommand(message.wb);
}

MaknetoWhiteboard.prototype.processCommand = function(drawCommand){  
  //console.log(" sending command..."+JSON.stringify(drawCommand));
  try{
    this.whiteboard.processCommand(drawCommand);
  }catch(e){
    console.error("error processing command "+JSON.stringify(e));
  }
//console.log("command: "+JSON.stringify(drawCommand));
//this.thumb.processCommand(drawCommand);
}

MaknetoWhiteboard.prototype.handleNewCommands = function(newElems){

  for (var i =0; i < newElems.length; i++){
        
    var elem = newElems[i];
      
    if (!elem.hasChildNodes())
      continue;
    var child = elem.childNodes[0];
    for (var chi = 0; chi < elem.childNodes.length; chi++){
      if (child.nodeName !== "#text")
        break;
      child = elem.childNodes[chi];
    }
    if (child.nodeName === "#text")
      continue;
    //console.log("child name "+child.nodeName+" ");
    //elem.chi
      
    var attributes = {};
    for (var ati = 0; ati< child.attributes.length; ati++){
      if (child.attributes[ati].nodeName !== undefined){
        //console.log("   "+child.nodeName+" "+ child.attributes[ati].nodeName+" = "+child.attributes[ati].nodeValue);
        attributes[ child.attributes[ati].nodeName ] = child.attributes[ati].nodeValue;
      }
    }

    this.processCommand({
      type: "new",
      id: elem.getAttribute ("id"),
      index: elem.getAttribute ("index"), 
      element: child.nodeName, // FIXME ?
      version: elem.getAttribute ("version"),
      attr: attributes
    });
  }
 
}

MaknetoWhiteboard.prototype.handleConfigureCommands = function(configureElems){
  for (var i=0; i< configureElems.length; i++){
    //console.log("iterate elems...");      
    var elem = configureElems[i];
    var target = elem.getAttribute("target");
    var version = elem.getAttribute("version");
    //console.log(" configure "+target);
    
    var attributes = elem.getElementsByTagName ("attribute");
    var jsonAttrs = [];
    for (var ai = 0; ai< attributes.length; ai++){
      var attribute = attributes[ai];
      var name = attribute.getAttribute("name");
      var value = attribute.hasChildNodes()? attribute.childNodes[0].nodeValue :"";
      //console.log(" configure "+target+" "+name+"="+value);
      jsonAttrs[ai] = {
        name: name, 
        value: value
      };
    }
    var command = {
      type: "configure",
      target: target,
      version: version,
      attr: jsonAttrs
    };
    this.processCommand(command);    
  }
}

MaknetoWhiteboard.prototype.processXmlCommand = function(message){
  //console.log("xml message ");
  
  try{
    var doc = Utils.text2xml(message);   

    var newElems = doc.getElementsByTagName ("new");
    this.handleNewCommands(newElems);
    
    var configureElems = doc.getElementsByTagName("configure");
    this.handleConfigureCommands(configureElems);
  //console.log("...done");
    
  }catch(e){
    console.error("PARSE ERROR");
  }
}

/**
 * callback method. notify that outgoing message was successfuly sent
 */
MaknetoWhiteboard.prototype.messageSent= function(message){
  //console.log("sent");
}
MaknetoWhiteboard.prototype.setForegroundColor = function(color){
  this.whiteboard.foregroundColor = color;
}