/***************************************************************************
 *   Copyright (C) 2011 by Lukáš Karas <lukas.karas@centrum.cz>            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

Whiteboard.prototype = SvgCanvas.prototype.clone();

Whiteboard.prototype.constructor = Whiteboard;

Whiteboard.tool = {
  PEN: "pen"
}

Whiteboard.mouseMode = Whiteboard.tool.clone(); // copy tool enum (Object) for simplify

Whiteboard.mouseMode.CANVAS_MOVE = "canvasMove";

function Whiteboard(canvasWrapper, canvasWidth, canvasHeight, showOutsideCanvas, callback, nick){

  // call super constructor
  SvgCanvas.apply(this, arguments);
  this.init();
  
  var inst = this;
  this.nick = nick;
  this.lastPathId = 0;
  this.tool = Whiteboard.tool.PEN;
  this.foregroundColor = "red";

  this.mouseIsDown = false;
  $( this.svgroot ).mousedown(function(e){inst.mouseDown(e)});
//    $( this.svgroot ).mouseclick(function(e){e.preventDefault();})
  $( window ).mouseup(function(e){inst.mouseUp(e)})
              .mousemove(function(e){inst.mouseMove(e)}); // .mouseout(function(e){inst.mouseUp(e)});


  this.callback = callback;  
}

Whiteboard.prototype.mouseDown2 = function(clientX,clientY,button){
  var xc = ((this.canvasWrapper.scrollLeft() / this.zoom) - this.canvasWidth) + ((clientX - this.canvasWrapper.offset().left ) / this.zoom);
  var yc = ((this.canvasWrapper.scrollTop() / this.zoom) - this.canvasHeight) + ((clientY - this.canvasWrapper.offset().top ) / this.zoom);    
    //console.debug("mousedown");
  this.mouseIsDown = true;
  
  this.mouseStart = {
    clientX: clientX,
    clientY: clientY,
    scrollLeft : this.canvasWrapper.scrollLeft(),
    scrollTop : this.canvasWrapper.scrollTop()
  };
  
  if (button == 1){ // middle button
    this.mouseMode = Whiteboard.mouseMode.CANVAS_MOVE;
    return;
  }

  this.mouseMode = this.tool;    
  
    switch (this.mouseMode) {
      case Whiteboard.mouseMode.PEN :
        this.currentElementId = ("path"+(++this.lastPathId)+"/"+this.nick);
        console.log(this.currentElementId);
        var children = $( this.svgcontent ).children();
        if (children.length==0){
          var index =  1 + Math.random();
        }else{
          var data = $( children[ children.length -1 ] ).data("index");
          var index = (data ? data/1 + 1 : 1) + Math.random();
        }
        this.lastLinePoints = "M"+xc+" "+yc+" ";
        this.callback.processLocalCommand({
            type: "new",
            id: this.currentElementId,
            index: index, 
            element: "path",
            version: 0,
            attr:{
              d: this.lastLinePoints,
              style:"fill:none;stroke:"+this.foregroundColor+";stroke-width:2;" 
            }});
        this.currentElement = $( this.getElem( this.translateId( this.currentElementId ) ) );
      default:
        break;
      
    }  
  this.lastXc = xc;this.lastYc = yc;  
}

Whiteboard.prototype.mouseDown = function(e){
  e.preventDefault();
  this.mouseDown2(e.clientX, e.clientY, e.button);
}

Whiteboard.prototype.mouseUp2 = function(){
  // this.mouseMove(e);  // invoke move function for final point 
    //console.debug("mouseup");
  this.mouseIsDown = false;
  this.currentElementId = null;  
}
Whiteboard.prototype.mouseUp = function(e){
  e.preventDefault();
  this.mouseUp2();
}

Whiteboard.prototype.mouseMove2 = function(clientX, clientY){
  var xc = ((this.canvasWrapper.scrollLeft() / this.zoom) - this.canvasWidth) + ((clientX - this.canvasWrapper.offset().left ) / this.zoom);
  var yc = ((this.canvasWrapper.scrollTop() / this.zoom) - this.canvasHeight) + ((clientY - this.canvasWrapper.offset().top ) / this.zoom);    

        //console.debug("mousemove "+this.mouseIsDown+" ("+Math.round(xc)+"x"+Math.round(yc)+")");
  // $('#viewport').html("("+Math.round(xc)+"x"+Math.round(yc)+")");
  
  if (this.mouseIsDown){
    //console.log("we are moving... "+this.mouseMode+" "+e.clientX+"x"+e.clientY+" "+this.canvasWrapper.scrollLeft()+"x"+this.canvasWrapper.scrollTop());

    var x = this.mouseStart.clientX - clientX;
    var y = this.mouseStart.clientY - clientY;
 
    switch (this.mouseMode) {
      case Whiteboard.mouseMode.CANVAS_MOVE :
        this.canvasWrapper.scrollLeft( this.mouseStart.scrollLeft + x );
        this.canvasWrapper.scrollTop( this.mouseStart.scrollTop + y );
        break;
      case Whiteboard.mouseMode.PEN :
        if ( Math.abs(this.lastXc - xc) + Math.abs(this.lastYc - yc) > 60){
          //this.lastLinePoints += "T"+xc+" "+yc+" ";
          this.lastLinePoints += "L"+xc+" "+yc+" ";
          //this.lastLinePoints += "Q"+this.lastXc+" "+this.lastYc+" "+xc+" "+yc+" ";
        }else{
          this.lastLinePoints += "L"+xc+" "+yc+" ";
        }
        this.callback.processLocalCommand({
            type: "configure",
            target: this.currentElementId,
            version: this.currentElement.data("version") +1,
            attr:[{
                name: "d",
                value: this.lastLinePoints
            }]});
        break;
      default:
        break;
    }

    this.lastXc = xc;this.lastYc = yc;
    
  } 
}

Whiteboard.prototype.mouseMove = function(e){
  e.preventDefault();
  this.mouseMove2(e.clientX, e.clientY);
}
