/***************************************************************************
 *   Copyright (C) 2011 by Lukáš Karas <lukas.karas@centrum.cz>            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * use pieces of code from svg-edit.googlecode.com (Licensed under the Apache License, Version 2)
 *
 * Svg-Edit autors:
 *
 * Copyright(c) 2010 Alexis Deveria
 * Copyright(c) 2010 Pavol Rusnak
 * Copyright(c) 2010 Jeff Schiller
 *
 */

SvgCanvas.count = 0;

function SvgCanvas(canvasWrapper, canvasWidth, canvasHeight, showOutsideCanvas){
  // Namespace Constants
	this.svgns = "http://www.w3.org/2000/svg";
	this.xlinkns = "http://www.w3.org/1999/xlink";
	this.xmlns = "http://www.w3.org/XML/1998/namespace";
	this.xmlnsns = "http://www.w3.org/2000/xmlns/"; // see http://www.w3.org/TR/REC-xml-names/#xmlReserved
	//this.se_ns = "http://svg-edit.googlecode.com";
	this.htmlns = "http://www.w3.org/1999/xhtml";
	this.mathns = "http://www.w3.org/1998/Math/MathML";

  this.zoom = 1;
  this.showOutsideCanvas = showOutsideCanvas;

  this.canvasWrapper = canvasWrapper;
  this.canvasWidth = canvasWidth;
  this.canvasHeight = canvasHeight;
  
  SvgCanvas.count ++ ;
  this.idPrefix = "svg"+SvgCanvas.count+"x"
}

/**
 * include inline svg into wrapper element
 */
SvgCanvas.prototype.init = function(){
  
  //this.svgdoc = workAreaDom.ownerDocument;
  this.svgdoc = document;
  
  this.svgroot = this.svgdoc.importNode(Utils.text2xml('<svg id="'+this.idPrefix+'svgroot" xmlns="' + this.svgns + '" xlinkns="' + this.xlinkns + '" ' +
					'width="'+(this.canvasWidth*3)+'" height="'+(this.canvasHeight*3)+'" x="0" y="0" overflow="visible" version="1.1" >' +
					'<defs>  ' +
						'<filter id="'+this.idPrefix+'canvashadow" filterUnits="objectBoundingBox">' +
							'<feGaussianBlur in="SourceAlpha" stdDeviation="4" result="blur"/>'+
							'<feOffset in="blur" dx="5" dy="5" result="offsetBlur"/>'+
							'<feMerge>'+
								'<feMergeNode in="offsetBlur"/>'+
								'<feMergeNode in="SourceGraphic"/>'+
							'</feMerge>'+
						'</filter>'+
            '<radialGradient id="'+this.idPrefix+'workspaceBackground" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">'+
              '<stop offset="0%" style="stop-color:rgb(200,200,200); stop-opacity:0"/>'+
              '<stop offset="70%" style="stop-color:rgb(0,0,0); stop-opacity:1"/>'+
            '</radialGradient>'+
          '</defs>'+
          //'<g transform="scale(1)">'+
            '<circle cx="50%" cy="50%" r="100%" fill="white" stroke-color="gray" style="color:#000000;fill:url(#'+this.idPrefix+'workspaceBackground);fill-opacity:1;" />'+
            '<svg id="'+this.idPrefix+'canvasBackground" width="'+this.canvasWidth+'" height="'+this.canvasHeight+'" x="'+this.canvasWidth+'" y="'+this.canvasHeight+'" '+
              'overflow="visible" style="pointer-events:none" '+
              'viewBox="0 0 '+this.canvasWidth+' '+this.canvasHeight+'">'+
              '<rect width="100%" height="100%" x="0" y="0" stroke-width="1" stroke="#000" fill="#FFF" style="pointer-events:none" filter="url(#'+this.idPrefix+'canvashadow)"></rect>'+
            '</svg>'+
          //'</g>'+
				'</svg>').documentElement, true);

  this.canvasWrapper.append(this.svgroot);

  // The actual element that represents the final output SVG element
  this.svgcontent = this.svgdoc.createElementNS(this.svgns, "svg");
  $(this.svgcontent).attr({
    id: this.idPrefix+'svgcontent',
    width: this.canvasWidth,
    height: this.canvasHeight,
    x: this.canvasWidth,
    y: this.canvasHeight,
    overflow: this.showOutsideCanvas?'visible':'hidden',
    xmlns: this.svgns,
    'viewBox': '0 0 '+this.canvasWidth+' '+this.canvasHeight+'',
    //"xmlns:se": this.se_ns,
    "xlinkns": this.xlinkns
  }).appendTo(this.svgroot);

  //this.svgcontent.setAttribute("viewBox", "0 0 320 240");
  //this.svgcontent.setAttribute("zoom", "2");

  // Animation element to change the opacity of any newly created element
  this.opac_ani = this.svgdoc.createElementNS(this.svgns, 'animate');
  $(this.opac_ani).attr({
    attributeName: 'opacity',
    begin: 'indefinite',
    dur: 1,
    fill: 'freeze'
  }).appendTo(this.svgroot);

  this.showInCenter( (this.canvasWidth) /2, (this.canvasHeight)/2 );
}

SvgCanvas.prototype.destroy = function(){
  $( this.svgroot ).remove();
} 

SvgCanvas.prototype.showInCenter = function(x,y){
  x = (x+this.canvasWidth) * this.zoom;
  y = (y+this.canvasHeight) * this.zoom;
  this.canvasWrapper.scrollLeft( x - (this.canvasWrapper.width() ) / 2 );
  this.canvasWrapper.scrollTop( y - (this.canvasWrapper.height()) / 2 );
}

SvgCanvas.prototype.setZoom = function(zoom){
  var x = (this.canvasWrapper.scrollLeft() + ((this.canvasWrapper.width() ) / 2) )/ this.zoom;
  var y = (this.canvasWrapper.scrollTop() + ((this.canvasWrapper.height() ) / 2) )/ this.zoom;
  x = x - this.canvasWidth;
  y = y - this.canvasHeight;
  console.log("zoom center "+x+"x"+y+" ("+this.zoom+")");
  
  this.zoom = zoom; 
  //this.whiteboard.setZoom(this.zoom);  
  
  var inst = this;
  $(this.svgroot).children("svg").each(function(e){
            $(this).attr({
              width: inst.canvasWidth * zoom,
              height: inst.canvasHeight * zoom,
              x: inst.canvasWidth * zoom,
              y: inst.canvasHeight * zoom
            });
          });          
  $(this.svgroot).attr({
    width: 3*this.canvasWidth * zoom,
    height: 3*this.canvasHeight * zoom
  });  
  
  this.showInCenter(x, y);
}

SvgCanvas.prototype.translateId = function(id){
  return this.idPrefix+ id.replace(/[\@\/.#]/gi, "_");
}

SvgCanvas.prototype.processCommand = function(drawCommand){
  //console.debug("processing command "+drawCommand.type);
  switch(drawCommand.type){
    case "configure":
      var id = this.translateId( drawCommand.target );
      var version = drawCommand.version ? drawCommand.version : 0;
      version = version /1;
      var target = this.getElem(id);
      if (!target)
        return;
      target = $( target );
      if (target.data("version")/1 > version)
        this.revertEdits(target, version);
      var history = target.data("history")? target.data("history"): [];
      target.data("version", version);
      for (var i=0; i < drawCommand.attr.length; i++){
        var item = drawCommand.attr[ i ];
        history[ history.length ] = {version: version, attr: item.name, oldValue: target.attr(item.name), newValue: item.value};
        target.attr(item.name, item.value);
      }
      target.data("history", history);
      break;
    case "new":
      var id = this.translateId( drawCommand.id );
      var version = drawCommand.version ? drawCommand.version : 0;
      var index = drawCommand.index;
      var parentElem = $( drawCommand.parent === undefined ? this.svgcontent :getElem( this.translateId( drawCommand.parent )) );
      drawCommand.attr.id = id
      var newElem = $( this.addSvgElementFromJson(drawCommand, null, false) );    
      newElem.data("index", index);
      newElem.data("version", version);
      newElem.data("id", drawCommand.id);

      var children = parentElem.children();
      var lastLess = null;
      // browse childs from last... we add new elements to end almost always
      for (var i=children.length -1; i>= 0 ; i--){
        lastLess = $( children[i] );
        if (lastLess.data("index") < index || (lastLess.data("index") == index && lastLess.attr("id") < id))
          break;
      }
      if (lastLess == null){
        parentElem.prepend( newElem );
      }else{
        lastLess.after(newElem);
      }
      break;
    default:
      break;
  }
}

SvgCanvas.prototype.revertEdits = function(target, version){
  console.warn("revert version of "+target.data("id")+" "+target.data("version")+" to "+version+"");
  // todo: write it
  var history = target.data("history");
  if (!history)
    return;
  for(var i=history.length -1; i>=0; i--){
    var item = history[i];
    if (item.version < version)
      return;
    target.attr(item.attr, item.oldValue);
  }
}

// Function: getElem
// Get a DOM element by ID within the SVG root element.
//
// Parameters:
// id - String with the element's new ID
SvgCanvas.prototype.getElem = function(id) {
// 	if(svgroot.getElementById) {
// 		// getElementById lookup
// 		return svgroot.getElementById(id);
// 	} else 

  if(this.svgroot.querySelector) {
    // querySelector lookup
    return this.svgroot.querySelector('#'+id);
  } else if(this.svgdoc.evaluate) {
    // xpath lookup
    return this.svgdoc.evaluate('svg:svg[@id="svgroot"]//svg:*[@id="'+id+'"]', this.canvasWrapper, function() {return "http://www.w3.org/2000/svg";}, 9, null).singleNodeValue;
  } else {
    // jQuery lookup: twice as slow as xpath in FF
    return $(this.svgroot).find('[id=' + id + ']')[0];
  }

  // getElementById lookup: includes icons, not good
  // return svgdoc.getElementById(id);
}

// Function: addSvgElementFromJson
// Create a new SVG element based on the given object keys/values and add it to the current layer
// The element will be ran through cleanupElement before being returned 
//
// Parameters:
// data - Object with the following keys/values:
// * element - DOM element to create
// * attr - Object with attributes/values to assign to the new element
// * curStyles - Boolean indicating that current style attributes should be applied first
//
// Returns: The new element
SvgCanvas.prototype.addSvgElementFromJson = function(data, parent, append) {
  if (!parent)
    parent = this.svgcontent;
  var shape = this.getElem(data.attr.id);
  // if shape is a path but we need to create a rect/ellipse, then remove the path
  if (shape && data.element != shape.tagName) {
    //current_layer.removeChild(shape);
    shape = null;
  }
  if (!shape) {
    shape = this.svgdoc.createElementNS(this.svgns, data.element);
    if (append != false)
      parent.appendChild(shape);
  }
  if(data.curStyles) {
    // fixme set styles from control pannel
    /*
    this.assignAttributes(shape, {
      "fill": cur_shape.fill,
      "stroke": cur_shape.stroke,
      "stroke-width": cur_shape.stroke_width,
      "stroke-dasharray": cur_shape.stroke_dasharray,
      "stroke-linejoin": cur_shape.stroke_linejoin,
      "stroke-linecap": cur_shape.stroke_linecap,
      "stroke-opacity": cur_shape.stroke_opacity,
      "fill-opacity": cur_shape.fill_opacity,
      "opacity": cur_shape.opacity / 2,
      "style": "pointer-events:inherit"
    }, 100);
    */
  }
  this.assignAttributes(shape, data.attr, 100);
  this.cleanupElement(shape);
  return shape;
}

// Function: assignAttributes
// Assigns multiple attributes to an element.
//
// Parameters: 
// node - DOM element to apply new attribute values to
// attrs - Object with attribute keys/values
// suspendLength - Optional integer of milliseconds to suspend redraw
// unitCheck - Boolean to indicate the need to use setUnitAttr
SvgCanvas.prototype.assignAttributes = function(node, attrs, suspendLength, unitCheck) {
  if(!this.suspendLength) this.suspendLength = 0;
  // Opera has a problem with suspendRedraw() apparently
  var handle = null;
  if (!window.opera) 
    this.svgroot.suspendRedraw(this.suspendLength);

  for (var i in attrs) {
    var ns = (i.substr(0,4) == "xml:" ? this.xmlns : 
      i.substr(0,6) == "xlink:" ? this.xlinkns : null);

    if(ns || !unitCheck) {
      node.setAttributeNS(ns, i, attrs[i]);
    } else {
      Utils.setUnitAttr(node, i, attrs[i]);
    }

  }

  if (!window.opera) 
    this.svgroot.unsuspendRedraw(handle);
}

// Function: cleanupElement
// Remove unneeded (default) attributes, makes resulting SVG smaller
//
// Parameters:
// element - DOM element to clean up
SvgCanvas.prototype.cleanupElement = function(element) {
  if (!window.opera) 
    var handle = this.svgroot.suspendRedraw(60);
  var defaults = {
    'fill-opacity':1,
    'stop-opacity':1,
    'opacity':1,
    'stroke':'none',
    'stroke-dasharray':'none',
    'stroke-linejoin':'miter',
    'stroke-linecap':'butt',
    'stroke-opacity':1,
    'stroke-width':1,
    'rx':0,
    'ry':0
  }

  for(var attr in defaults) {
    var val = defaults[attr];
    if(element.getAttribute(attr) == val) {
      element.removeAttribute(attr);
    }
  }

  if (!window.opera) 
    this.svgroot.unsuspendRedraw(handle);
}
