/***************************************************************************
 *   Copyright (C) 2011 by Lukáš Karas <lukas.karas@centrum.cz>            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

Thumbnail.prototype = SvgCanvas.prototype.clone();

Thumbnail.prototype.constructor = Thumbnail;

function Thumbnail(canvasWrapper, canvasWidth, canvasHeight, showOutsideCanvas, callback){
  // call super constructor
  SvgCanvas.apply(this, arguments);
  this.init();

  this.viewportCenter = {
    x: (this.canvasWidth / 2), 
    y: (this.canvasHeight / 2)
  }

  // add viewport svg elem
  this.viewport = this.svgdoc.createElementNS(this.svgns, "svg");
  $(this.viewport).attr({
    id: this.idPrefix+'viewport',
    width: this.canvasWidth,
    height: this.canvasHeight,
    x: this.canvasWidth,
    y: this.canvasHeight,
    overflow: 'visible',
    xmlns: this.svgns,
    'viewBox': '0 0 '+this.canvasWidth+' '+this.canvasHeight+'',
    //"xmlns:se": this.se_ns,
    "xlinkns": this.xlinkns
  }).appendTo(this.svgroot);  
  
  this.viewportRect = this.addSvgElementFromJson({
            "element": "rect",
            "attr": {
              "id": (this.idPrefix+"viewportRect" ),
              "fill": "rgba(255, 255, 255, 0)",
              "width": this.canvasWidth,
              "height": this.canvasHeight,
              "x": 0,
              "y": 0,
              "stroke": "#22C",
              "stroke-width": 4,
              "style": "cursor: hand;"
            }
          });
  $( this.viewport ).append( this.viewportRect );
          
  // fit to wrapper (only width)
  var z = this.canvasWrapper.width() / (this.canvasWidth *1.5); 
  this.setZoom(z);
  this.registerViewportCallback(callback);
}

/**
 * ...for register callback interface for listen of viewport changes from user
 * it can be registered only once
 */
Thumbnail.prototype.registerViewportCallback = function(callback){
  if (this.viewportCallback)
    return;
  this.viewportCallback = callback;
  
  var inst = this;
  // $( this.canvasWrapper ).mousedown(function(e){inst.wrapperMouseDown(e)});
  $( this.canvasWrapper ).mousedown(function(e){inst.mouseDown(e)});
  $( window ).mouseup(function(e){inst.mouseUp(e)})
              .mousemove(function(e){inst.mouseMove(e)});  
}

Thumbnail.prototype.mouseDown = function(e){
  this.mouseIsDown = true;
  
  if (e.target.id != this.viewportRect.id ){
    console.log("thumbnail click out of viewport...");
    var xc = ((this.canvasWrapper.scrollLeft() / this.zoom) - this.canvasWidth) + ((e.clientX - this.canvasWrapper.offset().left ) / this.zoom);
    var yc = ((this.canvasWrapper.scrollTop() / this.zoom) - this.canvasHeight) + ((e.clientY - this.canvasWrapper.offset().top ) / this.zoom);    
    this.viewportCenter.x = xc;
    this.viewportCenter.y = yc;
    this.viewportCallback.showInCenter( xc, yc);
  }
  
  this.mouseStart = {
    clientX: e.clientX,
    clientY: e.clientY, 
    viewportCenterX: this.viewportCenter.x, 
    viewportCenterY: this.viewportCenter.y
  };
  e.preventDefault();
}

Thumbnail.prototype.mouseUp = function(e){
  this.mouseIsDown = false;
  e.preventDefault();
}

Thumbnail.prototype.mouseMove = function(e){
  if (this.mouseIsDown){
    var dx = (e.clientX - this.mouseStart.clientX) / this.zoom;
    var dy = (e.clientY - this.mouseStart.clientY) / this.zoom;
    // console.log("viewport moved "+dx+"x"+dy);
    this.viewportCallback.showInCenter( this.mouseStart.viewportCenterX + dx, this.mouseStart.viewportCenterY + dy);
  }
}

Thumbnail.prototype.viewportChanged = function(x,y,w,h){
  this.viewportCenter = {
    x: (x+(w/2)), 
    y: (y+(h/2))
  };
  $( this.viewportRect ).attr({
    "x": x,
    "y": y,
    "width": w,
    "height": h
  });
}
