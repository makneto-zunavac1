/***************************************************************************
 *   Copyright (C) 2011 by Lukáš Karas <lukas.karas@centrum.cz>            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/*
 * use pieces of code from svg-edit.googlecode.com (Licensed under the Apache License, Version 2)
 *
 * Svg-Edit autors:
 *
 * Copyright(c) 2010 Alexis Deveria
 * Copyright(c) 2010 Pavol Rusnak
 * Copyright(c) 2010 Jeff Schiller
 *
 */


(function() {

	// This fixes $(...).attr() to work as expected with SVG elements.
	// Does not currently use *AttributeNS() since we rarely need that.

	// See http://api.jquery.com/attr/ for basic documentation of .attr()

	// Additional functionality:
	// - When getting attributes, a string that's a number is return as type number.
	// - If an array is supplied as first parameter, multiple values are returned
	// as an object with values for each given attributes

	var proxied = jQuery.fn.attr, svgns = "http://www.w3.org/2000/svg";
	jQuery.fn.attr = function(key, value) {
		var len = this.length;
		if(!len) return this;
		for(var i=0; i<len; i++) {
			var elem = this[i];
			// set/get SVG attribute
			if(elem.namespaceURI === svgns) {
				// Setting attribute
				if(value !== undefined) {
					elem.setAttribute(key, value);
				} else if($.isArray(key)) {
					// Getting attributes from array
					var j = key.length, obj = {};

					while(j--) {
						var aname = key[j];
						var attr = elem.getAttribute(aname);
						// This returns a number when appropriate
						if(attr || attr === "0") {
							attr = isNaN(attr)?attr:attr-0;
						}
						obj[aname] = attr;
					}
					return obj;

				} else if(typeof key === "object") {
					// Setting attributes form object
					for(var v in key) {
						elem.setAttribute(v, key[v]);
					}
				// Getting attribute
				} else {
					var attr = elem.getAttribute(key);
					if(attr || attr === "0") {
						attr = isNaN(attr)?attr:attr-0;
					}

					return attr;
				}
			} else {
				return proxied.apply(this, arguments);
			}
		}
		return this;
	};

}());




// Static class for various utility functions
var Utils = this.Utils = function() {

	var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

	return {

		// Function: Utils.toXml
		// Converts characters in a string to XML-friendly entities.
		//
		// Example: "&" becomes "&amp;"
		//
		// Parameters:
		// str - The string to be converted
		//
		// Returns:
		// The converted string
		"toXml": function(str) {
			return $('<p/>').text(str).html();
		},

		// Function: Utils.fromXml
		// Converts XML entities in a string to single characters.
		// Example: "&amp;" becomes "&"
		//
		// Parameters:
		// str - The string to be converted
		//
		// Returns:
		// The converted string
		"fromXml": function(str) {
			return $('<p/>').html(str).text();
		},

		// This code was written by Tyler Akins and has been placed in the
		// public domain.  It would be nice if you left this header intact.
		// Base64 code from Tyler Akins -- http://rumkin.com

		// schiller: Removed string concatenation in favour of Array.join() optimization,
		//           also precalculate the size of the array needed.

		// Function: Utils.encode64
		// Converts a string to base64
		"encode64" : function(input) {
			// base64 strings are 4/3 larger than the original string
	//		input = Utils.encodeUTF8(input); // convert non-ASCII characters
			input = Utils.convertToXMLReferences(input);
			if(window.btoa) return window.btoa(input); // Use native if available
			var output = new Array( Math.floor( (input.length + 2) / 3 ) * 4 );
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0, p = 0;

			do {
				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output[p++] = _keyStr.charAt(enc1);
				output[p++] = _keyStr.charAt(enc2);
				output[p++] = _keyStr.charAt(enc3);
				output[p++] = _keyStr.charAt(enc4);
			} while (i < input.length);

			return output.join('');
		},

		// Function: Utils.decode64
		// Converts a string from base64
		"decode64" : function(input) {
			if(window.atob) return window.atob(input);
			var output = "";
			var chr1, chr2, chr3 = "";
			var enc1, enc2, enc3, enc4 = "";
			var i = 0;

			 // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
			 input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			 do {
				enc1 = _keyStr.indexOf(input.charAt(i++));
				enc2 = _keyStr.indexOf(input.charAt(i++));
				enc3 = _keyStr.indexOf(input.charAt(i++));
				enc4 = _keyStr.indexOf(input.charAt(i++));

				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;

				output = output + String.fromCharCode(chr1);

				if (enc3 != 64) {
				   output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
				   output = output + String.fromCharCode(chr3);
				}

				chr1 = chr2 = chr3 = "";
				enc1 = enc2 = enc3 = enc4 = "";

			 } while (i < input.length);
			 return unescape(output);
		},

		// Currently not being used, so commented out for now
		// based on http://phpjs.org/functions/utf8_encode:577
		// codedread:does not seem to work with webkit-based browsers on OSX
// 		"encodeUTF8": function(input) {
// 			//return unescape(encodeURIComponent(input)); //may or may not work
// 			var output = '';
// 			for (var n = 0; n < input.length; n++){
// 				var c = input.charCodeAt(n);
// 				if (c < 128) {
// 					output += input[n];
// 				}
// 				else if (c > 127) {
// 					if (c < 2048){
// 						output += String.fromCharCode((c >> 6) | 192);
// 					}
// 					else {
// 						output += String.fromCharCode((c >> 12) | 224) + String.fromCharCode((c >> 6) & 63 | 128);
// 					}
// 					output += String.fromCharCode((c & 63) | 128);
// 				}
// 			}
// 			return output;
// 		},

		// Function: Utils.convertToXMLReferences
		// Converts a string to use XML references
		"convertToXMLReferences": function(input) {
			var output = '';
			for (var n = 0; n < input.length; n++){
				var c = input.charCodeAt(n);
				if (c < 128) {
					output += input[n];
				}
				else if(c > 127) {
					output += ("&#" + c + ";");
				}
			}
			return output;
		},

		// Function: rectsIntersect
		// Check if two rectangles (BBoxes objects) intersect each other
		//
		// Paramaters:
		// r1 - The first BBox-like object
		// r2 - The second BBox-like object
		//
		// Returns:
		// Boolean that's true if rectangles intersect
		"rectsIntersect": function(r1, r2) {
			return r2.x < (r1.x+r1.width) &&
				(r2.x+r2.width) > r1.x &&
				r2.y < (r1.y+r1.height) &&
				(r2.y+r2.height) > r1.y;
		},

		// Function: snapToAngle
		// Returns a 45 degree angle coordinate associated with the two given
		// coordinates
		//
		// Parameters:
		// x1 - First coordinate's x value
		// x2 - Second coordinate's x value
		// y1 - First coordinate's y value
		// y2 - Second coordinate's y value
		//
		// Returns:
		// Object with the following values:
		// x - The angle-snapped x value
		// y - The angle-snapped y value
		// snapangle - The angle at which to snap
		"snapToAngle": function(x1,y1,x2,y2) {
			var snap = Math.PI/4; // 45 degrees
			var dx = x2 - x1;
			var dy = y2 - y1;
			var angle = Math.atan2(dy,dx);
			var dist = Math.sqrt(dx * dx + dy * dy);
			var snapangle= Math.round(angle/snap)*snap;
			var x = x1 + dist*Math.cos(snapangle);
			var y = y1 + dist*Math.sin(snapangle);
			//console.log(x1,y1,x2,y2,x,y,angle)
			return {x:x, y:y, a:snapangle};
		},

		// Function: text2xml
		// Cross-browser compatible method of converting a string to an XML tree
		// found this function here: http://groups.google.com/group/jquery-dev/browse_thread/thread/c6d11387c580a77f
		"text2xml": function(sXML) {
			var out;
			try{
				var dXML = ($.browser.msie)?new ActiveXObject("Microsoft.XMLDOM"):new DOMParser();
				dXML.async = false;
			} catch(e){
				throw new Error("XML Parser could not be instantiated");
			};
			try{
				if($.browser.msie) out = (dXML.loadXML(sXML))?dXML:false;
				else out = dXML.parseFromString(sXML, "text/xml");
			}
			catch(e){ throw new Error("Error parsing XML string"); };
			return out;
		},
    
    // Function: setUnitAttr
    // Sets an element's attribute based on the unit in its current value.
    //
    // Parameters: 
    // elem - DOM element to be changed
    // attr - String with the name of the attribute associated with the value
    // val - String with the attribute value to convert
    "setUnitAttr" : function(elem, attr, val) {
      if(!isNaN(val)) {
        // New value is a number, so check currently used unit
        var old_val = elem.getAttribute(attr);

        if(old_val !== null && isNaN(old_val)) {
          // Old value was a number, so get unit, then convert
          var unit;
          if(old_val.substr(-1) === '%') {
            var res = getResolution();
            unit = '%';
            val *= 100;
            if($.inArray(attr, w_attrs) !== -1) {
              val = val / res.w;
            } else if($.inArray(attr, h_attrs) !== -1) {
              val = val / res.h;
            } else {
              return val / Math.sqrt((res.w*res.w) + (res.h*res.h))/Math.sqrt(2);
            }

          } else {
            unit = old_val.substr(-2);
            val = val / unit_types[unit];
          }

          val += unit;
        }
      }
      elem.setAttribute(attr, val);
    }
    
	}

}();

/**
 * extend Object for clone function
 * function copied from blog: http://my.opera.com/GreyWyvern/blog/show.dml/1725165
 */
Object.prototype.clone = function() {
  var newObj = (this instanceof Array) ? [] : {};
  for (i in this) {
    if (i == 'clone') continue;
    if (this[i] && typeof this[i] == "object") {
      newObj[i] = this[i].clone();
    } else newObj[i] = this[i]
  } return newObj;
};

