/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <qt4/QtOpenGL/qgl.h>
#include <QGraphicsScene>
#include <qt4/QtDeclarative/qdeclarativeitem.h>

#include "view-graber.h"

ViewGraber::ViewGraber(QDeclarativeItem *parent) :
QDeclarativeItem(parent),
scheduledSnapshot(false),
paintSnapshot(false) {

}

ViewGraber::~ViewGraber() {
  //qDebug() << "ViewGraber: destroyed";
}

void ViewGraber::takeSnapshot() {
  //QDeclarativeItem *parent = parentItem();
  if (!_delegate)
    return;

  scheduledSnapshot = true;
  paintSnapshot = true;

  setFlag(QGraphicsItem::ItemHasNoContents, false);
  update(_delegate->boundingRect());
}

void ViewGraber::releaseSnapshot() {
  paintSnapshot = false;
}

void ViewGraber::paint(QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0) {
  Q_UNUSED(option);

  if (!_delegate)
    return;

  if (scheduledSnapshot) {
    QGLWidget *qgl = reinterpret_cast<QGLWidget *> (widget);
    if (!qgl) {
      qWarning() << "ViewGraber: cast" << widget << "to QGLWidget failed";
      return;
    }

    _delegate->paint(painter, option, widget);
    scheduledSnapshot = false;
    snapshot = qgl->grabFrameBuffer(false);
    qDebug() << "MyWebView: take snapshot ";
    emit snapshotTaken();
  }

  if (!paintSnapshot)
    return;

  QPointF sceneMapRect = _delegate->mapToScene(0, 0);
  QPointF selfMapRect = ((QGraphicsItem*) _delegate)->mapToItem((QGraphicsItem*) this, QPointF(0, 0));
  //rect = this->scene()->mapToItem(this, rect);
  //qDebug() << "MyWebView: paint" << sceneMapRect << selfMapRect;
  painter->drawImage((sceneMapRect.x() - selfMapRect.x()) *-1, (sceneMapRect.y() - selfMapRect.y()) *-1, snapshot);
}

#include "view-graber.moc"
