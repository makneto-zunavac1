/**
 * Copyright 2010 Max Desyatov <max.desyatov@gmail.com>
 * 
 * https://github.com/explicitcall/QMLbox
 * 
 * Warning: license is unknown!
 */

#include "wheelarea.h"

void WheelArea::wheelEvent(QGraphicsSceneWheelEvent *event) {
  switch (event->orientation()) {
    case Qt::Horizontal:
      emit horizontalWheel(event->delta());
      break;
    case Qt::Vertical:
      emit verticalWheel(event->delta());
      break;
    default:
      event->ignore();
      break;
  }
}

#include "wheelarea.moc"
