/* 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDebug>
#include <qt4/QtNetwork/qnetworkreply.h>

// backend
#include "../backend/accounts-model.h"

#include "contacts-sort-filter-model-proxy.h"
#include "contacts-model-proxy.h"
#include "telepathytypes.h"

ContactsSortFilterModelProxy::ContactsSortFilterModelProxy(ContactsModelProxy *contactsModel, QObject *parent) :
QSortFilterProxyModel(parent),
_hideOffline(false),
_lastRowCount(0),
_contactsModel(contactsModel) {


  setSourceModel(_contactsModel);
  setDynamicSortFilter(true); // ... dynamically sorted and filtered whenever the contents of the source model change.

  connect(_contactsModel, SIGNAL(contactCountChanged()),
    this, SIGNAL(contactCountChanged()));

  connect(this, SIGNAL(layoutChanged()),
    this, SLOT(rowCountChanged()));

  connect(this, SIGNAL(rowsRemoved(const QModelIndex, int, int)),
    this, SLOT(rowCountChanged()));

  connect(this, SIGNAL(rowsInserted(const QModelIndex, int, int)),
    this, SLOT(rowCountChanged()));
}

bool ContactsSortFilterModelProxy::lessThan(const QModelIndex &left,
  const QModelIndex &right) const {
  QVariant leftData = sourceModel()->data(left);
  QVariant rightData = sourceModel()->data(right);

  //qDebug() << "ContactsSortFilterModelProxy: lessThan" << left.row() << right.row() << "?";
  // TODO implement this;
  return true;
}

bool ContactsSortFilterModelProxy::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
  if (_hideOffline) {
    QVariant presenceType = _contactsModel->data(source_row, MaknetoBackend::AccountsModel::PresenceTypeRole);
    if (presenceType.toInt() == TelepathyTypes::ConnectionPresenceTypeOffline)
      return false;
  }
  if (_contactStringFilter.length() > 0){      
    QString filterToLower = _contactStringFilter.toLower();
    QString id = ( _contactsModel->data(source_row, MaknetoBackend::AccountsModel::IdRole) ).toString().toLower();
    QString alias = ( _contactsModel->data(source_row, MaknetoBackend::AccountsModel::AliasRole) ).toString().toLower();
    
    if ((!id.contains(filterToLower)) && (!alias.contains(filterToLower))){
      return false;
    }
  }
  return true;
}

QVariant ContactsSortFilterModelProxy::data(int row, int role) {
  return QSortFilterProxyModel::data(index(row, 0, QModelIndex()), role);
}

bool ContactsSortFilterModelProxy::hideOffline() {
  return _hideOffline;
}

void ContactsSortFilterModelProxy::setHideOffline(bool hide) {
  if (_hideOffline == hide)
    return;

  //qDebug() << "ContactsSortFilterModelProxy: hide offline:" << hide;
  _hideOffline = hide;
  filterChanged();
}

void ContactsSortFilterModelProxy::setContactStringFilter(QVariant filter) {
  QString old = _contactStringFilter;
  _contactStringFilter = filter.toString();
  if (_contactStringFilter == old)
    return;
  //qDebug() << "ContactsSortFilterModelProxy: string filter:" << _contactStringFilter;
  filterChanged();
}

QVariant ContactsSortFilterModelProxy::contactStringFilter() {
  return QVariant::fromValue<QString>(_contactStringFilter);
}

int ContactsSortFilterModelProxy::contactCount() const {
  return rowCount(QModelIndex());
}

void ContactsSortFilterModelProxy::rowCountChanged() {
  int count = rowCount(QModelIndex());
  if (count == _lastRowCount) // really chaged?
    return;

  _lastRowCount = count;
  emit contactCountChanged();
}

#include "contacts-sort-filter-model-proxy.moc"

