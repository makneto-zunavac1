/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
//import QtMultimediaKit 1.1
import org.makneto 0.1 as Makneto

Rectangle {
    id: sessionController
    color: parent.color

    property variant scenes: {} // associative array (by session id) of opened sessions
    property variant topScene: undefined // session id of scene that is on top, or udefined when contact list is on top
    property variant sessionSceneComponent : Qt.createComponent("SessionScene.qml");
    property variant _model: ListModel {}

    function log(msg){
        console.log("II [SessionController.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [SessionController.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [SessionController.qml]: "+msg);
    }

    function getSessionModel(){
        return _model;
    }

    function onSessionsModelChanged(model){
        sessionController._model = model;
        // connect to signals from session model
        try{
            sessionController._model.sessionOpened.connect(sessionOpened);
            sessionController._model.sessionClosed.connect(sessionClosed);
            sessionController._model.whiteboardMessageReceived.connect(whiteboardMessageReceived);
            sessionController._model.textMessageReceived.connect(textMessageReceived);
            sessionController._model.chatStateChanged.connect(chatStateChanged);
            sessionController._model.sessionTypeChanged.connect(sessionTypeChanged);
            sessionController._model.messageSent.connect(messageSent);

            /*
            sessionController._model.startBeeping.connect(startBeeping);
            sessionController._model.stopBeeping.connect(stopBeeping);
            sessionController._model.startRinging.connect(startRinging);
            sessionController._model.stopRinging.connect(stopRinging);
            */

        }catch(e){
            error("error while connecting to signals "+JSON.stringify(e));
        }
    }

    /*
    function startBeeping(){
        log("startBeeping");
        //beeper.start();
    }

    function stopBeeping(){
        log("stopBeeping");
        //beeper.stop();
    }

    function startRinging(){
        log("startRinging");
        //ringer.start();
    }

    function stopRinging(){
        log("stopRinging");
        //ringer.stop();
    }
    */

    function whiteboardMessageReceived(sessionId, data, contact){
        try{
            var scene = scenes[ sessionId ];
            if (scene===undefined){
                error("whiteboard message recived on session "+sessionId+" but this session doesn't exists ("+contact+":"+data+")");
                return;
            }
            scene.whiteboardMessageReceived(data, contact);
        }catch(e){
            error("error "+JSON.stringify(e));
        }
    }
    function messageSent(sessionId, content){
        try{
            var scene = scenes[ sessionId ];
            if (scene===undefined){
                error("text message recived on session "+sessionId+" but this session doesn't exists ("+contact+":"+text+")");
                return;
            }
            scene.messageSent(content);
        }catch(e){
            error("error "+JSON.stringify(e));
        }
    }
    function textMessageReceived(sessionId, text, contact){
        try{
            var scene = scenes[ sessionId ];
            if (scene===undefined){
                error("text message recived on session "+sessionId+" but this session doesn't exists ("+contact+":"+text+")");
                return;
            }
            scene.textMessageReceived(text, contact);
        }catch(e){
            error("error "+JSON.stringify(e));
        }
    }
    function chatStateChanged(sessionId, chatState, contact){
        try{
            var scene = scenes[ sessionId ];
            if (scene===undefined){
                error("chat state change in session "+sessionId+" but this session doesn't exists");
                return;
            }
            scene.chatStateChanged(chatState, contact);
        }catch(e){
            error("error "+JSON.stringify(e));
        }
    }
    function sessionTypeChanged(sessionId, type){
        try{
            var scene = scenes[ sessionId ];
            if (scene===undefined){
                error("session type changed in session "+sessionId+" but this session doesn't exists");
                return;
            }
            scene.sessionTypeChanged(type);
        }catch(e){
            error("error "+JSON.stringify(e));
        }
    }

    function showContactList(){
        log("show contact list");
        for (var i in scenes){
            var sc = scenes[i]
            if (sc.sessionId == topScene){
                sc.z = 2;
            }else{
                sc.z = 0;
            }
        }
        contactListScene.z = 1;
        if (topScene !== undefined){
            var sc = scenes[topScene]
            sc.state = "right"
        }
        topScene = undefined;
    }

    function closeSession(sessionId){
        log("close session "+sessionId);
        _model.closeSession(sessionId);
        //showContactList();
    }

    // I don't know how make it better
    function objRemove(obj, id){
        var tmp = {}
        for (var i in obj){
            if (i !== id)
                tmp[i] = obj[i];
        }
        return tmp;
    }

    function sessionClosed(sessionId){
        var scene = scenes[ sessionId ];
        if (scene===undefined){
            error("scene for session "+sessionId+" doesn't exists");
            return;
        }
        if (topScene == sessionId){
            showContactList();
        }
        //log("scene before delete = "+scenes[ sessionId ]);
        scenes = objRemove(scenes, sessionId);
        //log("scene after delete = "+scenes[ sessionId ]);
        scene.destroyLater();
    }

    function sessionOpened(sessionId, extendExisting, requeted){
        log("create new session for "+sessionId+" ("+extendExisting+", "+requeted+")");

        //log("we play russian rulete with scenes...");
        var scene = scenes[ sessionId ];
        //log("scene is "+scene);
        if (scene !== undefined){
            if (requeted){
                activateSession(sessionId); // we requested session that already exists, just activate it
            }else if (!extendExisting){
                error("scene with id "+sessionId+" already exists");
            }
            return;
        }

        if(sessionSceneComponent.status != Component.Ready){
            error("Component "+sessionSceneComponent.url+" is not ready!");
            error(sessionSceneComponent.errorString());
            return false;
        }

        var session = sessionSceneComponent.createObject(sessionController);
        if(session === null){
            error("error creating session scene");
            error(sessionSceneComponent.errorString());
            return false;
        }
        var tmp = scenes;
        tmp[ sessionId ] = session;
        scenes = tmp;

        //log("scenes: "+JSON.stringify(scenes));

        session.state = "right";
        session.animate = true;
        session.sessionId = sessionId;
        session.setSessionsModel(_model);
        activateSession(sessionId);
    }

    function activateSession(id){
        log("activate scene "+id)
        //log("scenes: "+JSON.stringify(scenes));

        var scene = scenes[ id ];
        if (scene === undefined){
            error("scene for session "+id+" doesn't exists");
            showContactList();
            return;
        }
        if (topScene == id){
            log("top scene already is "+id);
            return;
        }
        for (var i in scenes){
            var sc = scenes[i]
            if (sc.sessionId == topScene){
                sc.z = 1;
            }else{
                sc.z = 0;
            }
        }
        contactListScene.z = (topScene === undefined)? 1: 0;
        scene.animate = false;
        scene.state = "right";

        scene.z = 2
        topScene = id;
        scene.animate = true;
        scene.state = "center";
        log("topScene: "+topScene);
    }
    function requestSession(managerId, accountId, type){
        log("request session for "+ managerId+" / "+accountId+" (type "+type+")");
        _model.requestSession(managerId, accountId, type);
    }
    function getTopScene(){
        return topScene;
    }

    Component.onCompleted: {
        scenes = {};
        //main.addSessionsModelListener(sessionController);
        main.sessionsModelChanged.connect(onSessionsModelChanged);
    }

    /*
    SoundEffect {
         id: beeper
         loops: 100
         source: "/sounds/telephonebeep.mp3"
     }
    SoundEffect {
         id: ringer
         loops: 100
         source: "/sounds/telephonering.mp3"
     }
     */

    ContactListScene{
        id: contactListScene
        color: parent.color
        anchors.fill: parent
        z: 2
    }
}
