/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
import "components"

Item {
    height: main.toolbarHeight
    SystemPalette{ id: syspal }

    Rectangle{
        id: topPanel
        anchors{top: parent.top; left: parent.left; right: parent.right}
        height: parent.height
        color: "transparent"

        gradient: Gradient {
            GradientStop { position: 0.0; color: Qt.rgba(0.086, 0.086, 0.086, 1) }
            GradientStop { position: 0.9; color: Qt.rgba(0.086, 0.086, 0.086, 0.9)}
            GradientStop { position: 1.0; color: Qt.rgba(0.086, 0.086, 0.086, 0.5)}
        }

        Component.onCompleted: {
            if (main.useSyspal){
                var col = syspal.shadow;
                // use system palete to gradient :)
                topPanel.gradient.stops[ 0 ].color = "#ff"+(col+"").substring(1);
                topPanel.gradient.stops[ 1 ].color = "#e5"+(col+"").substring(1);
                topPanel.gradient.stops[ 2 ].color = "#7f"+(col+"").substring(1);
            }
        }

        Rectangle{
            id: topLine
            anchors{top: parent.bottom; left: parent.left; right: parent.right}
            height: 1
            color: "transparent"

            Image {
                id: topLineImage
                source: "img/line-horizontal.svg"
                anchors.fill: parent
            }
        }
    }
}
