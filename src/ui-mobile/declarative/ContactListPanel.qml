/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
import org.makneto 0.1 as Makneto
import "components"

TopPanel {
  id: contactListPanel

  property variant _model: ListModel {}


  function log(msg){
      console.log("II [ContatListPanel.qml]: "+msg);
  }
  function error(msg){
      console.log("EE [ContatListPanel.qml]: "+msg);
  }
  function warn(msg){
      console.log("WW [ContatListPanel.qml]: "+msg);
  }
  function onContactsModelChanged(model){
      _model = model;
  }
  function changeTextFilter(text){
      //log("contact filter: "+text);
      backGroundText.text = (text.length > 0)?"": backGroundText.defaultText;
      _model.contactStringFilter = text;
      // TODO: return filtered contacts as array
      return [];
  }

  Component.onCompleted: {
      //main.addContactsModelListener( contactListPanel );
      main.contactsModelChanged.connect(onContactsModelChanged);
    }

  SystemPalette{ id: syspal }

  Rectangle{
      id: topPanel
      anchors{top: parent.top; left: parent.left; right: parent.right}
      height: parent.height
      color: "transparent"

      GlobalStatus{
          id: accountManager
          BorderImage { source: "img/lineedit.sci"; anchors.fill: parent; opacity: .5 }

          anchors.top: parent.top
          anchors.right: parent.right;
          anchors.bottom: parent.bottom
          anchors.left: filterText.right
          anchors.leftMargin: 20
          anchors.topMargin: 2
          anchors.bottomMargin: 5
          height: parent.height
      }


      Rectangle{
          id: filterText
          anchors{ verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter}
          width: (parent.width * 0.3)
          height: (parent.height - 10)
          BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
          color: "transparent"

          TextInput{
              id: editor
              anchors {
                  left: parent.left; right: parent.right; leftMargin: 10; rightMargin: 10
                  verticalCenter: parent.verticalCenter
              }
              cursorVisible: true; font.bold: true
              color:  main.useSyspal? syspal.windowText :"white"
              selectionColor: "gray"

              Keys.onReleased: {
                  var items = changeTextFilter(editor.text);
                  if (items.length == 1 && contactsFlickable.interactive && event.key == Qt.Key_Return){
                      log("show enter contact details ");
                      //items[0].component.state = "detail"
                  }
              }
              focus: true
          }

          Keys.forwardTo: [ (editor)]

          Text {
              id: backGroundText
              text: defaultText
              property string defaultText: "Search contact..."

              color:  main.useSyspal? syspal.highlightedText :"white"
              anchors{verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: 10}
              opacity: 0.2
              font.italic: true
              font.bold: false
              style: Text.Raised; styleColor:  main.useSyspal? syspal.highlightedText :"white"
          }

      }

      Rectangle{
          id: checkBoxWrapper
          //border{ color: "white"; width: 1 }
          width: showOfflineCheckBox.width + checkBoxText.width + 10
          height: 20
          color: "transparent"
          anchors.verticalCenter: filterText.verticalCenter
          anchors.left: parent.left
          anchors.leftMargin: 10

          CheckBox {
              id: showOfflineCheckBox
              height: parent.height
              width: height
              checked: true
              anchors.left: parent.left
              onCheckedChanged: (contactListPanel._model.hideOffline !== undefined)? contactListPanel._model.hideOffline = !checked: 0
          }
          Text {
              id: checkBoxText
              text: "Show offline contacts"
              anchors.right: parent.right
              anchors.verticalCenter: parent.verticalCenter
              color:  main.useSyspal? syspal.highlightedText :"white"
          }
          MouseArea{
              anchors.fill: parent
              onClicked: showOfflineCheckBox.checked = !showOfflineCheckBox.checked
          }
      }
  }

}
