/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.0

Rectangle {
    id: myDelegate

    property int count: -1
    property variant last: (count - 1 ===index)
    property variant first: (index === 0)

    property variant topMargin: 5
    property variant bottomMargin: 2
    property variant leftMargin: 6
    property variant rightMargin: 4

    color: "transparent"

    SystemPalette{ id: syspal }

    Rectangle{
        id: topBorder
        color: main.useSyspal? syspal.light :"white"
        opacity: first? 0: .03
        height: 1
        anchors{top: parent.top; left: parent.left; right: parent.right}
    }

    Rectangle{
        id: bottomBorder
        color: main.useSyspal? syspal.shadow :"black"
        opacity: last? 0 : .2
        height: 1
        anchors{bottom: parent.bottom; left: parent.left; right: parent.right}
    }
}
