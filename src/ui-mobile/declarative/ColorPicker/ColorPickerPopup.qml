import QtQuick 1.0
import Qt 4.7
import QtWebKit 1.0
import ".."
import "../components"

AbstractPopup {
    id: popup
    /*
    width: 300
    height: 320
    */

    property variant color: "#ff0000"
    finalWidth : 320
    finalHeight : 360

    signal colorChanged(variant color)

    function log(msg){
        console.log("II [ColorPickerPopup.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [ColorPickerPopup.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [ColorPickerPopup.qml]: "+msg);
    }

    Rectangle{
        id: background
        anchors.fill: wrapper
        opacity: wrapper.opacity
        color: "#323235"
        radius: (width*0.03)

        Rectangle{
            id: border
            anchors.fill: parent
            radius: parent.radius
            border{ color: "white"; width: 1 }
            color: "transparent"
            opacity: 0.25
            //opacity: 1.0
        }

        Rectangle{
            id: webViewWrapper
            anchors.margins: 10
            anchors.fill: parent
            color: parent.color

            WebView{
                id: pickerWb
                anchors.fill: parent
                settings.pluginsEnabled: false
                pressGrabTime:0


                javaScriptWindowObjects: [QtObject {
                        WebView.windowObjectName: "connector"
                        function colorChanged(c){
                            //log("color changed: "+c);
                            color = c;
                            popup.colorChanged(color);
                        }
                        function ready(){
                            log("ready...");
                            pickerWb.evaluateJavaScript ( 'cp.color("'+color+'");' );
                        }
                    },
                    QtObject {
                        WebView.windowObjectName: "console"
                        function log(msg)   { console.log("II [ColorPicker]: " + msg); }
                        function debug(msg) { console.log("DD [ColorPicker]: " + msg); }
                        function warn(msg)  { console.log("WW [ColorPicker]: " + msg); }
                        function error(msg) { console.log("EE [ColorPicker]: " + msg); }
                    }
                ]

                url:"ColorPicker.html"
            }
        }
    }
}
