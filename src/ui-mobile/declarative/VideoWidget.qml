/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.0
import Qt 4.7
import Gst 1.0

Rectangle {
    id: videoWidget
    /*
    width: 100
    height: 62
    */
    color: "black"


    function log(msg){
        console.log("II [VideoWidget.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [VideoWidget.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [VideoWidget.qml]: "+msg);
    }
    function setSessionsModel(model){
        chatWidget.sendMessage.connect(model.sendTextMessage);
    }


    state: "normal"
    states: [
        State {
            name: "normal"
            PropertyChanges {
                target: stateIcon
                source: "img/fullscreen.png"
            }
            AnchorChanges {
                target: videoWidget;
                anchors.left: board.right;
                anchors.bottom: board.bottom
            }
        },
        State {
            name: "fullscreen"
            PropertyChanges {
                target: stateIcon
                source: "img/minimize.png"
            }
            AnchorChanges {
                target: videoWidget;
                anchors.left: videoWidget.parent.left;
                anchors.bottom: videoWidget.parent.bottom
            }
        }
    ]

    property variant session;
    property variant emptyVideoSurface: VideoSurface{}
    property variant emptyPreviewSurface: VideoSurface{}

    function videoSurfaceReady(type, sessionId, newSurface){
        if (sessionId != session.sessionId)
            return;

        try{
            log("video surface ready \""+type+"\" \""+sessionId+"\" "+newSurface+"");
            //log(JSON.stringify());

            if (type=="preview"){
                log("setting surface for previewVideoItem");
                previewVideoItem.surface = newSurface;
            }else{
                log("setting surface for mainVideoItem");
                mainVideoItem.surface = newSurface;
            }
        }catch (e){
            error(JSON.stringify(e));
        }
    }
    function videoSurfaceRemoved(type, sessionId){
        if (sessionId != session.sessionId)
            return;

        try{
            log("video surface removed \""+type+"\" \""+sessionId+"\"");
            //log(JSON.stringify(surface));
            if (type=="preview"){
                previewVideoItem.surface = emptyPreviewSurface;
            }else{
                mainVideoItem.surface = emptyVideoSurface;
            }
        }catch (e){
            error(JSON.stringify(e));
        }

    }

    Component.onCompleted:{
        videoWidget.session = videoWidget.parent;
        main.videoSurfaceReady.connect(videoSurfaceReady);
        main.videoSurfaceRemoved.connect(videoSurfaceRemoved);
    }


    Behavior on width {
        NumberAnimation{duration: 200}
    }
    Behavior on opacity {
        NumberAnimation{duration: 200}
    }
    Behavior on height {
        NumberAnimation{duration: 200}
    }

    transitions: Transition {
           // smoothly reanchor myRect and move into new position
           AnchorAnimation { duration: 200 }
       }


    Rectangle{
        id: video
        color: parent.color
        BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
        anchors{fill: parent; margins:4 }

        Image{
            source: "img/cam.svg"
            anchors.centerIn: parent;
            width: Math.min( parent.width / 3, parent.height / 3)
            height: width
        }

        Rectangle{
            id: videoWrapper
            color: "transparent"
            anchors.fill: parent

            VideoItem {
              id: mainVideoItem
              surface: emptyVideoSurface
              size: Qt.size(parent.width, parent.height)
              //anchors.fill: parent
            }
        }
        Rectangle{
            anchors{left: parent.left; bottom: parent.bottom}
            width: Math.max(80, Math.min(320, parent.width /3));
            height: (width / 4) * 3
            color: "#090909"

            VideoItem {
              id:previewVideoItem
              surface: emptyPreviewSurface
              size: Qt.size(parent.width, parent.height)
            }
        }


        Image{
            id: stateIcon
            source: "img/fullscreen.png"
            anchors{right: parent.right; bottom: parent.bottom}
            width: 64
            height: width

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    //videoWidget.state = videoWidget.state == "normal"? "fullscreen": "normal"
                    if (videoWidget.state == "normal")
                        videoWidget.parent.fullScreenVideo();
                    else
                        videoWidget.parent.minimizeVideo();
                }
            }
        }
    }

}
