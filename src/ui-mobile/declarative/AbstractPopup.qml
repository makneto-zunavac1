/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import QtQuick 1.0

Rectangle {
    id: popup
    anchors{fill:parent}

    property variant wrapper: _wrapper
    property int animationInLength: 500
    property int animationOutLength: 500
    property int popupFromX: parent.width
    property int popupFromY: 0
    property int finalWidth: parent.width * 0.8
    property int finalHeight: parent.height * 0.8
    property int minCorner: main.toolbarHeight
    //property int minCorner: 50
    property int finalX: (popupFromX - finalWidth) < minCorner ? minCorner: popupFromX - finalWidth
    property int finalY: (popupFromY + finalHeight) > parent.height - minCorner ? parent.height - finalHeight - minCorner: popupFromY
    property bool destroyOnHide: true

    function log(msg){
        console.log("II [AbstractPopup.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [AbstractPopup.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [AbstractPopup.qml]: "+msg);
    }

    function hidePopup(){
        popup.state = "hide";
        if (destroyOnHide)
            destroyTimer.start();
    }

    Timer {
        id: destroyTimer
        interval: animationOutLength; running: false; repeat: false
        onTriggered: {
            //log("destroy");
            popup.destroy();
        }
    }

    color: "transparent"
    state: "hide"

    states: [
        State {
            name: "hide"
            PropertyChanges { target: background; opacity: 0 }
            PropertyChanges { target: backgroundMouseArea; enabled: false}
            PropertyChanges { target: wrapper; width: 0; height: 0; x: popupFromX; y: popupFromY; opacity: 0}
        },
        State {
            name: "visible"
            PropertyChanges { target: background; opacity: .5 }
            PropertyChanges { target: backgroundMouseArea; enabled: true}
            PropertyChanges { target: wrapper; width: finalWidth; height: finalHeight; x: finalX; y: finalY; opacity: 1}
        }
    ]
    transitions: [
        Transition {
            from: "hide"; to: "visible"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationInLength }
            NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad; duration: animationInLength }
            NumberAnimation { properties: "opacity"; easing.type: Easing.InOutQuad; duration: animationInLength }
        },
        Transition {
            from: "visible"; to: "hide"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationOutLength }
            NumberAnimation { properties: "x,y"; easing.type: Easing.InOutQuad; duration: animationOutLength }
            NumberAnimation { properties: "opacity"; easing.type: Easing.InOutQuad; duration: animationOutLength }
        }
    ]

    SystemPalette { id: syspal }

    Rectangle{
        id: background
        color: "black"
        anchors{fill:parent}

        MouseArea{
            id: backgroundMouseArea
            anchors{fill:parent}
            onClicked: {
                hidePopup();
            }
        }
    }
    Rectangle{
        id: _wrapper
        color: "transparent"
        MouseArea{
            id: wrapperMouseArea
            anchors{fill:parent}
        }
    }
    Rectangle{
        height : main.toolbarHeight
        width: height
        radius : width / 2
        z: 10
        border.color: main.useSyspal? syspal.light :"white"
        border.width: 1
        color: main.useSyspal? syspal.mid :"#323235"
        anchors.horizontalCenter: _wrapper.right
        anchors.verticalCenter: _wrapper.top
        opacity: _wrapper.opacity

        /*
        x: _wrapper.x + _wrapper.width - (width /2)
        y: _wrapper.y - (height /2)
        */

        Image {
            id: crossIcon
            source: "img/cross.png"
            //anchors.fill: parent
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * .6
            height: width
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                hidePopup();
            }
        }
    }
}
