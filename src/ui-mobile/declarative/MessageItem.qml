/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import QtQuick 1.0

Item {
    id: messageItem

    property bool incoming: false
    property string sender: "you"
    property variant time : new Date();
    property string message : ""

    property int margins: 8

    anchors.margins: margins
    height: statusText.height + messageText.height + (2 * margins)
    width: parent.width

    SystemPalette{ id: syspal }

    Rectangle{
        id: background
        color: incoming? "red":"green"
        opacity: 0.07
        anchors.fill: parent
        radius: margins
        anchors.margins: margins / 4;
    }
    Rectangle{
        anchors.margins: margins;
        anchors.fill: parent;
        color: "transparent"

        Text {
            id: statusText
            text: main.formatTime(time)+" "+sender+": "
            color: main.useSyspal? syspal.windowText :"gray"
            opacity: 0.6
            anchors{top: parent.top; right: parent.right; left: parent.left}
        }
        Text {
            id: messageText
            text: message
            width: parent.width
            anchors{top: statusText.bottom; right: parent.right; left: parent.left}
            wrapMode: Text.Wrap
            color: main.useSyspal? syspal.windowText :"white"
        }
    }
}
