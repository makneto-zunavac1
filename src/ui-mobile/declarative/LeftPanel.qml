/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import Qt 4.7
import QtQuick 1.0
import "./components/styles/default" as DefaultStyles
import "components"
import org.makneto 0.1 as Makneto

Rectangle{
    id: leftPanel
    SystemPalette{ id: syspal }
    color: main.useSyspal ? syspal.mid : "#262626"

    property variant expandedWidth : 200
    property variant hidedWidth : 0
    property variant animationLength: 200

    property variant _model: ListModel {}
    property variant _notifications: ListModel {}

    function onSessionsModelChanged(model){
        log("set sessions model");
        leftPanel._model = model;
    }
    function onNotificationsModelChanged(model){
        log("set notifications model");
        leftPanel._notifications = model;
    }

    function log(msg){
        console.log("II [LeftPanel.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [LeftPanel.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [LeftPanel.qml]: "+msg);
    }

    states: [
        State {
            name: "expanded"
            PropertyChanges { target: leftPanelShadow; opacity: 1}
            PropertyChanges { target: leftPanel; width: expandedWidth}
            PropertyChanges { target: clipImage; source: "img/clip-hide.png"}
        },
        State {
            name: "hided"
            PropertyChanges { target: leftPanelShadow; opacity: 0}
            PropertyChanges { target: leftPanel; width: hidedWidth}
            PropertyChanges { target: clipImage; source: "img/clip-show.png"}
        },
        State {
            name: "hold"
            PropertyChanges { target: leftPanelShadow; opacity: 1}
        }
    ]
    transitions: [
        Transition {
            from: "hided"; to: "expanded"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationLength }
        },
        Transition {
            from: "expanded"; to: "hided"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationLength }
        },
        Transition {
            from: "hold"; to: "expanded"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationLength /2 }
        },
        Transition {
            from: "hold"; to: "hided"
            NumberAnimation { properties: "width,height"; easing.type: Easing.InOutQuad; duration: animationLength /2 }
        },
        Transition {
            NumberAnimation { properties: "opacity"; easing.type: Easing.InOutQuad; duration: animationLength }
        }
    ]

    state: "expanded"
    width: expandedWidth

    Component.onCompleted : {
        main.sessionsModelChanged.connect(onSessionsModelChanged);
        main.notificationsModelChanged.connect(onNotificationsModelChanged);
    }

    Image{
        id: leftPanelShadow
        anchors{top: parent.top; bottom: parent.bottom; left: parent.right}
        width: 15

        source: "./img/shadow-vertical.svg"
        clip: true
        z: -1
    }

    Rectangle{
        id: clip
        anchors.left: parent.right
        anchors.top: parent.top
        anchors.topMargin: leftPanel.height / 4
        width: 30
        height: 100
        color: leftPanel.color

        //property bool hold : false
        property int dragStartX:0
        property variant dragStartWidth:0

        Image {
            id: clipImage
            source: "img/clip-hide.png"
            width: clip.width
            height: width*2
            anchors.horizontalCenter: clip.horizontalCenter
            anchors.verticalCenter: clip.verticalCenter
        }

        MouseArea{
            id: clipMouseArea
            anchors.fill: parent

            onClicked: {
                var pos = clipMouseArea.mapToItem(main,mouseX, mouseY);
                var div = Math.abs(clip.dragStartX - pos.x);
                if (div > 10){
                    //log("ignore clicked "+div);
                    return;
                }
                //log("clicked "+div+" "+leftPanel.state);

                leftPanel.state = (leftPanel.state == "hided")? "expanded": "hided";
            }
            onReleased: {
                if (clip.dragStartWidth == leftPanel.hidedWidth){
                    leftPanel.state = (leftPanel.width - leftPanel.hidedWidth) > ((leftPanel.expandedWidth - leftPanel.hidedWidth) * .25) ?
                            "expanded": "hided";
                }else{
                    leftPanel.state = (leftPanel.width - leftPanel.hidedWidth) < ((leftPanel.expandedWidth - leftPanel.hidedWidth) * .75) ?
                            "hided":"expanded";
                }

                //log("onReleased "+leftPanel.state);
            }
            function startDrag(mouseX, mouseY){
                var pos = clipMouseArea.mapToItem(main,mouseX, mouseY);
                var currentWidth = leftPanel.width;

                leftPanel.state = "hold";
                leftPanel.width = currentWidth;
                clip.dragStartX = pos.x;
                clip.dragStartWidth = leftPanel.width;

                //log("start hold w: "+leftPanel.width+", x: "+pos.x);
            }

            onPressAndHold: {
                startDrag(mouseX, mouseY);
            }
            onPressed: {
                startDrag(mouseX, mouseY);
            }
            onMousePositionChanged: {
                if (leftPanel.state != "hold")
                    return

                var pos = clipMouseArea.mapToItem(main,mouseX, mouseY);
                //log("move w: "+clip.dragStartWidth +" + x: "+ pos.x +" - s: "+ clip.dragStartX);
                var w = clip.dragStartWidth + (pos.x - clip.dragStartX);
                if (w < leftPanel.hidedWidth)
                    w = leftPanel.hidedWidth;
                if (w > leftPanel.expandedWidth)
                    w = leftPanel.expandedWidth;
                leftPanel.width = w;
            }
        }
    }

    Button{
        id: contactListButton
        anchors{margins: 6; top: parent.top; right: parent.right}
        width: leftPanel.expandedWidth - (contactListButton.anchors.margins * 2)
        text: "Show Contact List"
        onClicked: {
            main.getSessionController().showContactList();
        }
    }

    Text{
        id: sessionsLabel
        anchors{ top: contactListButton.bottom; right: parent.right;  }
        anchors{ topMargin: 12; leftMargin: 6; bottomMargin: 10; rightMargin: 6}
        width: leftPanel.expandedWidth - (contactListButton.anchors.margins * 2)
        color: main.useSyspal? syspal.windowText :"white"
        text: "Opened sessions:"
    }

    ListView {
        id: sessionsView
        model: leftPanel._model;
        anchors{margins: 6; topMargin: 20; top: sessionsLabel.bottom; right: parent.right; bottom: notificationsView.top}
        width: leftPanel.expandedWidth - (contactListButton.anchors.margins * 2)
        clip: true

        Timer {
            id: sessionCloseTimer
            property variant sessionId:0
            interval: 10; running: false; repeat: false
            onTriggered: {
                /*  this method destroys this ListViewLine (id notificationLine).
                    invoking this code directly from Button's method onClicked
                    causes application SEGFAULT! This is workaround, we invoke
                    it from timer...
                 */
                main.getSessionController().closeSession(sessionId);
            }
        }

        delegate: ListViewLine{
            id: sessionLine
            width: sessionsView.width
            property variant sessionId : id
            property variant titleText: ((type & Makneto.Session.SessionTypeAudio) || (type & Makneto.Session.SessionTypeVideo)?
                                             "call":"chat")+" with "+sessionName
            property variant statusText: lastMessage // sessionId
            property variant iconSource: icon !== ""?
                                             "file:"+ icon : "qrc:/declarative/img/default_avatar.jpg"

            property variant titleTextBold: main.getSessionController().getTopScene() == sessionId;
            property variant textElide: Text.ElideRight;

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    main.getSessionController().activateSession(sessionId);
                }
            }
            Rectangle {
                Image {
                    source: "img/cross.png"
                    height: parent.height * 0.8
                    anchors{ margins: parent.height*0.1; fill:parent}
                }
                color: leftPanel.color
                height: parent.height
                width: height
                opacity: .5
                anchors{ margins: parent.height*0.1; right: parent.right; top: parent.top}
                //BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        sessionCloseTimer.sessionId = sessionId;
                        sessionCloseTimer.start();
                        // this code cause application crash...
                        //main.getSessionController().closeSession(sessionId);
                    }
                }
            }
        }
    }

    Rectangle{
        id: separatorLine
        anchors{top: sessionsView.bottom; right: parent.right;}
        width: sessionsView.width
        anchors.margins: 4
        height: 1
        color: "transparent"

        Image {
            id: lineImage
            source: "img/line-horizontal.svg"
            anchors.fill: parent
        }
    }

    ListView {
        id: notificationsView
        model: leftPanel._notifications;
        anchors{margins: 6; topMargin: 20; right: parent.right; bottom: parent.bottom}
        width: sessionsView.width
        height: Math.max(leftPanel.height * .2, 100)
        clip: true

        Timer {
            id: acceptTimer
            property variant notificationId:0
            interval: 10; running: false; repeat: false
            onTriggered: {
                /*  this method destroys this ListViewLine (id notificationLine).
                    invoking this code directly from Button's method onClicked
                    causes application SEGFAULT! This is workaround, we invoke
                    it from timer...
                 */
                leftPanel._notifications.acceptNotification(notificationId);
            }
        }
        Timer {
            id: closeTimer
            property variant notificationId:0
            interval: 10; running: false; repeat: false
            onTriggered: {
                leftPanel._notifications.declineNotification(notificationId);
            }
        }


        delegate: AbstractListViewLine{
            id: notificationLine
            width: notificationsView.width
            height: titleText.height + contactText.height + acceptButton.height + acceptButton.anchors.topMargin +
                    (wrapper.anchors.topMargin + wrapper.anchors.bottomMargin ) + 2

            property variant notificationId : id
            property variant sessionId: sessionId
            property variant name : sessionName
            property variant notificationType : type
            property variant notificationTitle: title
            property variant notificationIcon: icon
            property variant notificationMessage: message

            Rectangle{
                id: wrapper
                anchors.topMargin: topMargin
                anchors.bottomMargin: bottomMargin
                anchors.leftMargin: leftMargin
                anchors.rightMargin: rightMargin
                anchors.fill: parent
                color: "transparent"

                Image {
                    id: notificationIcon
                    source: notificationLine.notificationIcon !== "" ?
                                "file:"+ notificationLine.notificationIcon : "qrc:/declarative/img/default_avatar.jpg"
                    width: height
                    anchors{top: titleText.top; bottom: contactText.bottom; left: parent.left}
                }

                Text {
                    id: titleText
                    text: notificationLine.notificationTitle
                    anchors{left: notificationIcon.right; right: parent.right; top: parent.top}
                    anchors.leftMargin: 8
                    wrapMode: Text.NoWrap
                    color: "grey"
                    clip: true
                }

                Text {
                    id: contactText
                    text: notificationLine.name+ (notificationLine.notificationMessage === ""?"":": "+notificationLine.notificationMessage)
                    anchors{left: titleText.left; right: parent.right; top: titleText.bottom}
                    wrapMode: Text.NoWrap
                    color: "white"
                    clip: true
                }
                Button{
                    id: acceptButton
                    anchors{ right: parent.right; top: contactText.bottom; topMargin: 4}
                    text: notificationLine.notificationType == Makneto.NotificationItem.CallErrorType? "Hide": "Accept"
                    onClicked: {
                        acceptTimer.notificationId = notificationLine.notificationId;
                        acceptTimer.start();
                    }
                }
                Button{
                    id: closeButton
                    opacity: notificationLine.notificationType == Makneto.NotificationItem.CallErrorType? 0: 1
                    anchors{ right: acceptButton.left; top: acceptButton.top}
                    text: "Close"
                    onClicked: {
                        closeTimer.notificationId = notificationLine.notificationId;
                        closeTimer.start();
                    }
                }
            }
        }
    }

}
