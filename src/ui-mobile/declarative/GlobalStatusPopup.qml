/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
import "./components/styles/default" as DefaultStyles
import "components"
import org.makneto 0.1 as Makneto

AbstractPopup {

    property variant statusesModel: ListModel{}
    property variant globalStatusComponent: { }

    SystemPalette { id: syspal }

    function setGlobalStatusComponent( c ){
        globalStatusComponent = c;
        /*
        statusChoiceList.currentIndex = globalStatusComponent.presenceToStatusIndex(
                    globalStatusComponent._presenceManager.getGlobalPresenceType() );
                    */
        statusMessage.text = globalStatusComponent._presenceManager.getGlobalPresenceStatusMessage();
    }

    Rectangle{
        id: background
        anchors.fill: wrapper
        opacity: wrapper.opacity
        color: main.useSyspal? syspal.mid :"#323235"
        radius: (width*0.03)

        Rectangle{
            id: border
            anchors.fill: parent
            radius: parent.radius
            border{ color: "white"; width: 1 }
            color: "transparent"
            opacity: 0.25
            //opacity: 1.0
        }

        Text{
            id: accountsLabel
            anchors{ top: parent.top; right: parent.right; left: parent.left }
            anchors{ topMargin: 12; leftMargin: 6; bottomMargin: 10; rightMargin: 6}
            color: main.useSyspal? syspal.windowText :"white"
            text: "Your IM accounts:"
        }

        ListView {
            id: accountView
            model: globalStatusComponent._model;
            anchors.top: accountsLabel.bottom
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: 5
            width: parent.width *.5
            clip: true
            anchors.topMargin: background.radius / 2
            anchors.bottomMargin: anchors.topMargin

            delegate: ListViewLine{
                width: accountView.width

                property variant accountStatus : status;
                property variant accountId : id

                // FIXME: find way howto properly use standard system icons
                //source: "image://desktoptheme/" + icon
                //source: "file:/usr/share/icons/hicolor/32x32/actions/" + icon + ".png"
                property string iconSource : "./img/" + icon + ".png"
                property variant titleText: displayName
                property variant statusText: statusMessage
                property variant iconOpacity: (accountStatus+"") == "offline"? 0.2:1.0;
                property int count: globalStatusComponent._model.accountCount
            }
        }

        Rectangle{
            id: separatorLine
            anchors{top: parent.top; left: accountView.right; bottom: parent.bottom}
            anchors.margins: 4
            width: 1
            color: "transparent"

            Image {
                id: topLineImage
                source: "img/line-vertical.svg"
                anchors.fill: parent
            }
        }

        Text{
            id: statusLabel
            anchors{ top: parent.top; right: parent.right; left: separatorLine.right }
            anchors{ topMargin: 12; leftMargin: 6; bottomMargin: 10; rightMargin: 6}
            color: main.useSyspal? syspal.windowText :"white"
            text: "Your global status:"
        }

        ChoiceList{
            id: statusChoiceList
            anchors{ top: statusLabel.bottom; right: parent.right; left: separatorLine.right }
            anchors{ topMargin: 6; leftMargin: 6; bottomMargin: 0; rightMargin: 6}

            behavior: "MeeGo"
            model: statusesModel

            label: Rectangle{
                color: "transparent"
                property variant defined: statusesModel.get(statusChoiceList.currentIndex) !== undefined
                Image {
                    id: currentStatusIcon
                    source: (!defined)? "": statusesModel.get(statusChoiceList.currentIndex).icon
                    height: parent.height
                    width: height
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text {
                    anchors.left: currentStatusIcon.right
                    anchors.leftMargin: 4
                    anchors.verticalCenter: parent.verticalCenter
                    color: syspal.text
                    text: (!defined)? "???":statusesModel.get(statusChoiceList.currentIndex).text
                }
            }
            listItem: Item {
                width: styledItem.width
                height: Math.max(itemText.height, 28)
                Image {
                    id: statusIcon
                    source: statusesModel.get(index).icon
                    height: Math.max(parent.height - 10, 12)
                    width: height
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                }
                Text {
                    id: itemText
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: statusIcon.right
                    anchors.leftMargin: 6

                    font.bold: index == statusChoiceList.currentIndex
                    color: styledItem.textColor
                    anchors.margins: 10
                    text: statusesModel ? statusesModel.get(index).text : ""  // list properties can't be automatically be added to the scope, so use get()
                }
            }
        }

        /*
        Text{
            id: statusTextLabel
            anchors{ top: statusChoiceList.bottom; right: parent.right; left: separatorLine.right }
            color: main.useSyspal? syspal.windowText :"white"
            anchors{ topMargin: 6; leftMargin: 6; bottomMargin: 0; rightMargin: 6}
            text: "Status:"
        }

        TextField{
            id: statusText
            anchors{ top: statusTextLabel.bottom; right: parent.right; left: separatorLine.right }
            anchors{ topMargin: 0; leftMargin: 6; bottomMargin: 6; rightMargin: 6}
        }
        */

        Text{
            id: statusMessageLabel
            anchors{ top: statusChoiceList.bottom; right: parent.right; left: separatorLine.right }
            color: main.useSyspal? syspal.windowText :"white"
            anchors{ topMargin: 6; leftMargin: 6; bottomMargin: 0; rightMargin: 6}
            text: "Status Message:"
        }

        TextArea{
            id: statusMessage
            anchors{ top: statusMessageLabel.bottom; right: parent.right; bottom:acceptButton.top; left: separatorLine.right }
            anchors{ topMargin: 0; leftMargin: 6; bottomMargin: 6; rightMargin: 6}
        }

        Button{
            id: acceptButton
            anchors{ right: parent.right; bottom: parent.bottom}
            anchors.rightMargin: 16
            anchors.bottomMargin: 2
            text: "Change Status"

            onClicked: {
                globalStatusComponent.setGlobalStatus(statusChoiceList.currentIndex, statusMessage.text, statusMessage.text);
                hidePopup();
            }
        }

    }
}
