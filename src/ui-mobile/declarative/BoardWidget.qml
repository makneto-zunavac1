/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.1 // version 1.1 include PinchArea
import Qt 4.7
import QtWebKit 1.0
import MyTools 1.0 // for WheelArea

Rectangle {
    id: board
    width: 100
    height: 62
    color: "black"

    signal sendWbMessage(string sessionId, string xmlContent);
    signal messageSent(string content);

    property int canvasWidth : 640;
    property int canvasHeight: 480;
    //property string uid : "demo@makneto.org"

    function log(msg){
        console.log("II [BoardWidget.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [BoardWidget.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [BoardWidget.qml]: "+msg);
    }

    //signal foregroundColorChanged(variant color)
    signal whiteboardMessageReceived(string data, string contact)
    property bool whiteboardInitialiyed: false
    property variant queue: undefined
    property int animationLength : 200;

    function onWhiteboardMessageReceived(data, contact){
        if (whiteboardInitialiyed){
            whiteboardMessageReceived.disconnect(onWhiteboardMessageReceived);
            return;
        }

        if (queue===undefined)
            queue = [];

        var obj = {};
        obj.contact = contact;
        obj.data = data;

        var tmp = queue;
        tmp.push(obj);
        queue = tmp;
        //log("whiteboard is not initialized... "+JSON.stringify(queue.length));
    }

    function hideWebView(b){
        wbView.opacity = b? 0:1;
        log("HACK: hide wbView? "+b);
    }

    state: "normal"
    states: [
        State {
            name: "normal"
            /*
            PropertyChanges {
                target: stateIcon
                source: "img/fullscreen.png"
            }
            */
            AnchorChanges { target: board; anchors.top: board.parent.top; anchors.left: board.parent.left; }
        },
        State {
            name: "fullscreen"
            /*
            PropertyChanges {
                target: stateIcon
                source: "img/minimize.png"
            }
            */
            AnchorChanges { target: board; anchors.right: board.parent.right; anchors.bottom: board.parent.bottom}
        }
    ]


    Behavior on width {
        NumberAnimation{
            id: onWidthAnimation
            duration: animationLength
        }
    }
    Behavior on height {
        NumberAnimation{
            id: onHeightAnimation
            duration: animationLength
        }
    }
    Behavior on opacity {
        NumberAnimation{duration: animationLength}
    }

    transitions: Transition {
       // smoothly reanchor myRect and move into new position
       AnchorAnimation { duration: animationLength }
    }

    Component.onCompleted: {
        whiteboardMessageReceived.connect(onWhiteboardMessageReceived);
        //toolbar.foregroundColorChanged.connect(board.foregroundColorChanged);
    }

    Rectangle{
        id: wrapper
        color: parent.color
        anchors{top:parent.top; right: toolbar.left; left: parent.left; bottom: parent.bottom}

        property int previousWidth : canvasWidth+30
        property int previousHeith : canvasHeight+30

        /**
          * Zooming process:

                PinchArea -> resizeAfterPinch -> onSnapshotTaken -> SVG resizing -> canvasResized

                For faster and smooth zooming, we use PinchArea with combination WebView scale property.
                After touch gesture release, we call resizeAfterPinch(). This take snaphost (graber component)
                of current WebView content (scaled) and overlap WebView.
                When snapshot is taken and shown (onSnapshotTaken), we invoke SVG scaling inside WebView.
                This operation is long (up to seconds) and not smooth. After SVG resizing is caled canvasResized()
                method. We currently change WebView size, return scale to 1 and remove old screenshot...
          */
        WebView {
            id: wbView
            width: canvasWidth * 3
            height: canvasHeight * 3
            x: -canvasWidth
            y: -canvasHeight
            settings.pluginsEnabled: false

            property bool whiteboardInitialized : false;
            property real previousX: 0
            property real previousY: 0

            pressGrabTime:0

            javaScriptWindowObjects: [QtObject {
                    WebView.windowObjectName: "connector"
                    function getUID(){ return board.parent.sessionId; }
                    function init() {
                        wbView.whiteboardInitialized = true;
                        log("html part of whiteboard is initialized! lets make big stufs...");
                        // html part is loaded, init MaknetoWhiteboard...
                        wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.init('+board.canvasWidth+', '+board.canvasHeight+')' );

                        //var zoom = Math.min( wbView.width / (wbView.contentsSize.width *0.3), wbView.height / (wbView.contentsSize.height *0.3) );
                        //wbView.evaluateJavaScript ( '$("#workArea").scrollLeft(640)');
                        //wbView.calculateZoom();
                        wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.setZoom('+ wbView.width / (canvasWidth*3) +')' );

                        whiteboardMessageReceived.connect(onWhiteboardMessageReceived);
                        toolbar.foregroundColorChanged.connect(onForegroundColorChanged);
                        messageSent.connect(onMessageSent);
                        whiteboardInitialiyed = true;
                        while (queue !== undefined && queue.length !== 0){
                            var tmp = queue;
                            var obj = tmp.shift(); // take first element (from array begin)
                            queue = tmp;
                            //log("queue message: "+obj.data);
                            onWhiteboardMessageReceived(obj.data, obj.contact);
                        }
                    }
                    function onForegroundColorChanged(c){
                        //log("onForegroundColorChanged "+c);
                        wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.setForegroundColor("'+c+'")' );
                    }
                    function onWhiteboardMessageReceived(data, contact){
                        wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.processXmlCommand('+JSON.stringify(data)+')' );
                        //log("on whiteboard msg received from "+contact+" "+JSON.stringify(data));
                    }
                    function onMessageSent(content){
                        wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.messageSent('+JSON.stringify(content)+')' );
                    }
                    function sendMessage(xmlMessage){
                        //log(xmlMessage);
                        sendWbMessage(board.parent.sessionId, xmlMessage);
                        return true;
                    }
                    function enableRendering(b){
                        wbView.renderingEnabled = b;
                    }
                    function canvasResized(){
                        var originalWidth = wbView.width;
                        var originalHeight = wbView.height;
                        wbView.width = wbView.width * wbView.scale;
                        wbView.height= wbView.height* wbView.scale;
                        wbView.scale = 1;
                        // move to new center
                        wbView.x = wbView.x + (originalWidth - wbView.width)*.5;
                        wbView.y = wbView.y + (originalHeight - wbView.height)*.5;
                        wbView.previousX = wbView.x;
                        wbView.previousY = wbView.y;

                        log("resizeAfterPinch "+wbView.scale+" "+Math.floor(originalWidth)+"x"+Math.floor(originalHeight)+
                            " => "+Math.floor(wbView.width)+"x"+Math.floor(wbView.height)+" ..."+Math.floor(wbView.x) +""+Math.floor(wbView.y)+"");

                        pinchy.pinch.minimumX = Math.max( (wbView.width  / (-2/3)) , -(wbView.width - wrapper.width));
                        pinchy.pinch.minimumY = Math.max( (wbView.height / (-2/3)) , -(wbView.height - wrapper.height));
                        pinchy.pinch.maximumX = 0;
                        pinchy.pinch.maximumY = 0;

                        var minWscale = wrapper.width / wbView.width;
                        var minHscale = wrapper.height / wbView.height;
                        pinchy.pinch.minimumScale = Math.max(minHscale, minWscale);
                        var maxWscale = (wrapper.width*15) / wbView.width;
                        var maxHscale = (wrapper.height*15) / wbView.height;
                        pinchy.pinch.maximumScale = Math.min(maxHscale, maxWscale);

                        graber.state = "hide";
                        //graberTimer.start();
                    }
                },
                QtObject {
                    WebView.windowObjectName: "console"
                    function log(msg)   { console.log("II [Whiteboard]: " + msg); }
                    function debug(msg) { console.log("DD [Whiteboard]: " + msg); }
                    function warn(msg)  { console.log("WW [Whiteboard]: " + msg); }
                    function error(msg)  { console.log("EE [Whiteboard]: " + msg); }
                }
            ]

            function resizeAfterPinch(){

                // TODO: SHOW waiting cursor while resizing SVG image
                graber.takeSnapshot();
                graber.state = "visible";
            }

            onContentsSizeChanged: {

            }

            Component.onCompleted: {
                wbView.previousX = wbView.x +15
                wbView.previousY = wbView.y +15
            }

            //html: "<html><head><script>console.log(\"This is in WebKit!\"); window.connector.qmlCall();</script></head><body><h1>Qt WebKit!</h1></body></html>"
            url:"qrc:/whiteboard/main.html"

        }

        ViewGraber{
            id: graber
            opacity: 0
            anchors.fill: parent
            delegate: wbView

            states: [
                State {
                    name: "hide"
                    PropertyChanges { target: graber; opacity: 0 }
                    PropertyChanges { target: scalingOverlay; opacity: 0 }
                },
                State {
                    name: "visible"
                    PropertyChanges { target: graber; opacity: 1 }
                    PropertyChanges { target: scalingOverlay; opacity: 0.2 }
                }
            ]
            transitions: [
                Transition {
                    from: "hide"; to: "visible"
                    NumberAnimation { target: graber; properties: "opacity"; duration: 0 }
                    // when webkit scaling svg image, renderign thread is busy, so we cant draw this nice animation
                    //NumberAnimation { target: scalingOverlay; properties: "opacity"; duration: 500 }
                },
                Transition {
                    from: "visible"; to: "hide"
                    NumberAnimation { target: graber; properties: "opacity"; duration: 100 }
                    NumberAnimation { target: scalingOverlay; properties: "opacity"; duration: 5000 } // opacity is multipied by parent's opacity
                }
            ]
            onSnapshotTaken:{
                log("snapshot taken");
                wbView.evaluateJavaScript ( 'MaknetoWhiteboard.instance.setZoom('+ wbView.width * wbView.scale / (canvasWidth*3) +')' );
            }

            Rectangle{
                id: scalingOverlay
                opacity: 0
                color: "black"
                anchors.fill: parent
            }
            Text {
                id: scalingText
                anchors{horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter}
                text: "Scaling..."
            }
            // discard all mouse operations while scaling
            PinchArea{ anchors.fill: parent; enabled: true}
            MouseArea{ anchors.fill: parent }
            WheelArea{ anchors.fill: parent }
        }


        //Rectangle { id: rect; color: "transparent"; border.color: "yellow"; x: 150; y: 150; height: 250; width: 250 }

        /**
          * I don't know better solution detect when resize animations stops...
          */
        Timer{
            id: animationNotifier
            interval: 300; running: false; repeat: false
            onTriggered: {
                wrapper.onDimensionChanged(false);
            }
        }

        // while animation we using QML scale for webView
        function onDimensionChanged(fast){
            if (wrapper.width < 10 || wrapper.height < 10)
                return;

            if (fast){
                var minWscale = wrapper.width / wbView.width;
                var minHscale = wrapper.height / wbView.height;
                pinchy.pinch.minimumScale = Math.max(minHscale, minWscale);

                var wSc = wrapper.width / previousWidth;
                var hSc = wrapper.height / previousHeith;
                wbView.scale =  Math.max(pinchy.pinch.minimumScale, Math.min(wSc, hSc)); // FIXME: it is good?
                wbView.x = wbView.previousX + (wrapper.width - previousWidth) / 2
                wbView.y = wbView.previousY + (wrapper.height - previousHeith) / 2

                /*
                log("fast scale to "+wbView.scale+"");
                log("       "+Math.floor( wbView.previousX)+" + ("+Math.floor(wrapper.width)+" - "+Math.floor(previousWidth)+") / 2) = "+Math.floor( wbView.x)+"");
                log("       "+Math.floor( wbView.previousY)+" + ("+Math.floor(wrapper.height)+" - "+Math.floor(previousHeith)+") / 2) = "+Math.floor( wbView.y)+"");
                */

                animationNotifier.stop();
                animationNotifier.start();
            }else{
                wbView.resizeAfterPinch();
                previousWidth = wrapper.width;
                previousHeith = wrapper.height
            }
        }

        onWidthChanged: {
            onDimensionChanged(true);
        }
        onHeightChanged: {
            onDimensionChanged(true);
        }

        PinchArea {
            id: pinchy
            enabled: graber.state == "hide"
            pinch.target: wbView
            anchors.fill: parent
            pinch.dragAxis: Pinch.XandYAxis
            pinch.minimumScale: 1
            pinch.maximumScale: 1
            pinch.minimumX: -canvasWidth*2
            pinch.maximumX: 0
            pinch.minimumY: -canvasHeight*2
            pinch.maximumY: 0
            //pinch.minimumRotation: -150
            //pinch.maximumRotation: 150

            onPinchStarted: {
                log("onPinchStarted");
                pressDelay.stop();
                wbView.evaluateJavaScript("MaknetoWhiteboard.instance.whiteboard.mouseUp2()");
            }
            onPinchUpdated: {                
            }
            onPinchFinished: {
                wbView.resizeAfterPinch();                
            }
        }

        /**
          * delayed mouse down event is workaround for late detected gestures.
          */
        Timer{
            id: pressDelay
            property int mouseX;
            property int mouseY;
            interval: 200; running: false; repeat: false
            onTriggered: {
                log("delayed mouseDown");
                wbView.evaluateJavaScript("MaknetoWhiteboard.instance.whiteboard.mouseDown2("+(mouseX-wbView.x)+", "+(mouseY-wbView.y)+",0)");
            }
        }
        MouseArea {
            id: mouseArea
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.MiddleButton | Qt.RightButton

            property int startX;
            property int startY;
            property int startWbX;
            property int startWbY;
            property variant pressed;

            onPressed:{
                //log("mouseArea: onPressed");
                //mouse.accepted = false;
                startX = mouse.x;
                startY = mouse.y;
                startWbX = wbView.x;
                startWbY = wbView.y;
                pressed = mouse.button;

                if (mouse.button == Qt.LeftButton){
                    log("left button");
                    pressDelay.mouseX = mouse.x;
                    pressDelay.mouseY = mouse.y;
                    pressDelay.start();
                    //wbView.evaluateJavaScript("MaknetoWhiteboard.instance.whiteboard.mouseDown2("+(mouseX-wbView.x)+", "+(mouseY-wbView.y)+",0)");
                    return;
                }
                pressDelay.stop();
            }
            onDoubleClicked:{
                //log("mouseArea: onDoubleClicked");
            }
            onPositionChanged:{
                //log("mouseArea: onPositionChanged");
            }
            onPressAndHold:{
                //log("mouseArea: onPressAndHold");                
            }
            onReleased :{
                //log("mouseArea: onReleased");
                pressDelay.stop();
                wbView.evaluateJavaScript("MaknetoWhiteboard.instance.whiteboard.mouseUp2()");
            }
            onMousePositionChanged: {
                //pressDelay.stop();
                //log("mouseArea: onMousePositionChanged");
                if (pressed == Qt.LeftButton){
                    //log("      ... paint");
                    wbView.evaluateJavaScript("MaknetoWhiteboard.instance.whiteboard.mouseMove2("+(mouse.x-wbView.x)+", "+(mouse.y-wbView.y)+")");
                    return;
                }

                if (pressed == Qt.RightButton || pressed == Qt.MiddleButton){
                    pressDelay.stop();

                    wbView.x = (mouse.x - startX) + startWbX;
                    wbView.y = (mouse.y - startY) + startWbY;
                    if (wbView.x < pinchy.pinch.minimumX)
                        wbView.x = pinchy.pinch.minimumX;
                    if (wbView.y < pinchy.pinch.minimumY)
                        wbView.y = pinchy.pinch.minimumY;
                    if (wbView.x > pinchy.pinch.maximumX)
                        wbView.x = pinchy.pinch.maximumX;
                    if (wbView.y > pinchy.pinch.maximumY)
                        wbView.y = pinchy.pinch.maximumY;
                    //log("       ... move "+wbView.x+" "+pinchy.pinch.minimumX);
                    return;
                }
            }
        }
        // we use wheel for zooming on desktop...
        WheelArea {
            anchors.fill: parent
            onVerticalWheel: {
                //console.log("Vertical Wheel: " + delta)
                var scaleRange = pinchy.pinch.maximumScale - pinchy.pinch.minimumScale;

                wbView.scale += scaleRange * (delta / 5000);
                if (wbView.scale > pinchy.pinch.maximumScale)
                    wbView.scale = pinchy.pinch.maximumScale;
                if (wbView.scale < pinchy.pinch.minimumScale)
                    wbView.scale = pinchy.pinch.minimumScale;
                // FIXME: adjust position when we zoom out

                animationNotifier.stop();
                animationNotifier.start();
            }
            onHorizontalWheel: {
                //console.log("Horizontal Wheel: " + delta)
            }
        }
    }
    BoardToolbar{
        id: toolbar
        //color: parent.color
        anchors{bottom: parent.bottom; right: parent.right; top: parent.top}
        BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
        width: 60
    }

    onWidthChanged:{
        wbView.renderingEnabled = false;
        wbView.renderingEnabled = true;
    }
    onHeightChanged:{
        wbView.renderingEnabled = false;
        wbView.renderingEnabled = true;
    }

}
