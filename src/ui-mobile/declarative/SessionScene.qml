/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import QtQuick 1.0
import org.makneto 0.1 as Makneto

Scene {
    id: sessionScene
    color: "black"

    property bool fullscreen: false;
    property int sessionWidth : sessionScene.width;
    //property variant sessionModel: {};
    property variant sessionId

    signal whiteboardMessageReceived(string data, string contact)
    signal textMessageReceived(string text, string contact)
    signal chatStateChanged(variant chatState, string contact);
    signal messageSent(string content);

    function log(msg){
        console.log("II [SessionScene.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [SessionScene.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [SessionScene.qml]: "+msg);
    }
    function setSessionsModel(model){
        chatWidget.sendMessage.connect(model.sendTextMessage);
        chatWidget.changeChatState.connect(model.changeChatState);
        boardComponent.sendWbMessage.connect(model.sendWhiteboardMessage);
        sessionToolbar.requestCall.connect(model.startCall);
        sessionToolbar.requestHangup.connect(model.hangup);

        // initial setup sessin type
        sessionTypeChanged(model.getSessionType(sessionId));
    }

    function sessionTypeChanged(type){
        log("session type changed "+type+" (audio: "+Makneto.Session.SessionTypeAudio+", video: "+Makneto.Session.SessionTypeVideo+")")
        setAudioCall( (type & Makneto.Session.SessionTypeAudio) !==0 );
        setVideoCall( (type & Makneto.Session.SessionTypeVideo) !==0 );
    }

    BoardWidget{
        id: boardComponent
        /*
        height: parent.height * 0.6
        width: parent.width * 0.6
        */
    }

    VideoWidget{
        id: videoComponent
        height: boardComponent.height
        anchors{ top: parent.top; right: parent.right; left: board.right }
    }

    ChatWidget{
        id: chatWidget
        anchors{top: boardComponent.bottom; right: parent.right; bottom: parent.bottom; left: parent.left}
    }

    SessionControlPanel{
        id: sessionToolbar
        z: 200
    }

    Component.onCompleted:{
        updateLayout();
        whiteboardMessageReceived.connect(boardComponent.whiteboardMessageReceived);
        whiteboardMessageReceived.connect( function(data, contact){
                                              if (!sessionScene.board){
                                                  setBoard(true);
                                              }
                                          });
        messageSent.connect(boardComponent.messageSent);
        textMessageReceived.connect(chatWidget.textMessageReceived);
        chatStateChanged.connect(chatWidget.chatStateChanged);
        //textMessageReceived("text", "contact");
    }

    onWidthChanged:{
        updateLayout();
    }
    onHeightChanged:{
        updateLayout();
    }
    onStateChanged: {
        // hack for nonfunctional clip property for WebView
        boardComponent.hideWebView( (state !== "center") );
    }

    function updateLayout(){
        // states
        if ((!videoCall) && videoComponent.state != "normal")
            videoComponent.state = "normal";
        if ((!board) && boardComponent.state != "normal")
            boardComponent.state = "normal";

        if (videoComponent.state == "fullscreen")
            boardComponent.state = "normal";

        if (!fullscreen)
            sessionWidth = sessionScene.width;

        // compute dimensions
        var boardHeight = (board || videoCall)? sessionScene.height * 0.6: 0;
        var videoHeight = boardHeight;

        var boardWidth = board? (videoCall? sessionWidth * 0.6 : sessionWidth): 0;
        var videoWidth = (sessionWidth - boardWidth);

        // z layer
        boardComponent.z = (!board)? -1 : (boardComponent.state == "fullscreen"? 100: 0);
        videoComponent.z = (!videoCall)? -1 : (videoComponent.state == "fullscreen"? 100: 0);

        // opacity
        boardComponent.opacity = board? 1:0;
        videoComponent.opacity = videoCall? 1:0;

        // fullscreen?
        fullscreen = (videoComponent.state == "fullscreen" || boardComponent.state == "fullscreen");
        //main.getStageController().fullScreen( fullScreen );
        if (fullscreen){
            main.getLeftPanel().state = "hided";
        }

        // debug
        /*
        log("update layout {board:"+board+", videoCall:"+videoCall+"}");
        log("board: "+boardWidth+"x"+boardHeight+" video: "+videoWidth+"x"+videoHeight+" / "+
                      boardComponent.width+"x"+boardComponent.height+"  "+videoComponent.width+"x"+videoComponent.height);
        */

        // set dimensions
        if (!fullscreen){
            boardComponent.width = boardWidth;
            boardComponent.height = boardHeight;
            //boardComponent.setDimensions(boardWidth, boardHeight);
            videoComponent.width = videoWidth;
            videoComponent.height = videoHeight;
        }
    }

    property bool videoCall : false;
    function setVideoCall(b){
        if (b != sessionToolbar.videoCall){
            sessionScene.videoCall = b;
            sessionToolbar.state = sessionScene.videoCall || sessionScene.audioCall ? "call": "chat";
            updateLayout();
        }
    }

    property bool audioCall : false;
    function setAudioCall(b){
        if (b != sessionScene.audioCall){
            sessionScene.audioCall = b;
            sessionToolbar.state = sessionScene.audioCall || sessionScene.audioCall ? "call": "chat";
            updateLayout();
        }
    }

    property bool board : false
    function setBoard(b){
        if (b != sessionScene.board){
            sessionScene.board = b;
            updateLayout();
        }
    }

    function closeSession(){
        if (sessionScene.videoCall || sessionScene.audioCall)
            sessionToolbar.requestHangup(sessionScene.sessionId);

        main.getSessionController().closeSession(sessionScene.sessionId);
    }
    function fullScreenVideo(){
        if (videoComponent.state == "fullscreen")
            return;
        videoComponent.state = "fullscreen";
        updateLayout();
    }
    function minimizeVideo(){
        if (videoComponent.state == "normal")
            return;
        videoComponent.state = "normal";
        updateLayout();
    }

}
