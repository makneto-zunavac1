/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

import QtQuick 1.0
import Qt 4.7
import Gst 1.0
import "components/styles/default"

Rectangle{
    id: main
    SystemPalette{ id: syspal }
    color: main.useSyspal? syspal.window : "#161616"
    width: 800
    height: 600

    property bool useSyspal: true
    property int toolbarHeight: fakeTextInput.height*3

    TextInput{
        id: fakeTextInput
        opacity: 1
    }

    function log(msg){
        console.log("II [mainview.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [mainview.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [mainview.qml]: "+msg);
    }

    signal contactsModelChanged(variant model)
    signal accountsModelChanged(variant model)
    signal sessionsModelChanged(variant model)
    signal presenceManagerChanged(variant manager);
    signal notificationsModelChanged(variant model)
    signal videoSurfaceReady(variant type, variant sessionId, variant surface);
    signal videoSurfaceRemoved(variant type, variant sessionId);

    function onModelsReady(){
        log("onModelsReady()");
        try{
            contactsModelChanged(contactsModel);
            accountsModelChanged(accountsModel);
            sessionsModelChanged(sessionsModel);
            notificationsModelChanged(notificationsModel);
            presenceManagerChanged(presenceManager);
        }catch(e){
            error(e);
        }
    } // stot

    function formatShortDateTime(dateobj){
        // FIXME: add support for locale, or configurable format
        var day  = dateobj.getDate();
        var mnth = dateobj.getMonth() + 1;
        var mins = dateobj.getMinutes(); var strMins = (mins > 9)? String(mins) : "0" + String(mins);
        var hrs  = dateobj.getHours(); var strHrs = (hrs > 9)? String(hrs) : "0" + String(hrs);

        return day + "." + mnth + ". " + strHrs + ":" + strMins ;
    }
    function formatDateTime(dateobj){
        // FIXME: add support for locale, or configurable format
        var secs = dateobj.getSeconds(); var strSecs = (secs > 9)? String(secs) : "0" + String(secs);
        var mins = dateobj.getMinutes(); var strMins = (mins > 9)? String(mins) : "0" + String(mins);
        var hrs  = dateobj.getHours(); var strHrs = (hrs > 9)? String(hrs) : "0" + String(hrs);
        var day  = dateobj.getDate(); var strDays = (day > 9)? String(day) : "0" + String(day);
        var mnth = dateobj.getMonth() + 1; var strMnth = (mnth > 9)? String(mnth) : "0" + String(mnth);
        var yr   = dateobj.getFullYear(); var strYr = String(yr);

        return strDays + "." + strMnth + "." + strYr + " " + strHrs + ":" + strMins + ":" + strSecs;
    }
    function formatTime(dateobj){
        // FIXME: add support for locale, or configurable format
        var secs = dateobj.getSeconds(); var strSecs = (secs > 9)? String(secs) : "0" + String(secs);
        var mins = dateobj.getMinutes(); var strMins = (mins > 9)? String(mins) : "0" + String(mins);
        var hrs  = dateobj.getHours(); var strHrs = (hrs > 9)? String(hrs) : "0" + String(hrs);

        return strHrs + ":" + strMins + ":" + strSecs;
    }

    function getSessionController(){
        return sessionController;
    }
    function getLeftPanel(){
        return leftPanel;
    }

    Component.onCompleted:{
        makneto.modelsReady.connect(onModelsReady);
        makneto.videoSurfaceReady.connect(videoSurfaceReady);
        makneto.videoSurfaceRemoved.connect(videoSurfaceRemoved);
        log("ready, fun starts...");
    }

    SessionController{
        id: sessionController
        color: parent.color
        anchors{top: parent.top; bottom: parent.bottom; right: parent.right; left: leftPanel.right}
    }

    LeftPanel{
        id: leftPanel
        anchors{top: parent.top; bottom: parent.bottom; left: parent.left}

        property variant expandedWidth : Math.max(200, parent.width / 3);

    }
}
