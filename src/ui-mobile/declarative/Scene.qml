/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.0

Rectangle {
    id: scene

    function log(msg){
        console.log("II [Scene.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [Scene.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [Scene.qml]: "+msg);
    }

    property bool animate: false
    property variant animationLength: 500
    property string name;
    property string icon;

    function destroyLater(){
        log("start destroyTimer")
        destroyTimer.start();
    }

    Timer {
        id: destroyTimer
        interval: animationLength; running: false; repeat: false
        onTriggered: {
            log("destroy scene");
            scene.destroy();
        }
    }

    width: parent.width;
    height: parent.height;
    x: 0; y:0;

    state: "center"
    states: [
        State {
            name: "left"
            PropertyChanges { target: scene; x: parent.width*-1; }
        },
        State {
            name: "center"
            PropertyChanges { target: scene; x: 0; }
        },
        State {
            name: "right"
            PropertyChanges { target: scene; x: parent.width;}
        }
    ]
    transitions: Transition {
        NumberAnimation { properties: "x"; duration: scene.animate? scene.animationLength: 0; easing.type: Easing.InOutQuad }
    }

    MouseArea{ // for overwrite MouseAreas from bottom scenes
        anchors.fill: parent
    }
}
