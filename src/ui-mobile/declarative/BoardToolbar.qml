import QtQuick 1.0
import Qt 4.7
import "ColorPicker"

Rectangle {
    id: toolbar
    width: 100
    height: 62
    color: main.useSyspal? syspal.window :parent.color

    signal foregroundColorChanged(variant color)
    property variant foregroundColor: "#ff0000"
    property variant backgroundColor: "#ffffff"

    property variant pickerComponent : Qt.createComponent("ColorPicker/ColorPickerPopup.qml");
    property variant popup: undefined;

    function log(msg){
        console.log("II [BoardToolbar.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [BoardToolbar.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [BoardToolbar.qml]: "+msg);
    }

    Component.onCompleted:{
        foregroundColorChanged.connect(function(color){toolbar.foregroundColor = color;});
    }

    SystemPalette{ id: syspal }

    Rectangle{
        id: foregroundColorSelector
        anchors{margins: 4; left: parent.left; right: parent.right; top: parent.top; }
        anchors.topMargin: main.toolbarHeight + 4

        height: width

        Rectangle{
            anchors.fill: parent
            color: toolbar.foregroundColor
            radius: border.radius

            Rectangle{
                id: border
                anchors.fill: parent
                radius: 3
                border{ color: "white"; width: 1 }
                color: "transparent"
                opacity: 0.25
                //opacity: 1.0
            }
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                //log("clicked on statusIndicator ");

                if(pickerComponent.status != Component.Ready){
                    error("Component "+pickerComponent.url+" is not ready!");
                    error(pickerComponent.errorString());
                    return false;
                }

                if (popup !== undefined && popup !== null && popup.state != "hide"){
                    popup.hidePopup();
                }

                popup = pickerComponent.createObject(main);
                if(popup === null){
                    error("error creating popup");
                    error(pickerComponent.errorString());
                    return false;
                }
                var pos = foregroundColorSelector.mapToItem(main,0, 0);
                popup.popupFromX = pos.x;
                popup.popupFromY = pos.y;
                popup.color = foregroundColor;

                popup.colorChanged.connect(foregroundColorChanged)

                popup.state = "visible"
                //log("visible popup "+JSON.stringify(parent));
            }
        }

    }
}
