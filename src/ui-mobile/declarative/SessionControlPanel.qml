/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import Qt 4.7

TopPanel{
    id: sessionToolbar
    //BorderImage { source: "img/toolbar.sci"; anchors.fill: parent }
    //color: "transparent"
    //y: 10
    //width: height * 3

    //height: main.toolbarHeight

    //anchors.horizontalCenter: parent.horizontalCenter
    anchors{top: parent.top; left: parent.left; right: parent.right}
    state: "chat"
    //anchors.right: crossIcon.right

    signal requestCall(string sessionId, bool video);
    signal requestHangup(string sessionId);

    function log(msg){
        console.log("II [SessionControlPanel.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [SessionControlPanel.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [SessionControlPanel.qml]: "+msg);
    }

    function updateMicIcon(b){
        //micIcon.source = b ? "img/mic-on.svg": "img/mic-off.png";
    }

    Rectangle{
        width: height * 5
        anchors{top: parent.top; bottom: parent.bottom}
        anchors.horizontalCenter: parent.horizontalCenter
        color: "transparent"

        Image {
            id: boardIcon
            source: "img/table.svg"
            height: parent.height * 0.8
            width: height
            anchors{ margins: parent.height*0.1; left: parent.left; top: parent.top }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    sessionToolbar.parent.setBoard(!sessionToolbar.parent.board);
                }
            }
        }

        Image {
            id: callIcon
            source: "img/call-start.png"
            height: parent.height * 0.8
            width: height
            anchors{ margins: parent.height*0.1; left: boardIcon.right; top: parent.top;
                leftMargin:parent.height *.5; }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (sessionToolbar.state == "call"){
                        log("request hangup "+sessionToolbar.parent.sessionId);
                        requestHangup(sessionToolbar.parent.sessionId);
                    }else{
                        log("request audio call "+sessionToolbar.parent.sessionId);
                        requestCall(sessionToolbar.parent.sessionId, false);
                    }
                }
            }
        }

        Image {
            id: camIcon
            source: "img/cam.svg"
            height: parent.height * 0.8
            width: 0
            //visible: false
            anchors{ margins: 0; left: callIcon.right; top: parent.top; leftMargin:parent.height *.5}
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    log("request video call "+sessionToolbar.parent.sessionId);
                    requestCall(sessionToolbar.parent.sessionId, true);
                }
            }
        }


    }
    Image {
        id: crossIcon
        source: "img/cross.png"
        height: parent.height * 0.8
        width: height
        anchors{ margins: parent.height*0.1; right: parent.right; top: parent.top}

        MouseArea{
            anchors.fill: parent
            onClicked: {
                sessionToolbar.parent.closeSession();
            }
        }
    }

    states: [
        State {
            name: "chat"
            //PropertyChanges { target: sessionToolbar; width:  height*4}
            PropertyChanges { target: callIcon; source: "img/call-start.png" }
            PropertyChanges { target: camIcon; anchors.margins: parent.height*0.1; visible: true; width: height; }
        },
        State {
            name: "call"
            PropertyChanges { target: callIcon; source: "img/call-end.png";}
            //PropertyChanges { target: sessionToolbar; width:  height*2.5 }
            PropertyChanges { target: camIcon; anchors.margins:0; width: 0; } // visible: false;
        }
    ]

    transitions: [
        Transition {
            from: "chat"; to: "call"
            NumberAnimation { properties: "width,height,x,y,anchors.margins"; easing.type: Easing.InOutQuad; duration: 500 }
        },
        Transition {
            from: "call"; to: "chat"
            NumberAnimation { properties: "width,height,x,y,anchors.margins"; easing.type: Easing.InOutQuad; duration: 500 }
        }
    ]


}

