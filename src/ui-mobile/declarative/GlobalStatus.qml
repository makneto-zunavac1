/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
import org.makneto 0.1 as Makneto

Item {
    id: globalStatusComponent

    property variant _model: ListModel {}
    property variant _presenceManager : undefined
    property variant popup: undefined;
    property string defaultStatusMsg: "Click here for change your status..."
    property string globalStatus: "Offline";
    property variant statusPopupComponent : Qt.createComponent("GlobalStatusPopup.qml");

    function log(msg){
        console.log("II [GlobalStatus.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [GlobalStatus.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [GlobalStatus.qml]: "+msg);
    }

    function setGlobalStatus(statusIndex, status, statusMessage){
        /*
        try{
            var presence = statusIndexToPressence(statusIndex);
            log("set global presence to "+presence );
            for (var i=0; i< _model.accountCount; i++){
                var id = _model.data(i, Makneto.AccountsModel.IdRole);
                var account = _model.accountItemForId(id);
                account.setRequestedPresence(presence, status, statusMessage);
            }
        }catch(e){
            error(e);
        }
        */
        var presence = statusIndexToPressence(statusIndex);
        _presenceManager.requestGlobalPresence(presence, status, statusMessage);
        //log("request global presence...");
    }

    function onGlobalPresenceChanged(type, status, statusMessage){
        log("onGlobalPresenceChanged (1) "+type+" "+status+", "+statusMessage);
        var i = presenceToStatusIndex(type);
        globalStatusIcon.source =  statusesModel.get(i).icon;
        globalStatusMsgText.text = statusMessage === ""? defaultStatusMsg: statusMessage;
        presenceText.text = presenceToStr( type );
        log("onGlobalPresenceChanged "+type+" "+status+", "+statusMessage);
    }

    function onAccountsModelChanged(model){
        _model = model;
        // this is emited every change
        /*
        _model.accountCountChanged.connect(
                    function(){
                        updateGlobalStatus();
                    });
                    */
    }       
    function onPresenceManagerChaged(manager){
        _presenceManager = manager;
        _presenceManager.globalPresenceChanged.connect( onGlobalPresenceChanged);
    }

    function presenceToStr(i){
        switch(i){
        case Makneto.TelepathyTypes.ConnectionPresenceTypeAvailable: return "Available";
        case Makneto.TelepathyTypes.ConnectionPresenceTypeOffline: return "Offline";
        case Makneto.TelepathyTypes.ConnectionPresenceTypeAway: return "Away";
        case Makneto.TelepathyTypes.ConnectionPresenceTypeExtendedAway: return "ExtendedAway";
        case Makneto.TelepathyTypes.ConnectionPresenceTypeBusy: return "Busy";
        case Makneto.TelepathyTypes.ConnectionPresenceTypeHidden: return "Hidden";
        }
        return "Offline";
    }

    function statusIndexToPressence(i){
        switch(i){
        case 0: return Makneto.TelepathyTypes.ConnectionPresenceTypeAvailable;
        case 1: return Makneto.TelepathyTypes.ConnectionPresenceTypeOffline;
        case 2: return Makneto.TelepathyTypes.ConnectionPresenceTypeAway;
        case 3: return Makneto.TelepathyTypes.ConnectionPresenceTypeExtendedAway;
        case 4: return Makneto.TelepathyTypes.ConnectionPresenceTypeBusy;
        case 5: return Makneto.TelepathyTypes.ConnectionPresenceTypeHidden;
        }
        return Makneto.TelepathyTypes.ConnectionPresenceTypeOffline;
    }
    function presenceToStatusIndex(i){
        switch(i){
        case Makneto.TelepathyTypes.ConnectionPresenceTypeAvailable: return 0;
        case Makneto.TelepathyTypes.ConnectionPresenceTypeOffline: return 1;
        case Makneto.TelepathyTypes.ConnectionPresenceTypeAway: return 2;
        case Makneto.TelepathyTypes.ConnectionPresenceTypeExtendedAway: return 3;
        case Makneto.TelepathyTypes.ConnectionPresenceTypeBusy: return 4;
        case Makneto.TelepathyTypes.ConnectionPresenceTypeHidden: return 5;
        }
        return 1;
    }

    Component.onCompleted: {
        //main.addAccountsModelListener( globalStatusComponent );
        main.accountsModelChanged.connect(onAccountsModelChanged);
        main.presenceManagerChanged.connect(onPresenceManagerChaged);
    }

    ListModel {
        id: statusesModel
        ListElement { text: "Online"; icon: "img/status-online.png"     ; index: 0}
        ListElement { text: "Offline"; icon: "img/status-offline.png"   ; index: 1}
        ListElement { text: "Away"; icon: "img/status-away.png"         ; index: 2}
        ListElement { text: "eXtended Away"; icon: "img/status-xa.png" ; index: 3}
        ListElement { text: "DND"; icon: "img/status-dnd.png"         ; index: 4}
        ListElement { text: "Invisible"; icon: "img/status-invisible.png" ; index: 5}
    }

    SystemPalette{ id: syspal }

    Rectangle{
        id:statusIndicator

        anchors.fill: parent
        color:"transparent"

        Image {
            id: globalStatusIcon
            anchors.verticalCenter: parent.verticalCenter
            source: statusesModel.get(Makneto.TelepathyTypes.ConnectionPresenceTypeOffline).icon
            height: parent.height - 6
            width: height
        }
        Rectangle{
            id: textWrapper
            height: presenceText.height + globalStatusMsgText.height
            anchors.verticalCenter: globalStatusIcon.verticalCenter
            anchors.left: globalStatusIcon.right
            anchors.right: parent.right
            anchors.leftMargin: 5
            color: "transparent"

            Text {
                id: presenceText
                text: globalStatus

                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
                wrapMode: Text.NoWrap
                color: main.useSyspal? syspal.highlightedText : "white"
                font.bold: true
            }
            Text {
                id: globalStatusMsgText
                text: defaultStatusMsg
                color: main.useSyspal? syspal.highlightedText : "white"

                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: presenceText.bottom
                //anchors.verticalCenter: globalStatusIcon.verticalCenter

                //height: parent.height
                wrapMode: Text.NoWrap

                elide: Text.ElideRight; textFormat: Text.PlainText // this adds \x2026 (…) when text is too long
            }
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                //log("clicked on statusIndicator ");

                if(statusPopupComponent.status != Component.Ready){
                    error("Component "+statusPopupComponent.url+" is not ready!");
                    error(statusPopupComponent.errorString());
                    return false;
                }

                if (popup !== undefined && popup !== null && popup.state != "hide"){
                    popup.hidePopup();
                }

                popup = statusPopupComponent.createObject(main);
                if(popup === null){
                    error("error creating popup");
                    error(statusPopupComponent.errorString());
                    return false;
                }
                var pos = statusIndicator.mapToItem(main,statusIndicator.width - main.toolbarHeight, main.toolbarHeight);
                popup.popupFromX = pos.x;
                popup.popupFromY = pos.y;
                popup.finalWidth = main.width * .7;
                popup.finalHeight = main.height * .5;
                popup.statusesModel = statusesModel;

                if (globalStatusComponent._presenceManager === undefined)
                    error("we don't have pressenceManager!");
                popup.setGlobalStatusComponent( globalStatusComponent );

                popup.state = "visible"
                //log("visible popup "+JSON.stringify(parent));
            }
        }
    }  
}
