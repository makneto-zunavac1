/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import QtQuick 1.0

Scene {
    id: contactListScene

    function log(msg){
        console.log("II [ContactListScene.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [ContactListScene.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [ContactListScene.qml]: "+msg);
    }

    ContactList{
        id: contactList
        anchors.top: contactListPanel.bottom
        anchors.left: parent.left;
        anchors.bottom: parent.bottom
        width: contactListPanel.width
    }

    ContactListPanel{
        id: contactListPanel
        anchors.top: parent.top
        anchors.left: parent.left;
        anchors.right: parent.right
        //height: 45
    }

}
