/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import Qt 4.7
import QtQuick 1.0
import "./components/styles/default" as DefaultStyles
import "components"
import org.makneto 0.1 as Makneto

AbstractPopup{

    property variant contactLine: {};
    property bool defined : contactLine.accountId !== undefined // true while popup is not set yet

    // this weird code align all buttons to block... I don't know how make it better...
    property int maxButtonWidth: Math.max(chatButton.width, Math.max(callButton.width, videoButton.width))

    SystemPalette { id: syspal }

    Rectangle{
        id: background
        anchors.fill: wrapper
        opacity: wrapper.opacity
        color: main.useSyspal? syspal.mid :"#323235"
        radius: (width*0.03)

        Rectangle{
            id: border
            anchors.fill: parent
            radius: parent.radius
            border{ color: main.useSyspal? syspal.windowText :"white"; width: 1 }
            color: "transparent"
            opacity: 0.25
            //opacity: 1.0
        }

        Text{
            id: contactName
            color: main.useSyspal? syspal.windowText :"white"
            anchors{
                top: parent.top;
                left: parent.left;
                margins: contactId.height;
            }
            text: defined? contactLine.titleText: "???"
            font.bold: true
            font.pixelSize: contactId.height * 1.3
        }
        Text{
            id: contactId
            color: main.useSyspal? syspal.windowText :"gray"
            opacity: 0.6
            anchors{
                top: contactName.bottom;
                left: parent.left;
                leftMargin: 6
            }
            text: defined? contactLine.contactId : "???"
        }

        Button{
            id: chatButton
            anchors{ right: parent.right; top: contactId.bottom}
            anchors.margins: 6
            text: "Start Chat"
            minimumWidth: maxButtonWidth

            onClicked: {
                main.getSessionController().requestSession(contactLine.accountId, contactLine.contactId, Makneto.Session.SessionTypeText);
                hidePopup();
            }
        }
        Button{
            id: callButton
            anchors{ right: parent.right; top: chatButton.bottom}
            anchors.margins: 6
            text: "Start Call"
            minimumWidth: maxButtonWidth

            onClicked: {
                main.getSessionController().requestSession(contactLine.accountId, contactLine.contactId, Makneto.Session.SessionTypeAudio);
                hidePopup();
            }
        }
        Button{
            id: videoButton
            anchors{ right: parent.right; top: callButton.bottom}
            anchors.margins: 6
            text: "Start Video Call"
            minimumWidth: maxButtonWidth

            onClicked: {
                main.getSessionController().requestSession(contactLine.accountId, contactLine.contactId, Makneto.Session.SessionTypeVideo);
                hidePopup();
            }
        }

        Image {
            id: contactIcon
            anchors{ top: contactId.bottom; right: videoButton.left; margins: 6}
            height: Math.min( parent.height / 2, parent.width / 2)
            width: height
            source: defined? contactLine.iconSource :""
        }

        Text{
            id:statusText
            text: defined? contactLine.statusMessage: "???"
            color: main.useSyspal? syspal.windowText :"white"
            anchors{
                top: contactIcon.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins: 6
            }
            font.italic: true
            wrapMode: Text.NoWrap
            textFormat: Text.RichText
            clip: true
        }
    }
}
