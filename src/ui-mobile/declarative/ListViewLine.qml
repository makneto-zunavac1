/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.0

AbstractListViewLine {
  id: myDelegate
  height: (title.height + statusMsg.height) + (wrapper.anchors.topMargin + wrapper.anchors.bottomMargin ) + 2 // topBorder.height + bottomBorder.height
  //width: parent.width

  // these properties should be set from parent
  property string iconSource:""
  property variant titleText:""
  property variant statusText:""
  property variant iconOpacity:1
  property variant titleTextBold: false
  property variant textElide: Text.ElideNone

  SystemPalette{ id: syspal }

  Rectangle{
      id: wrapper
      anchors.topMargin: topMargin
      anchors.bottomMargin: bottomMargin
      anchors.leftMargin: leftMargin
      anchors.rightMargin: rightMargin
      anchors.fill: parent
      color: "transparent"

      // TODO: add icon overlay for statuses (dnd, away...)
      Image {
          id: iconImage
          width: height

          anchors{ top: parent.top; bottom: statusMsg.bottom}

          source: iconSource
          opacity: iconOpacity
      }

      Text {
          id: title
          text: titleText
          color:  main.useSyspal? syspal.windowText :"white"
          anchors.top: iconImage.top
          anchors.left: iconImage.right
          anchors.right: parent.right
          anchors.leftMargin: 8
          wrapMode: Text.NoWrap
          font.bold: titleTextBold
          clip: textElide == Text.ElideNone? true : false
          elide: textElide
      }
      Text {
          id: statusMsg
          text: statusText
          color: main.useSyspal? syspal.windowText :"grey"
          opacity: .6
          anchors.left: iconImage.right
          anchors.right: parent.right
          anchors.top: title.bottom
          anchors.leftMargin: title.anchors.leftMargin
          wrapMode: Text.NoWrap
          textFormat: Text.RichText
          clip: textElide == Text.ElideNone? true : false
          elide: textElide
      }
  }

}
