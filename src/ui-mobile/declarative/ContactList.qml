/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import Qt 4.7
import QtQuick 1.0
import org.makneto 0.1 as Makneto

Item {
  id: contactList

  property variant _model: ListModel {}
  property variant contactDetailComponent : Qt.createComponent("ContactDetailPopup.qml");

  function log(msg){
      console.log("II [ContactList.qml]: "+msg);
  }
  function error(msg){
      console.log("EE [ContactList.qml]: "+msg);
  }
  function warn(msg){
      console.log("WW [ContactList.qml]: "+msg);
  }

  function onContactsModelChanged(model){
      _model = model;
      log("set contacts model");
      // this is emited every change
      _model.contactCountChanged.connect(
                  function(){
                      try{
                          // print all contacts for debug...
                          //log("update contacts...");
                          //log("available contacts "+_model.contactCount)

                          for (var i=0; i< _model.contactCount; i++){
                              var id = _model.data(i, Makneto.AccountsModel.IdRole);
                              //log("  "+i+": "+id);
                          }
                      }catch(e){
                          error(e);
                      }
                  });
  }

  Component.onCompleted: {
      //main.addContactsModelListener( contactList );
      main.contactsModelChanged.connect(onContactsModelChanged);
  }

  Rectangle {
      id: background
      anchors.fill: parent
      color: "transparent"

      ListView {
          id: contactsView
          model: contactList._model;
          anchors.fill: parent

          delegate: ListViewLine{
              id: contactLine
              width: contactsView.width

              property variant contactId : id
              property variant accountId: managerId
              property string iconSource : avatar? "file:"+ avatar : "qrc:/declarative/img/default_avatar.jpg"
              property variant titleText: aliasName
              property variant statusText: "" + contactId+", "+presenceMessage
              //property variant statusText: accountId+" " + contactId+", "+presenceMessage
              property variant iconOpacity: (presenceType) == Makneto.TelepathyTypes.ConnectionPresenceTypeOffline ? 0.2:1.0;
              property variant statusMessage: presenceMessage
              property variant leftMargin: 37

              MouseArea{
                  anchors.fill: parent
                  onClicked: {
                      log("show contact detail for "+contactId);

                      if(contactDetailComponent.status != Component.Ready){
                          error("Component "+contactDetailComponent.url+" is not ready!");
                          return false;
                      }

                      var popup = contactDetailComponent.createObject(main);
                      if(popup === null){
                          error("error creating popup");
                          error(contactDetailComponent.errorString());
                          return false;
                      }
                      var pos = contactLine.mapToItem(main,contactLine.width / 2, contactLine.height / 2);
                      popup.popupFromX = pos.x;
                      popup.popupFromY = pos.y;
                      popup.finalWidth = main.width * .7;
                      popup.finalHeight = main.height * .6;

                      popup.contactLine = contactLine

                      popup.state = "visible"
                  }
              }
          }

      }
  }
}
