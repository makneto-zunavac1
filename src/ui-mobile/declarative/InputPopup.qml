/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import QtQuick 1.0
import "components"

AbstractPopup{
    id: inputPopup

    signal enterText(string text);
    signal typing(variant b);

    function show(){
        inputPopup.state = "visible";
        textArea.editorFocus = true;
        textArea.cursorPosition = 0;
    }

    Rectangle{
        id: background
        anchors.fill: wrapper
        opacity: wrapper.opacity
        color: "#323235"
        radius: (sendButton.height * .5)

        TextArea{
            id: textArea
            anchors{top: parent.top; right: parent.right; bottom:sendButton.top; left: parent.left; margins: 4;}

            Keys.onReleased: {
                typing(true);
            }
        }
        Button{
            id: sendButton
            text: "Send"
            anchors{ right: parent.right; bottom: parent.bottom; margins: 4;}
            onClicked: {
                enterText(textArea.text);
                textArea.text = "";
                // now, we can destry it and trash whole content
                inputPopup.destroyOnHide = true;
                hidePopup();
                textArea.focus = false
            }
        }
    }
}
