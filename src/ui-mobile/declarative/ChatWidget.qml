/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
import QtQuick 1.0
import Qt 4.7
import "./components/styles/default" as DefaultStyles
import "components"
import org.makneto 0.1 as Makneto

import QtWebKit 1.0

Rectangle {
    id: chatWidget
    width: 100
    height: 62
    color: main.useSyspal? syspal.window :"black"

    signal sendMessage(string sessionId, string text)
    signal changeChatState(string sessionId, int state)

    property variant inputPopupComponent : Qt.createComponent("InputPopup.qml");
    property variant messageItemComponent : Qt.createComponent("MessageItem.qml");
    property variant popup: undefined
    property bool typingState: false

    function log(msg){
        console.log("II [ChatWidget.qml]: "+msg);
    }
    function error(msg){
        console.log("EE [ChatWidget.qml]: "+msg);
    }
    function warn(msg){
        console.log("WW [ChatWidget.qml]: "+msg);
    }
    function textMessageReceived(text, contact){
        //log("message received from "+contact+": "+text);
        history.addMessage(true, contact, new Date(), text);
    }
    function chatStateChanged(chatState, contact){
        // status message like "karry typing..."
        var msg = "";
        if (chatState == Makneto.Session.ChatStateGone)
            msg = contact+" gone"
        if (chatState == Makneto.Session.ChatStateComposing)
            msg = contact+" typing..."

        statusMessage.text = msg;
    }

    function sendMessagePriv(message){
        message = message.trim();
        if (message.length===0)
            return;
        //log("sending message... "+message);
        sendMessage(sessionScene.sessionId, message);
        history.addMessage(false, "you", new Date(), message);
    }
    function typing(b){
        if (b){
            typingTimer.restart();
        }
        if (typingState == b)
            return;
        typingState = b;
        //log("typing "+b);
        changeChatState(sessionScene.sessionId, b? Makneto.Session.ChatStateComposing: Makneto.Session.ChatStateActive)
    }

    Timer{
        id: typingTimer
        interval: 3000; running: false; repeat: false
        onTriggered: {
            typing(false);
        }
    }

    Component.onCompleted: {

    }

    SystemPalette{ id: syspal }

    Rectangle{
        id: historyWrapper
        BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
        anchors{ bottom: editor.top; right: parent.right; left: parent.left; top: parent.top; margins: 4}
        color: parent.color

        // Create a flickable to view a large view.
        Flickable{
            id: history
            anchors.fill: parent
            clip: true;

            contentHeight: content.height
            contentWidth: content.width

            function addMessage(incoming, sender, time, message) {
                var item = messageItemComponent.createObject(content);
                item.incoming = incoming;
                item.sender = sender;
                item.time = time;
                item.message = message;
                item.x = 0;
                item.y = content.height;
                content.height += item.height;
                updatePosition();
            }
            function updatePosition(){
                contentWrapper.height = Math.max(content.height, history.height);
                history.contentY = contentWrapper.height - history.height;
            }
            onHeightChanged: {updatePosition();}

            Rectangle{
                id: contentWrapper
                width: historyWrapper.width
                color: "transparent"
                Rectangle{
                    id: content
                    height: 0
                    width: historyWrapper.width
                    color: "transparent"
                    anchors.bottom: contentWrapper.bottom
                }
            }
        }
        Text {
            id: statusMessage
            text: ""
            color: main.useSyspal? syspal.windowText :"gray"
            opacity: 0.6
            anchors{horizontalCenter: parent.horizontalCenter; bottom: parent.bottom; leftMargin:6}
        }
    }

    Rectangle{
        id: editor
        color: parent.color
        //BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
        //height: Math.max( parent.height * 0.2, messageTextArea.minimumHeight)
        height: 2 * messageTextArea.minimumHeight
        anchors{ bottom: parent.bottom; right: parent.right; left: parent.left; margins: 4}

        Rectangle{
            id: flipButton
            anchors{top: parent.top; right: parent.right; bottom: parent.bottom}
            width: height
            color: parent.color
            BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    inputMethod.flipped = !inputMethod.flipped;
                    messageTextArea.focus = !inputMethod.flipped;
                }
            }
            Image {
                id: keyboard
                source: "img/keyboard.png"
                anchors.fill: parent
            }
        }

        Flipable {
            id: inputMethod
            state: main.useSyspal? syspal.window :"back"

            property bool flipped: false
            property int angle: 180
            property int yAxis: 0
            property int xAxis: 1

            anchors{top: parent.top; left: parent.left; bottom: parent.bottom; right: flipButton.left; rightMargin: 4}

            transform: Rotation {
                id: rotation; origin.x: inputMethod.width / 2; origin.y: inputMethod.height / 2
                axis.x: inputMethod.xAxis; axis.y: inputMethod.yAxis; axis.z: 0
            }

            states: State {
                name: "back"; when: inputMethod.flipped
                PropertyChanges { target: rotation; angle: inputMethod.angle }
            }

            transitions: Transition {
                ParallelAnimation {
                    NumberAnimation { target: rotation; properties: "angle"; duration: 500 }
                    SequentialAnimation {
                        NumberAnimation { target: inputMethod; property: "scale"; to: 0.75; duration: 250 }
                        NumberAnimation { target: inputMethod; property: "scale"; to: 1.0; duration: 250 }
                    }
                }
            }

            front: TextArea{
                id: messageTextArea
                anchors.fill: parent
                //anchors.margins: 10

                Keys.onReleased: {
                    if (event.key == Qt.Key_Return && !(event.modifiers & Qt.ShiftModifier)){
                        var message = messageTextArea.text.trim();
                        messageTextArea.text = "";
                        sendMessagePriv(message);
                        //items[0].component.state = "detail"
                    }
                    typing(true);
                }
                focus: true
            }

            back: Rectangle{
                id: back
                color: editor.color;
                anchors.fill: parent
                clip: true

                BorderImage { source: "img/lineedit.sci"; anchors.fill: parent }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        if (popup === undefined || popup === null){
                            if(inputPopupComponent.status != Component.Ready){
                                error("Component "+inputPopupComponent.url+" is not ready!");
                                error(inputPopupComponent.errorString());
                                return false;
                            }

                            popup = inputPopupComponent.createObject(main);
                            if(popup === null){
                                error("error creating popup");
                                error(inputPopupComponent.errorString());
                                return false;
                            }

                            popup.destroyOnHide = false;
                            popup.popupFromX = main.width * .9;
                            popup.popupFromY = main.height * .2;
                            popup.finalWidth = main.width * .8;
                            popup.finalHeight = editor.height * 3;
                            popup.state = "visible";

                            popup.enterText.connect(sendMessagePriv);
                            popup.typing.connect(typing);
                        }
                        popup.show();
                        typing(true);
                    }
                }
                Image {
                    id: sendIcon
                    source: "img/chat.svg"
                    anchors{horizontalCenter: parent.horizontalCenter; top: parent.top; bottom: parent.bottom}
                    width: height
                }
            }
        }
    }

}
