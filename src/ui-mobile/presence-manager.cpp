/*
 * PresenceManager - object for managing global presence, automatic change 
 * presence to "away" after long inactivity (TODO)...
 * 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QModelIndex>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/account.h>

#include "presence-manager.h"

PresenceManager::PresenceManager(MaknetoBackend::AccountsModel* accountModel, QObject* parent) :
QObject(parent),
_accountModel(accountModel) {

  connect(_accountModel, SIGNAL(accountCountChanged()),
    this, SLOT(onAccountCountChanged()));

  onAccountCountChanged();
}

PresenceManager::~PresenceManager() {
}

void PresenceManager::onPresenceChanged(Tp::ConnectionPresenceType type, const QString &status, const QString &statusMsg) {
  emit globalPresenceChanged(getGlobalPresenceType(), getGlobalPresenceStatus(), getGlobalPresenceStatusMessage());
}

void PresenceManager::onAccountCountChanged() {
  emit globalPresenceChanged(getGlobalPresenceType(), getGlobalPresenceStatus(), getGlobalPresenceStatusMessage());

  for (int i = 0; i < _accountModel->accountCount(); i++) {
    QVariant var = _accountModel->data(_accountModel->index(i, 0, QModelIndex()), MaknetoBackend::AccountsModel::ItemRole);
    MaknetoBackend::AccountsModelItem *item = var.value<MaknetoBackend::AccountsModelItem*>();
    if (item) {
      connect(item, SIGNAL(presenceChanged(Tp::ConnectionPresenceType, const QString &, const QString &)), 
        SLOT(onPresenceChanged(Tp::ConnectionPresenceType, const QString &, const QString &)));
    }
  }
}

void PresenceManager::requestGlobalPresence(int type, const QString &status, const QString &statusMessage) {
  for (int i = 0; i < _accountModel->accountCount(); i++) {
    QVariant var = _accountModel->data(_accountModel->index(i, 0, QModelIndex()), MaknetoBackend::AccountsModel::ItemRole);
    MaknetoBackend::AccountsModelItem *item = var.value<MaknetoBackend::AccountsModelItem*>();
    if (item)
      item->setRequestedPresence(type, status, statusMessage);
  }
}

MaknetoBackend::AccountsModelItem *PresenceManager::getGlobalPresenceAccount() {
  MaknetoBackend::AccountsModelItem *result = NULL;
  for (int i = 0; i < _accountModel->accountCount(); i++) {
    QVariant var = _accountModel->data(_accountModel->index(i, 0, QModelIndex()), MaknetoBackend::AccountsModel::ItemRole);
    MaknetoBackend::AccountsModelItem *item = var.value<MaknetoBackend::AccountsModelItem*>();
    if (item) {
      if (!result) {
        result = item;
        continue;
      }
      if (result->data(MaknetoBackend::AccountsModel::CurrentPresenceTypeRole).toInt()
        != item->data(MaknetoBackend::AccountsModel::CurrentPresenceTypeRole).toInt()
        && item->data(MaknetoBackend::AccountsModel::CurrentPresenceTypeRole).toInt() >= (int) Tp::ConnectionPresenceTypeOffline
        && item->data(MaknetoBackend::AccountsModel::CurrentPresenceTypeRole).toInt() <= (int) Tp::ConnectionPresenceTypeBusy) {

        result = item;
      }

    }
  }
  return result;
}

int PresenceManager::getGlobalPresenceType() {
  MaknetoBackend::AccountsModelItem *account = getGlobalPresenceAccount();
  if (account) {
    return account->data(MaknetoBackend::AccountsModel::CurrentPresenceTypeRole).toInt();
  }
  return (int) Tp::ConnectionPresenceTypeUnknown;
}

QString PresenceManager::getGlobalPresenceStatus() {
  MaknetoBackend::AccountsModelItem *account = getGlobalPresenceAccount();
  if (account) {
    return account->data(MaknetoBackend::AccountsModel::CurrentPresenceRole).toString();
  }
  return "";
}

QString PresenceManager::getGlobalPresenceStatusMessage() {
  MaknetoBackend::AccountsModelItem *account = getGlobalPresenceAccount();
  if (account) {
    return account->data(MaknetoBackend::AccountsModel::CurrentPresenceStatusMessageRole).toString();
  }
  return "";
}

#include "presence-manager.moc"

