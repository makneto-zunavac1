/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDebug>

#include "main-window.h"

// TODO: change tray icon related with global status

MainWindow::MainWindow(QApplication *app, QObject *parent) :
QMainWindow(),
_app(app),
_trayIcon(NULL),
_trayIconMenu(NULL),
_minimizeAction(NULL),
_maximizeAction(NULL),
_restoreAction(NULL),
_quitAction(NULL) {

  QIcon icon = QIcon(":/declarative/img/status-online.png");
  qDebug() << "MainWindow: tray sizes:" << icon.availableSizes();

  setWindowIcon(icon);
  setWindowTitle("Makneto Mobile");

  if (QSystemTrayIcon::isSystemTrayAvailable()) {

    createActions();
    _trayIconMenu = new QMenu(this);
    _trayIconMenu->addAction(_minimizeAction);
    _trayIconMenu->addAction(_maximizeAction);
    _trayIconMenu->addAction(_restoreAction);
    _trayIconMenu->addSeparator();
    _trayIconMenu->addAction(_quitAction);

    _trayIcon = new QSystemTrayIcon(icon, this);
    _trayIcon->setContextMenu(_trayIconMenu);

    _trayIcon->setToolTip("Makneto");

    _trayIcon->show();

    connect(_trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
      this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

  } else {
    qWarning() << "MainWindow: This environment doesn't support QSystemTrayIcon";
  }

  _app->setQuitOnLastWindowClosed(true);
  if (_app->quitOnLastWindowClosed()) {
    qDebug() << "MainWindow: will quit on close";
  }
}

void MainWindow::createActions() {
  _minimizeAction = new QAction(tr("Mi&nimize"), this);
  connect(_minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

  _maximizeAction = new QAction(tr("Ma&ximize"), this);
  connect(_maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

  _restoreAction = new QAction(tr("&Restore"), this);
  connect(_restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

  _quitAction = new QAction(tr("&Quit"), this);
  connect(_quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::closeEvent(QCloseEvent *event) {
  if (_trayIcon && _trayIcon->isVisible()) {
    qWarning() << "MainWindow: The program will keep running in the system tray.";
    qWarning() << "MainWindow: To terminate the program, choose Quit in the context menu of the system tray entry.";
    toggleVisibility();
    event->ignore();
  } else {
    qWarning() << "MainWindow: Close window and exit.";
    event->accept();
  }
}

void MainWindow::setVisibleAndActivate() {
  if (!isVisible())
    toggleVisibility();
  activateWindow();
}

void MainWindow::toggleVisibility() {
  if (isVisible())
    _geometry = saveGeometry();
  setVisible(!isVisible());
  if (isVisible())
    restoreGeometry(_geometry);
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason) {
  switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
      toggleVisibility();
      break;
    case QSystemTrayIcon::MiddleClick:
      break;
    default:
      ;
  }
}

MainWindow::~MainWindow() {
  if (_trayIcon)
    _trayIcon->deleteLater();
  if (_trayIconMenu)
    _trayIconMenu->deleteLater();
  if (_minimizeAction)
    _minimizeAction->deleteLater();
  if (_maximizeAction)
    _maximizeAction->deleteLater();
  if (_restoreAction)
    _restoreAction->deleteLater();
  if (_quitAction)
    _quitAction->deleteLater();
}

#include "main-window.moc"
