/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QObject>
#include <QVariant>
#include <QTimer>

#include "../libnotify-qt/Notification.h"
#include "../backend/session.h"

#ifndef NOTIFICATIONITEM_H
#define	NOTIFICATIONITEM_H

class NotificationItem : public QObject {

  Q_OBJECT
  Q_DISABLE_COPY(NotificationItem)
  Q_ENUMS(NotificationType)

public:
  enum NotificationType {
    ChatType,
    AudioCallType,
    VideoCallType,
    CallErrorType,
    SendMessageErrorType
  };

  NotificationItem(int id, MaknetoBackend::Session *session, NotificationType type, QString msg = QString());
  virtual ~NotificationItem();

  int getId();
  QVariant data(int role);
  QString getTitle();
  NotificationType getType();
  MaknetoBackend::Session *getSession();

Q_SIGNALS:
  void acceptNotification(int id);
  void declineNotification(int id);
  void notificationExpired(int id);

  public
Q_SLOTS:
  void onNotificationAction(const QString &);
  void onNotificationClosed(quint32);
  void onExpire();
  void onCallEnded(const QString &);

private:
  int id;
  NotificationType type;
  QString message;
  MaknetoBackend::Session *_session;
  Notification *_notification;
  QTimer *_expireTimer;
};

#endif	/* NOTIFICATIONITEM_H */

