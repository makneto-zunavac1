/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDebug>
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsWidget>
#include <QGraphicsLinearLayout>
#include <QDeclarativeComponent>
#include <QDeclarativeEngine>
#include <QDebug>
#include <QDeclarativeView>
#include <QVBoxLayout>
#include <QMainWindow>
#include <QtWebKit/QtWebKit>
#include <QtWebKit/QGraphicsWebView>

#include "makneto.h"
#include "telepathy-initializer.h"
#include "telepathy-client.h"
#include "session.h"

int main(int argc, char **argv) {

  // TODO: this option should be configurable (it is configurable only over param. "--graphicssystem") Other values can be "native", "raster", "opengl1" and "opengl".
  //QApplication::setGraphicsSystem(QString("raster"));

  QApplication app(argc, argv);
  //  Phonon needs QCoreApplication::applicationName to be set to export audio output names through the DBUS interface
  app.setApplicationName("MaknetoMobile"); 
  Makneto makneto(&app);
  if (!makneto.init()) {
    qWarning() << "Makneto initializing failed, exiting";
    return -1;
  }

  return app.exec();
}
