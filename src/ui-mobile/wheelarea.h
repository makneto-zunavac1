/**
 * Copyright 2010 Max Desyatov <max.desyatov@gmail.com>
 * 
 * https://github.com/explicitcall/QMLbox
 * 
 * Warning: license is unknown!
 */

#ifndef WHEELAREA_H

#define WHEELAREA_H

#include <QDeclarativeItem>
#include <QGraphicsSceneWheelEvent>

class WheelArea : public QDeclarativeItem {
  Q_OBJECT
  Q_DISABLE_COPY(WheelArea)
public:

  explicit WheelArea(QDeclarativeItem *parent = 0) : QDeclarativeItem(parent) {
  }
protected:

  void wheelEvent(QGraphicsSceneWheelEvent *event);

signals:
  void verticalWheel(int delta);
  void horizontalWheel(int delta);
};

#endif // WHEELAREA_H