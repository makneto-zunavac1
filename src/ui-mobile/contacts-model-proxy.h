/*
 * this contact model proxy that joins contacts from all accounts 
 * to one level model
 * 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QtCore/QObject>
#include <QAbstractListModel>

#include <TelepathyQt4/Account>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/Types>

// backend
#include "../backend/telepathy-client.h"
#include "../backend/session.h"
#include "../backend/voip/element-factory/defaultelementfactory.h"
#include "../backend/voip/element-factory/deviceelementfactory.h"
#include "../backend/accounts-model-item.h"
#include "../backend/accounts-model.h"

#ifndef CONTACTSMODEL_H
#define	CONTACTSMODEL_H

class ContactsModelProxy : public QAbstractItemModel {

  Q_OBJECT
  Q_DISABLE_COPY(ContactsModelProxy)

  Q_PROPERTY(int contactCount READ contactCount NOTIFY contactCountChanged)

public:

  enum ContactFiltering {
    All,
    AvailableOnly
  };

  ContactsModelProxy(QObject *parent, MaknetoBackend::AccountsModel* accountsModel);
  int contactCount() const;
  
Q_INVOKABLE QVariant data(int row, int role); // ivokable, for QML  

  // virtual methods from QAbstractItemModel
  virtual QModelIndex index(int row, int column = 0, const QModelIndex &parent = QModelIndex()) const;
  virtual QModelIndex parent(const QModelIndex &index) const;
  virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
  virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role) const;

Q_SIGNALS:
  void contactCountChanged();

  public
Q_SLOTS:
  void onAccountModelChanged();

  void onRowsAboutToBeInserted(const QModelIndex &parent, int first, int last);
  void onRowsInserted(const QModelIndex &parent, int first, int last);

  void onRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
  void onRowsRemoved(const QModelIndex &parent, int first, int last);

  void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

private:  

  bool _inserting;
  bool _removing;
  
  MaknetoBackend::AccountsModel* _accountsModel;
  
  /*
   * Original model AccountsModel:
   *   - root
   *    + Account0	... account offset = 0
   *    |  + Contact0
   *    |  + Contact1
   *    |  + Contact2
   *    |  + Contact3
   *    + Account1	... account offset = 4
   *       + Contact4
   *       + Contact5
   * 
   * Final model (this)
   *   - root
   *    + Contact0
   *    + Contact1
   *    + Contact2
   *    + Contact3
   *    + Contact4
   *    + Contact5
   */

  QMap<QModelIndex, QPersistentModelIndex> _indexesMapping; // mapping from proxy (this) index to parents (_accountModel) index
  QMap<QPersistentModelIndex, int> _accountOffset;
  

};

#endif	/* CONTACTSMODEL_H */

