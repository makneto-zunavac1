/*
 * PresenceManager - object for managing global presence, automatic change 
 * presence to "away" after long inactivity (TODO)...
 * 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QObject>
#include <TelepathyQt4/Account>

#include "../backend/accounts-model.h"

#ifndef PRESENCEMANAGER_H
#define	PRESENCEMANAGER_H

class PresenceManager : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(PresenceManager)

public:
  PresenceManager(MaknetoBackend::AccountsModel* accountModel, QObject* parent = 0);
  virtual ~PresenceManager();
  Q_INVOKABLE void requestGlobalPresence(int type, const QString &status, const QString &statusMessage);
  Q_INVOKABLE int getGlobalPresenceType();
  Q_INVOKABLE QString getGlobalPresenceStatus();
  Q_INVOKABLE QString getGlobalPresenceStatusMessage();

Q_SIGNALS:
  void globalPresenceChanged(int type, const QString &status, const QString &statusMessage);

  public
Q_SLOTS:
  void onPresenceChanged(Tp::ConnectionPresenceType, const QString &, const QString &);
  void onAccountCountChanged();

private:
  MaknetoBackend::AccountsModelItem *getGlobalPresenceAccount();

  MaknetoBackend::AccountsModel* _accountModel;
};

#endif	/* PRESENCEMANAGER_H */

