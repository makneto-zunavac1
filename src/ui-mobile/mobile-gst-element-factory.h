/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */


#include <QObject>
#include <QGst/Bin>
#include <QGst/ElementFactory>

#include <QtGui>
#include <QtDeclarative>
#include <QtOpenGL/QGLWidget>
#include <gst/gst.h>
#include <QtGstQmlSink/qmlvideosurfacegstsink.h>
#include <QtGstQmlSink/qmlgstvideoitem.h>

#include "../backend/voip/element-factory/defaultelementfactory.h"

#ifndef MOBILEGSTELEMENTFACTORY_H
#define	MOBILEGSTELEMENTFACTORY_H

class MobileGstElementFactory : public MaknetoBackend::DefaultElementFactory {
  Q_OBJECT
  Q_DISABLE_COPY(MobileGstElementFactory)

public:
  MobileGstElementFactory(QDeclarativeView *qmlView);
  virtual ~MobileGstElementFactory();
  virtual QGst::ElementPtr makeVideoOutputElement();

  QmlPainterVideoSurface *mapElementToSurface(QGst::ElementPtr element);
  bool forgetElement(QGst::ElementPtr element);

  public 
Q_SLOTS:
  void onFrameChanged();

private:
  QDeclarativeView *_qmlView;
  QGLWidget *g;

  QMap<QGst::ElementPtr, QmlPainterVideoSurface *> _surfacesMap;
  //QMap<QGst::ElementPtr, QGLWidget *> _glMap;
};

#endif	/* MOBILEGSTELEMENTFACTORY_H */

