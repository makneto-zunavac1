/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */


#include "notification-item.h"
#include "notifications-model.h"

static const QString ACCEPT_KEY = "accept";
static const QString DECLINE_KEY = "decline";

NotificationItem::NotificationItem(int id, MaknetoBackend::Session *session, NotificationType type, QString msg) :
id(id),
type(type),
message(msg),
_session(session),
_notification(NULL),
_expireTimer(NULL) {

  qDebug() << "NotificationItem: create notification" << id << ": " << getTitle() << " " << _session->getName();

  if (Notification::isInitted()) {
    _notification = new Notification(getTitle(), _session->getName(), _session->getIcon(), this);

    connect(_notification, SIGNAL(actionInvoked(const QString &)), this, SLOT(onNotificationAction(const QString &)));
    connect(_notification, SIGNAL(closed(quint32)), this, SLOT(onNotificationClosed(quint32)));

    _notification->addAction(ACCEPT_KEY, "Accept");
    _notification->addAction(DECLINE_KEY, "Decline");
    _notification->setTimeout(20000); // 10s
    _notification->show();
  }

  if (type == AudioCallType || type == VideoCallType) {
    connect(session, SIGNAL(callEnded(QString)), SLOT(onCallEnded(QString)));
  }

  if (type == CallErrorType || type == SendMessageErrorType) {
    QTimer::singleShot(10000, this, SLOT(onExpire()));
  }
}

void NotificationItem::onNotificationAction(const QString &action) {
  if (action == ACCEPT_KEY) {
    emit acceptNotification(id);
    return;
  }
  if (action == DECLINE_KEY) {
    emit declineNotification(id);
    return;
  }
  qWarning() << "NotificationItem: udefined notification action" << action;
}

void NotificationItem::onNotificationClosed(quint32) {
  _notification = NULL;
}

NotificationItem::~NotificationItem() {
  if (_notification)
    _notification->close(); // _notification autodete is set to true...
}

void NotificationItem::onExpire() {
  qDebug() << "NotificationItem: notification" << id << "expired";
  emit notificationExpired(getId());
}

void NotificationItem::onCallEnded(const QString &msg) {
  qDebug() << "NotificationItem: call ended, decline notification";
  emit declineNotification(id);
}

QVariant NotificationItem::data(int role) {
  switch (role) {
    case NotificationsModel::IdRole:
      return id;
    case NotificationsModel::TitleRole:
      return getTitle();
    case NotificationsModel::SessionIdRole:
      return _session->getUniqueName();
    case NotificationsModel::SessionNameRole:
      return _session->getName();
    case NotificationsModel::MessageRole:
      return message;
    case NotificationsModel::TypeRole:
      return type;
    case NotificationsModel::IconRole:
      return _session->getIcon();
    default:
      return QVariant();
  }
}

QString NotificationItem::getTitle() {
  switch (type) {
    case VideoCallType:
      return "Incoming Video Call";
    case AudioCallType:
      return "Incoming Audio Call";
    case CallErrorType:
      return "Call Error";
    case SendMessageErrorType:
      return "Sending error";
    case ChatType:
    default:
      return "New Message";
  }
}

int NotificationItem::getId() {
  return id;
}

NotificationItem::NotificationType NotificationItem::getType() {
  return type;
}

MaknetoBackend::Session *NotificationItem::getSession() {
  return _session;
}

#include "notification-item.moc"
