/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include "session-model.h"

SessionModel::SessionModel(MaknetoBackend::TelepathyClient* client, NotificationsModel* notificationsModel, QObject *parent) :
QAbstractListModel(parent),
_client(client),
_notificationsModel(notificationsModel) {

  connect(_client, SIGNAL(sessionCreated(MaknetoBackend::Session *, bool, bool)),
    SLOT(onSessionCreated(MaknetoBackend::Session *, bool, bool)));

  connect(notificationsModel, SIGNAL(acceptCall(QString)), SLOT(onAcceptCall(QString)));
  connect(notificationsModel, SIGNAL(rejectCall(QString)), SLOT(onRejectCall(QString)));
  connect(notificationsModel, SIGNAL(acceptChat(QString)), SLOT(onAcceptChat(QString)));
  connect(notificationsModel, SIGNAL(rejectChat(QString)), SLOT(onRejectChat(QString)));
  connect(notificationsModel,
    SIGNAL(notificationRemoved(const QString &, NotificationItem::NotificationType)),
    SLOT(onNotificationRemoved(const QString &, NotificationItem::NotificationType)));

  QHash<int, QByteArray> roles;

  // general roles
  roles[ItemRole] = "item";
  roles[IdRole] = "id";
  roles[NameRole] = "sessionName";
  roles[IconRole] = "icon";
  roles[TypeRole] = "type";
  roles[LastMessageRole] = "lastMessage";

  setRoleNames(roles);
}

SessionModel::~SessionModel() {

}

void SessionModel::onSessionCreated(MaknetoBackend::Session *session, bool extendsExisting, bool requested) {
  bool newSession = false;
  SessionModelItem *item = _itemsMap[ session->getUniqueName() ];
  if (!item) { // new session
    item = new SessionModelItem(session, this);
    newSession = true;

    connect(item, SIGNAL(textMessageReceived(const QString &, const QString &, const QString &)),
      this, SIGNAL(textMessageReceived(const QString &, const QString &, const QString &)));
    connect(item, SIGNAL(textMessageReceived(const QString &, const QString &, const QString &)),
      this, SLOT(onTextMessageReceived(const QString &, const QString &, const QString &)));
    connect(item, SIGNAL(whiteboardMessageReceived(const QString &, const QString &, const QString &)),
      this, SIGNAL(whiteboardMessageReceived(const QString &, const QString &, const QString &)));
    connect(item, SIGNAL(messageEnqueued(const QString &)), SLOT(onMessageEnqueued(const QString &)));

    connect(item, SIGNAL(sessionTypeChanged(const QString &, int)),
      this, SIGNAL(sessionTypeChanged(const QString &, int)));
    connect(item, SIGNAL(sessionTypeChanged(const QString &, int)),
      this, SLOT(onSessionTypeChanged(const QString &, int)));
    
    connect(item, SIGNAL(messageSent(const QString &, const QString &)),
      this, SIGNAL(messageSent(const QString &, const QString &)));
    connect(item, SIGNAL(sendingError(const QString &, const QString &, const QString &)),
      this, SIGNAL(sendingError(const QString &, const QString &, const QString &)));
    connect(item, SIGNAL(sendingError(const QString &, const QString &, const QString &)),
      this, SLOT(onSendingError(const QString &, const QString &, const QString &)));    
    connect(item, SIGNAL(chatStateChanged(const QString &, int, const QString &)),
      this, SIGNAL(chatStateChanged(const QString &, int, const QString &)));

    connect(item, SIGNAL(incomingCall(const QString &)), SLOT(onIncomingCall(const QString &)));
    //connect(item, SIGNAL(callReady(const QString &)), SIGNAL(callReady(const QString &)));
    connect(item, SIGNAL(callReady(const QString &)), SLOT(onCallReady(const QString &)));
    connect(item, SIGNAL(callEnded(const QString &)), SLOT(onCallEnded(const QString &)));
    connect(item, SIGNAL(callError(const QString &, const QString &)), SLOT(onCallError(const QString &, const QString &)));

    connect(item, SIGNAL(videoPreviewAvailable(const QString &, QGst::ElementPtr)), SIGNAL(videoPreviewAvailable(const QString &, QGst::ElementPtr)));
    connect(item, SIGNAL(videoAvailable(const QString &, QGst::ElementPtr)), SIGNAL(videoAvailable(const QString &, QGst::ElementPtr)));
    connect(item, SIGNAL(videoPreviewRemoved(const QString &, QGst::ElementPtr)), SIGNAL(videoPreviewRemoved(const QString &, QGst::ElementPtr)));
    connect(item, SIGNAL(videoRemoved(const QString &, QGst::ElementPtr)), SIGNAL(videoRemoved(const QString &, QGst::ElementPtr)));
    connect(item, SIGNAL(remoteAudioStreamAdded(const QString &)), SLOT(onRemoteAudioStreamAdded(const QString &)));

    _itemsMap.insert(item->getId(), item);
  }
  if (requested) {
    openSession(item);

    //if (newSession) {
    if (session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeAudio) ||
      session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeVideo)) {

      if (session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeCallPending)) {
        _dialingSessions.insert(session->getUniqueName());
        emit startDialing();
      } else {
        //bool beeping = !_beepingSessions.isEmpty();
        _beepingSessions.insert(session->getUniqueName());
        //if (!beeping)
        emit startBeeping();
      }
    }
    //}

  } else if (newSession) {

    NotificationItem::NotificationType type = NotificationItem::ChatType;
    if (session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeAudio))
      type = NotificationItem::AudioCallType;
    if (session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeVideo))
      type = NotificationItem::VideoCallType;

    if (type == NotificationItem::AudioCallType || type == NotificationItem::VideoCallType) {
      //bool ringing = !_ringingSessions.isEmpty();
      _ringingSessions.insert(item->getId());
      //if (!ringing && !_ringingSessions.isEmpty())
      emit startRinging();
    }

    _notificationsModel->addNotification(session, type);
  }
}

void SessionModel::onAcceptCall(const QString &sessionId) {
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: Session with id" << sessionId << "does't exists. This should not happen.";
    return;
  }
  if (!item->isVisible()) {
    openSession(item);
  }
  item->getSession()->acceptCall();
}

void SessionModel::onRejectCall(const QString &sessionId) {
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: Session with id" << sessionId << "does't exists. This should not happen.";
    return;
  }
  /*
  if (!item->isVisible()) {
    openSession(item);
  }
   */
  item->getSession()->rejectCall();
}

void SessionModel::onAcceptChat(const QString &sessionId) {
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: Session with id" << sessionId << "does't exists. This should not happen.";
    return;
  }
  if (!item->isVisible()) {
    openSession(item);
  }
}

void SessionModel::onNotificationRemoved(const QString &sessionId, NotificationItem::NotificationType type) {
  if (type == NotificationItem::AudioCallType || type == NotificationItem::VideoCallType) {
    stopRinging(sessionId);
  }
}

void SessionModel::onSessionTypeChanged(const QString &sessionId, int type){
  int row = 0;
  for (; row < _items.size(); row++) {
    if (_items.at(row)->getId() == sessionId)
      break;
  }
  emit dataChanged( createIndex(row,0,0), createIndex(row,0,0) );
}
  
void SessionModel::onTextMessageReceived(const QString &sessionId, const QString &text, const QString &contact){
  int row = 0;
  for (; row < _items.size(); row++) {
    if (_items.at(row)->getId() == sessionId)
      break;
  }
  emit dataChanged( createIndex(row,0,0), createIndex(row,0,0) );  
}

void SessionModel::onRejectChat(const QString &sessionId) {
  // do nothing
}

void SessionModel::openSession(SessionModelItem *item) {
  if (!item->isVisible()) {
    emit beginInsertRows(QModelIndex(), _items.size(), _items.size());
    _items.push_back(item);
    emit endInsertRows();
  }

  emit sessionOpened(item->getSession()->getUniqueName(), false, true);
  item->setVisible(true);
}

void SessionModel::closeSession(const QString &sessionId) {
  int row = 0;
  for (; row < _items.size(); row++) {
    if (_items.at(row)->getId() == sessionId)
      break;
  }
  if (row >= _items.size())
    return;

  SessionModelItem *item = _items.at(row);
  if (item->getId() != sessionId)
    return;

  item->setVisible(false);
  emit beginRemoveRows(QModelIndex(), row, row);
  _items.removeOne(item);
  //_itemsMap.remove(id);
  emit endRemoveRows();
  emit sessionClosed(sessionId);
}

void SessionModel::onMessageEnqueued(const QString &sessionId) {
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: Session with id" << sessionId << "does't exists. This should not happen.";
    return;
  }
  if (!item->isVisible()) {
    if (!_notificationsModel->containsNotification(sessionId, NotificationItem::ChatType)) {
      _notificationsModel->addNotification(item->getSession(), NotificationItem::ChatType);
    }
  }
}

void SessionModel::onSendingError(const QString &sessionId, const QString &message, const QString &errorMessage) {
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: Session with id" << sessionId << "does't exists. This should not happen.";
    return;
  }
  _notificationsModel->addNotification(item->getSession(), NotificationItem::SendMessageErrorType, "Error sending message, "+errorMessage + " (" + message + ")");
}

void SessionModel::onIncomingCall(const QString &sessionId) {
  qDebug() << "SessionModel: onIncomingCall" << sessionId;
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: incoming call from Session (" << sessionId << ") that don't exists in SessionModel. This should not happen.";
    return;
  }
  NotificationItem::NotificationType type = item->getSession()->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeVideo) ?
    NotificationItem::VideoCallType : NotificationItem::AudioCallType;

  if (!_notificationsModel->containsNotification(sessionId, type)) {
    _notificationsModel->addNotification(item->getSession(), type);
  }

  bool ringing = !_ringingSessions.isEmpty();
  _ringingSessions.insert(item->getId());
  if (!ringing && !_ringingSessions.isEmpty())
    emit startRinging();
}

void SessionModel::onCallError(const QString &sessionId, const QString &errorMsg) {
  qDebug() << "SessionModel: onCallError" << sessionId << " " << errorMsg;
  SessionModelItem *item = _itemsMap[ sessionId ];
  if (!item) {
    qWarning() << "SessionModel: incoming call from Session (" << sessionId << ") that don't exists in SessionModel. This should not happen.";
    return;
  }
  _notificationsModel->addNotification(item->getSession(), NotificationItem::CallErrorType, errorMsg);
}

void SessionModel::onCallEnded(const QString &sessionId) {
  stopBeeping(sessionId);
  stopDialing(sessionId);
  emit callEnded(sessionId);
}

void SessionModel::onCallReady(const QString &sessionId) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }
  stopDialing(sessionId);
  _beepingSessions.insert(sessionId);
  emit startBeeping();
  emit callReady(sessionId);
}

void SessionModel::onRemoteAudioStreamAdded(const QString &sessionId) {
  stopDialing(sessionId);
  stopBeeping(sessionId);
}

int SessionModel::sessionCount() const {
  return _items.size();
}

int SessionModel::rowCount(const QModelIndex& index) const {
  return _items.size();
}

QVariant SessionModel::data(int row, int role) {
  return data(index(row, 0, QModelIndex()), role);
}

QVariant SessionModel::data(const QModelIndex& index, int role) const {
  if (index.column() != 0)
    return QVariant();

  SessionModelItem *item = _items.at(index.row());
  if (!item)
    return QVariant();

  return item->data(role);
}

void SessionModel::requestSession(const QString &accountId, const QString &contactId, int intType) {
  MaknetoBackend::Session::SessionType type = MaknetoBackend::Session::SessionTypeNone;
  // convert int to SessionType...
  switch (intType) {
    case MaknetoBackend::Session::SessionTypeText:
      type = MaknetoBackend::Session::SessionTypeText;
      break;
    case MaknetoBackend::Session::SessionTypeMuc:
      type = MaknetoBackend::Session::SessionTypeMuc;
      break;
    case MaknetoBackend::Session::SessionTypeAudio:
      type = MaknetoBackend::Session::SessionTypeAudio;
      break;
    case MaknetoBackend::Session::SessionTypeVideo:
      type = MaknetoBackend::Session::SessionTypeVideo;
      break;
  }
  qDebug() << "SessionModel: request session type" << type;
  _client->onSessionRequested(accountId, contactId, type);
}

void SessionModel::sendTextMessage(const QString &sessionId, const QString &text) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }
  item->sendTextMessage(text);
}

int SessionModel::getSessionType(const QString &sessionId) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return 0;
  }
  return item->getSession()->getSessionType();
}

void SessionModel::startCall(const QString &sessionId, bool video) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }
  _dialingSessions.insert(sessionId);
  emit startDialing();
  item->getSession()->startMediaCall(video);
}

void SessionModel::hangup(const QString &sessionId) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }
  item->getSession()->onHangup();
  stopDialing(sessionId);
  stopBeeping(sessionId);
}

void SessionModel::stopBeeping(const QString &sessionId) {
  bool beeping = !_beepingSessions.isEmpty();
  _beepingSessions.remove(sessionId);
  if (beeping && _beepingSessions.isEmpty())
    emit stopBeeping();
}

void SessionModel::stopRinging(const QString &sessionId) {
  bool ringing = !_ringingSessions.isEmpty();
  _ringingSessions.remove(sessionId);
  if (ringing && _ringingSessions.isEmpty())
    emit stopRinging();
}

void SessionModel::stopDialing(const QString &sessionId) {
  bool dialing = !_dialingSessions.isEmpty();
  _dialingSessions.remove(sessionId);
  if (dialing && _beepingSessions.isEmpty())
    emit stopDialing();
}

void SessionModel::sendWhiteboardMessage(const QString &sessionId, const QString &content) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }

  QDomDocument doc;
  doc.setContent(content);
  QDomElement elem = doc.documentElement();

  if (elem.isNull()) {
    qWarning() << "SessionModel: could not parse whiteboard XML from" << content;
    return;
  }

  item->sendWhiteboardMessage(elem);
}

void SessionModel::changeChatState(const QString &sessionId, int intState) {
  SessionModelItem* item = _itemsMap[sessionId];
  if (!item) {
    qWarning() << "SessionModel: session id" << sessionId << "does not exist";
    return;
  }

  MaknetoBackend::Session::ChatState state = MaknetoBackend::Session::ChatStateInactive;
  // convert int to SessionType...
  switch (intState) {
    case MaknetoBackend::Session::ChatStateActive:
      state = MaknetoBackend::Session::ChatStateActive;
      break;
    case MaknetoBackend::Session::ChatStateComposing:
      state = MaknetoBackend::Session::ChatStateComposing;
      break;
    case MaknetoBackend::Session::ChatStateGone:
      state = MaknetoBackend::Session::ChatStateGone;
      break;
    case MaknetoBackend::Session::ChatStateInactive:
      state = MaknetoBackend::Session::ChatStateInactive;
      break;
    case MaknetoBackend::Session::ChatStatePaused:
      state = MaknetoBackend::Session::ChatStatePaused;
      break;
  }
  item->changeChatState(state);
}

#include "session-model.moc"
