/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDebug>
#include <QSound>
#include <QMainWindow>
#include <QtOpenGL/QGLWidget>

// declarative 
#include <QDeclarativeView>
#include <QDeclarativeContext>
#include <QDeclarativeError>
#include <QDeclarativeItem>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeContext>

// Gstreamer and QtGstQmlSink
#include <gst/gst.h>
#include <QtGstQmlSink/qmlvideosurfacegstsink.h>
#include <QtGstQmlSink/qmlgstvideoitem.h>

// telepathy 
#include <TelepathyQt4/Types>
#include <TelepathyQt4/Account>

// backend
#include "../backend/telepathy-initializer.h"
#include "../backend/telepathy-client.h"
#include "../backend/session.h"
#include "../backend/voip/element-factory/defaultelementfactory.h"
#include "../backend/voip/element-factory/deviceelementfactory.h"
#include "../backend/accounts-model-item.h"
#include "../backend/accounts-model.h"

// makneto-mobile
#include "main-window.h"
#include "makneto.h"
#include "telepathytypes.h"
#include "contacts-model-proxy.h"
#include "contacts-sort-filter-model-proxy.h"
#include "mobile-gst-element-factory.h"
#include "sound-notificator.h"
#include "wheelarea.h"
#include "view-graber.h"

Makneto::Makneto(QApplication *app) : _app(app),
_qmlView(0),
_window(0),
_contactsModel(0),
_contactsProxy(0),
_sessionModel(0),
_notificationsModel(0),
_trayIcon(0),
_ownDeviceFactory(0) {

  // register data types to make its enums accessible from qml
  qmlRegisterUncreatableType<MaknetoBackend::AccountsModel > ("org.makneto", 0, 1, "AccountsModel", QLatin1String("This type is only exported for its enums"));
  qmlRegisterUncreatableType<MaknetoBackend::Session > ("org.makneto", 0, 1, "Session", QLatin1String("This type is only exported for its enums"));
  qmlRegisterUncreatableType<SessionModel > ("org.makneto", 0, 1, "SessionModel", QLatin1String("This type is only exported for its enums"));
  qmlRegisterUncreatableType<NotificationsModel > ("org.makneto", 0, 1, "NotificationsModel", QLatin1String("This type is only exported for its enums"));
  qmlRegisterUncreatableType<NotificationItem > ("org.makneto", 0, 1, "NotificationItem", QLatin1String("This type is only exported for its enums"));
  qmlRegisterUncreatableType<TelepathyTypes > ("org.makneto", 0, 1, "TelepathyTypes", QLatin1String("This type is only exported for its enums"));

  /* register QtGstQmlSink items for playing videos */
  qmlRegisterType<QmlGstVideoItem > ("Gst", 1, 0, "VideoItem");
  qmlRegisterType<QmlPainterVideoSurface > ("Gst", 1, 0, "VideoSurface");

  qmlRegisterType<WheelArea>("MyTools", 1, 0, "WheelArea");
  qmlRegisterType<ViewGraber>("MyTools", 1, 0, "ViewGraber");  
  
  _qmlView = new QDeclarativeView();
  _window = new MainWindow(_app, this);

  connect(_qmlView, SIGNAL(statusChanged(QDeclarativeView::Status)),
    this, SLOT(onQmlStatusChanged(QDeclarativeView::Status)));

  _qmlView->setResizeMode(QDeclarativeView::SizeRootObjectToView);
  _window->setCentralWidget(_qmlView);

}

Makneto::~Makneto() {
  MaknetoBackend::DeviceElementFactory::destroyAllFactories();

  if (_qmlView)
    _qmlView->deleteLater();
  if (_window)
    _window->deleteLater();
  if (_notificationsModel)
    _notificationsModel->deleteLater();
  if (_contactsProxy)
    _contactsProxy->deleteLater();
  if (_contactsModel)
    _contactsModel->deleteLater();
  if (_sessionModel)
    _sessionModel->deleteLater();
  if (_soundNotificator)
    _soundNotificator->deleteLater();
  if (_presenceManager)
    _presenceManager->deleteLater();
}

bool Makneto::init() {
  QDeclarativeContext* rootContext = _qmlView->rootContext();
  rootContext->setContextProperty("makneto", this);

  _qmlView->setSource(QUrl("qrc:/declarative/mainview.qml"));
  if (_qmlView->status() == QDeclarativeView::Error)
    return false;

  return true;
}

void Makneto::onQmlStatusChanged(QDeclarativeView::Status status) {
  switch (status) {
    case QDeclarativeView::Null:
      break;
    case QDeclarativeView::Ready:
      qDebug() << "Makneto: QML ready...";
      onViewReady();
      break;
    case QDeclarativeView::Loading:
      qDebug() << "Makneto: Loading QML...";
      break;
    case QDeclarativeView::Error:
      qWarning() << "Makneto: Error occurs while loading qml (ui) file. Exiting";
      qWarning() << _qmlView->errors();
      _app->exit(-1);
      break;
    default:
      qWarning() << "Makneto: udefind QML status" << status;
  }
}

void Makneto::onViewReady() {
  showWindow();

  _ownDeviceFactory = new MobileGstElementFactory(_qmlView);
  MaknetoBackend::DeviceElementFactory::installFactory(_ownDeviceFactory);

  MaknetoBackend::TelepathyInitializer *tpInit = new MaknetoBackend::TelepathyInitializer();

  connect(tpInit,
    SIGNAL(finished(MaknetoBackend::TelepathyClient *)),
    SLOT(onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient *)));

  tpInit->createClient(_app->applicationName());
}

void Makneto::showWindow() {
#ifdef Q_OS_SYMBIAN
  qDebug() << "running on Symbian, show fullscreen";
  _window->showFullScreen();
#elif defined(Q_WS_MAEMO_5) || defined(Q_WS_MAEMO_6)
  qDebug() << "running on Maemo, show maximized";
  _window->showMaximized();
#else
  qDebug() << "Makneto: running environment is not Symbian nor Maemo - show in window";
  _window->show();
#endif
}

void Makneto::onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient* client) {

  QDeclarativeContext* rootContext = _qmlView->rootContext();
  //QObject *rootObject = dynamic_cast<QObject*> (_qmlView->rootObject());

  // initialize models
  //  - account model direct from backend
  rootContext->setContextProperty("accountsModel", client->accountsModel());

  _presenceManager = new PresenceManager(client->accountsModel(), this);
  rootContext->setContextProperty("presenceManager", _presenceManager);

  //  - contacts model 
  _contactsModel = new ContactsModelProxy(this, client->accountsModel());
  _contactsProxy = new ContactsSortFilterModelProxy(_contactsModel, this);
  rootContext->setContextProperty("contactsModel", _contactsProxy);

  // - notifications model
  _notificationsModel = new NotificationsModel(client, _window, this);
  rootContext->setContextProperty("notificationsModel", _notificationsModel);

  // - session model
  _sessionModel = new SessionModel(client, _notificationsModel, this);
  rootContext->setContextProperty("sessionsModel", _sessionModel);

  connect(_sessionModel, SIGNAL(videoPreviewAvailable(const QString &, QGst::ElementPtr)), SLOT(onVideoPreviewAvailable(const QString &, QGst::ElementPtr)));
  connect(_sessionModel, SIGNAL(videoAvailable(const QString &, QGst::ElementPtr)), SLOT(onVideoAvailable(const QString &, QGst::ElementPtr)));
  connect(_sessionModel, SIGNAL(videoPreviewRemoved(const QString &, QGst::ElementPtr)), SLOT(onVideoPreviewRemoved(const QString &, QGst::ElementPtr)));
  connect(_sessionModel, SIGNAL(videoRemoved(const QString &, QGst::ElementPtr)), SLOT(onVideoRemoved(const QString &, QGst::ElementPtr)));

  _soundNotificator = new SoundNotificator(_app, this);
  connect(_sessionModel, SIGNAL(startBeeping()), _soundNotificator, SLOT(onStartBeeping()));
  connect(_sessionModel, SIGNAL(stopBeeping()), _soundNotificator, SLOT(onStopBeeping()));
  connect(_sessionModel, SIGNAL(startRinging()), _soundNotificator, SLOT(onStartRinging()));
  connect(_sessionModel, SIGNAL(stopRinging()), _soundNotificator, SLOT(onStopRinging()));
  connect(_sessionModel, SIGNAL(startDialing()), _soundNotificator, SLOT(onStartDialing()));
  connect(_sessionModel, SIGNAL(stopDialing()), _soundNotificator, SLOT(onStopDialing()));
  
  emit modelsReady();
  qDebug() << "Makneto: Telepathy has been initialised";
}

void Makneto::onVideoPreviewAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr) {
  //QDeclarativeContext* rootContext = _qmlView->rootContext();

  QmlPainterVideoSurface *surface = _ownDeviceFactory->mapElementToSurface(videoOutElemPtr);
  if (!surface) {
    qWarning() << "Makneto: element for id" << sessionId << " is not created by own factory";
    return;
  }
  //rootContext->setContextProperty(sessionId, surface);
  qDebug() << "Makneto: videoSurfaceReady preview," << sessionId << "," << surface;
  emit videoSurfaceReady("preview", sessionId, surface);
}

void Makneto::onVideoAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr) {
  //QDeclarativeContext* rootContext = _qmlView->rootContext();

  QmlPainterVideoSurface *surface = _ownDeviceFactory->mapElementToSurface(videoOutElemPtr);
  if (!surface) {
    qWarning() << "Makneto: element for id" << sessionId << " is not created by own factory";
    return;
  }
  //rootContext->setContextProperty(sessionId, surface);
  qDebug() << "Makneto: videoSurfaceReady video," << sessionId << "," << surface;
  emit videoSurfaceReady("video", sessionId, surface);
}

void Makneto::onVideoPreviewRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr) {
  _ownDeviceFactory->forgetElement(videoOutElemPtr);
  emit videoSurfaceRemoved("preview", sessionId);
}

void Makneto::onVideoRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr) {
  _ownDeviceFactory->forgetElement(videoOutElemPtr);
  emit videoSurfaceRemoved("video", sessionId);
}


#include "makneto.moc"