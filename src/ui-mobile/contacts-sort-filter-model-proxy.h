/* 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QSortFilterProxyModel>
#include "contacts-model-proxy.h"

#ifndef CONTACTSSORTFILTERMODELPROXY_H
#define	CONTACTSSORTFILTERMODELPROXY_H

class ContactsSortFilterModelProxy : public QSortFilterProxyModel {
  Q_OBJECT
  Q_DISABLE_COPY(ContactsSortFilterModelProxy)

  Q_PROPERTY(bool hideOffline READ hideOffline WRITE setHideOffline NOTIFY hideOfflineChanged)
  Q_PROPERTY(int contactCount READ contactCount NOTIFY contactCountChanged)
  Q_PROPERTY(QVariant contactStringFilter READ contactStringFilter WRITE setContactStringFilter)

public:
  ContactsSortFilterModelProxy(ContactsModelProxy *contactsModel, QObject *parent = 0);

  Q_INVOKABLE QVariant data(int row, int role); // ivokable, for access from QML    

  bool hideOffline();
  void setHideOffline(bool hide);
  int contactCount() const;
  void setContactStringFilter(QVariant filter);
  QVariant contactStringFilter();

Q_SIGNALS:
  void contactCountChanged();
  void hideOfflineChanged();

protected:
  virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
  virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

  private
Q_SLOTS:
  void rowCountChanged();
  //void rowCountChanged(const QModelIndex &parent, int first, int last);

private:
  bool _hideOffline;
  int _lastRowCount;
  QString _contactStringFilter;
  ContactsModelProxy *_contactsModel;
};

#endif	/* CONTACTSSORTFILTERMODELPROXY_H */

