/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QDebug>
#include <QMediaPlayer>
#include <QMediaPlaylist>

#include "sound-notificator.h"

SoundNotificator::SoundNotificator(QApplication* app, QObject *parent) : QObject(parent) {

  
  _ringer = new QMediaPlayer(this);  
  _ringerPlaylist = new QMediaPlaylist(this);
  _ringer->setPlaylist(_ringerPlaylist);
  _ringerPlaylist->addMedia(QUrl::fromLocalFile(app->applicationDirPath() + "/../share/sounds/makneto/telephonering.mp3"));
  _ringerPlaylist->setCurrentIndex(0);
  _ringerPlaylist->setPlaybackMode(QMediaPlaylist::Loop);  
  
  _beeper = new QMediaPlayer(this);
  _beeperPlaylist = new QMediaPlaylist(this);
  _beeper->setPlaylist(_beeperPlaylist);
  _beeperPlaylist->addMedia(QUrl::fromLocalFile(app->applicationDirPath() + "/../share/sounds/makneto/telephonebeep.mp3"));
  _beeperPlaylist->setCurrentIndex(0);
  _beeperPlaylist->setPlaybackMode(QMediaPlaylist::Loop);
  
  _dialer = new QMediaPlayer(this);
  _dialerPlaylist = new QMediaPlaylist(this);
  _dialer->setPlaylist(_dialerPlaylist);
  _dialerPlaylist->addMedia(QUrl::fromLocalFile(app->applicationDirPath() + "/../share/sounds/makneto/telephonedialing.mp3"));
  _dialerPlaylist->setCurrentIndex(0);
  _dialerPlaylist->setPlaybackMode(QMediaPlaylist::Loop); 
}

SoundNotificator::~SoundNotificator() {
  if (_ringer)
    _ringer->deleteLater();
  if (_beeper)
    _beeper->deleteLater();
  if (_dialer)
    _dialer->deleteLater();
  if (_beeperPlaylist)
    _beeperPlaylist->deleteLater();
  if (_ringerPlaylist)
    _ringerPlaylist->deleteLater();
  if (_dialerPlaylist)
    _dialerPlaylist->deleteLater();
}

void SoundNotificator::onStartBeeping() {
  qDebug() << "SoundNotificator: start beeping";
  _beeper->play();
}

void SoundNotificator::onStopBeeping() {
  qDebug() << "SoundNotificator: stop beeping";
  _beeper->stop();
}

void SoundNotificator::onStartRinging() {
  qDebug() << "SoundNotificator: start ringing";
  _ringer->play();
}

void SoundNotificator::onStopRinging() {
  qDebug() << "SoundNotificator: stop ringing";
  _ringer->stop();
}

void SoundNotificator::onStartDialing() {
  qDebug() << "SoundNotificator: start dialing";
  _dialer->play();
}

void SoundNotificator::onStopDialing() {
  qDebug() << "SoundNotificator: stop dialing";
  _dialer->stop();
}

#include "sound-notificator.moc"
