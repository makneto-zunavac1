/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include "../libnotify-qt/Notification.h"

#include "notifications-model.h"
#include "session-model.h"

NotificationsModel::NotificationsModel(MaknetoBackend::TelepathyClient* client, MainWindow *window, QObject *parent) :
QAbstractListModel(parent),
_client(client),
lastId(0),
_window(window) {

  if (!Notification::isInitted()) {
    if (Notification::init("makneto-mobile")) {
      qDebug() << "NotificationsModel: init notifications";
      QStringList caps = Notification::getServerCaps();
      if (!caps.contains("actions")) {
        qWarning() << "NotificationsModel: notification server doesn't support actions";
      }
    } else {
      qWarning() << "NotificationsModel: could not init Freedesktop Notification API";
    }
  }

  QHash<int, QByteArray> roles;

  // general roles
  roles[IdRole] = "id";
  roles[TitleRole] = "title";
  roles[SessionIdRole] = "sessionId";
  roles[SessionNameRole] = "sessionName";
  roles[TypeRole] = "type";
  roles[IconRole] = "icon";
  roles[MessageRole] = "message";

  setRoleNames(roles);
}

void NotificationsModel::addNotification(MaknetoBackend::Session *session, NotificationItem::NotificationType type, QString msg) {

  emit beginInsertRows(QModelIndex(), _items.size(), _items.size());
  NotificationItem *item = new NotificationItem(lastId++, session, type, msg);

  connect(item, SIGNAL(acceptNotification(int)), this, SLOT(acceptNotification(int)));
  connect(item, SIGNAL(declineNotification(int)), this, SLOT(declineNotification(int)));
  connect(item, SIGNAL(notificationExpired(int)), this, SLOT(onNotificationExpired(int)));

  _items.push_back(item);
  _itemsMap.insert(item->getId(), item);
  emit endInsertRows();

  //emit sessionCreated(session->getUniqueName(), extendsExisting, requested);
}

NotificationsModel::~NotificationsModel() {
  if (Notification::isInitted()) {
    qDebug() << "NotificationsModel: de-init notifications";
    Notification::uninit();
  }
}

bool NotificationsModel::containsNotification(const QString &sessionId, NotificationItem::NotificationType type) {
  QList<NotificationItem *>::iterator i = _items.begin();
  while (i != _items.end()) {
    if ((*i)->getSession()->getUniqueName() == sessionId && (*i)->getType() == type)
      return true;
    ++i;
  }
  return false;
}

int NotificationsModel::rowCount(const QModelIndex& index) const {
  return _items.size();
}

QVariant NotificationsModel::data(int row, int role) {
  return data(index(row, 0, QModelIndex()), role);
}

QVariant NotificationsModel::data(const QModelIndex& index, int role) const {
  if (index.column() != 0)
    return QVariant();

  NotificationItem *item = _items.at(index.row());
  if (!item)
    return QVariant();

  return item->data(role);
}

void NotificationsModel::acceptNotification(int id) {
  NotificationItem *item = _itemsMap[id];
  if (!item)
    return;

  if (_window)
    _window->setVisibleAndActivate();

  if (item->getType() == NotificationItem::ChatType) {
    emit acceptChat(item->getSession()->getUniqueName());
  }
  if (item->getType() == NotificationItem::AudioCallType || item->getType() == NotificationItem::VideoCallType) {
    emit acceptCall(item->getSession()->getUniqueName());
  }
  removeNotification(item, id);
}

void NotificationsModel::declineNotification(int id) {
  NotificationItem *item = _itemsMap[id];
  if (!item)
    return;

  if (item->getType() == NotificationItem::ChatType) {
    emit rejectChat(item->getSession()->getUniqueName());
  }
  if (item->getType() == NotificationItem::AudioCallType || item->getType() == NotificationItem::VideoCallType) {
    emit rejectCall(item->getSession()->getUniqueName());
  }
  removeNotification(item, id);
}

void NotificationsModel::onNotificationExpired(int id) {
  declineNotification(id);
}

void NotificationsModel::removeNotification(NotificationItem *item, int id) {
  int row = 0;
  for (; row < _items.size(); row++) {
    if (_items.at(row) == item)
      break;
  }
  emit beginRemoveRows(QModelIndex(), row, row);
  _items.removeOne(item);
  _itemsMap.remove(id);
  emit endRemoveRows();
  emit notificationRemoved(item->getSession()->getUniqueName(), item->getType());
  item->deleteLater();
}

#include "notifications-model.moc"
