/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QAbstractListModel>

#include "../backend/telepathy-client.h"

#include "session-model-item.h"
#include "notification-item.h"
#include "main-window.h"

#ifndef NOTIFICATIONSMODEL_H
#define	NOTIFICATIONSMODEL_H

class NotificationsModel : public QAbstractListModel {

  Q_OBJECT
  Q_DISABLE_COPY(NotificationsModel)
  Q_ENUMS(Role)

public:
  enum Role {
    // general roles
    IdRole = Qt::UserRole,
    TitleRole,
    SessionIdRole,
    SessionNameRole,
    MessageRole,
    IconRole,
    TypeRole
  };

  NotificationsModel(MaknetoBackend::TelepathyClient* client, MainWindow *window, QObject *parent = 0);
  virtual ~NotificationsModel();
  bool containsNotification(const QString &sessionId, NotificationItem::NotificationType type);

  Q_INVOKABLE QVariant data(int row, int role); // ivokable, for QML  

  virtual int rowCount(const QModelIndex& index) const;
  virtual QVariant data(const QModelIndex& index, int role) const;

Q_SIGNALS:
  void acceptCall(const QString &sessionId);
  void rejectCall(const QString &sessionId);
  void acceptChat(const QString &sessionId);
  void rejectChat(const QString &sessionId);
  void notificationRemoved(const QString &sessionId, NotificationItem::NotificationType type);

  public
Q_SLOTS:
  void addNotification(MaknetoBackend::Session *session, NotificationItem::NotificationType type, QString msg = QString());
  Q_INVOKABLE void acceptNotification(int id);
  Q_INVOKABLE void declineNotification(int id);
  void onNotificationExpired(int id);

private:
  void removeNotification(NotificationItem *item, int id);

  MaknetoBackend::TelepathyClient* _client;
  QList<NotificationItem*> _items;
  QMap<int, NotificationItem*> _itemsMap;
  int lastId;
  MainWindow *_window;
};

#endif	/* NOTIFICATIONSMODEL_H */

