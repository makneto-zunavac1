/*
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QtCore/QObject>
#include <QAbstractListModel>

#include <TelepathyQt4/Account>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/TextChannel>
#include <TelepathyQt4/Types>

#include "../backend/telepathy-client.h"
#include "../backend/session.h"

#include "session-model-item.h"
#include "notifications-model.h"

#ifndef SESSIONMODEL_H
#define	SESSIONMODEL_H

class SessionModel : public QAbstractListModel {

  Q_OBJECT
  Q_DISABLE_COPY(SessionModel)
  Q_ENUMS(Role)

  Q_PROPERTY(int sessionCount READ sessionCount)

public:
  enum Role {
    // general roles
    ItemRole = Qt::UserRole,
    IdRole,
    IconRole,
    NameRole,
    TypeRole,
    LastMessageRole
  };

  SessionModel(MaknetoBackend::TelepathyClient* client, NotificationsModel* notificationsModel, QObject *parent = 0);
  virtual ~SessionModel();

  int sessionCount() const;
  // ivokable, for QML  
  Q_INVOKABLE QVariant data(int row, int role);
  Q_INVOKABLE int getSessionType(const QString &sessionId);

  // virtual methods from QAbstractItemModel
  virtual int rowCount(const QModelIndex& index) const;
  virtual QVariant data(const QModelIndex& index, int role) const;

  public
Q_SLOTS:
  void onSessionCreated(MaknetoBackend::Session *, bool extendsExisting, bool requested);
  void onMessageEnqueued(const QString &);
  void onSendingError(const QString &sessionId, const QString &message, const QString &errorMessage);

  void onIncomingCall(const QString &);
  void onCallError(const QString &, const QString &);
  void onCallEnded(const QString &);
  void onCallReady(const QString &sessionId);  
  void onRemoteAudioStreamAdded(const QString &sessionId);

  // slots for signals from QML
  Q_INVOKABLE void sendTextMessage(const QString &sessionId, const QString &text);
  Q_INVOKABLE void sendWhiteboardMessage(const QString &sessionId, const QString &content);
  Q_INVOKABLE void changeChatState(const QString &sessionId, int state);
  Q_INVOKABLE void requestSession(const QString &accountId, const QString &contactId, int intType);
  Q_INVOKABLE void closeSession(const QString &sessionId);
  Q_INVOKABLE void startCall(const QString &sessionId, bool video);
  Q_INVOKABLE void hangup(const QString &sessionId);

  void onAcceptCall(const QString &sessionId);
  void onRejectCall(const QString &sessionId);
  void onAcceptChat(const QString &sessionId);
  void onRejectChat(const QString &sessionId);
  void onNotificationRemoved(const QString &sessionId, NotificationItem::NotificationType type);
  void onSessionTypeChanged(const QString &, int);
  void onTextMessageReceived(const QString &, const QString &, const QString &);
  
Q_SIGNALS:
  void sessionOpened(QString sessionId, bool updatedeExisting, bool requested);
  void sessionClosed(QString sessionId);
  void whiteboardMessageReceived(const QString &sessionId, const QString &text, const QString &contact);
  void textMessageReceived(const QString &sessionId, const QString &text, const QString &contact);
  void sessionTypeChanged(const QString &sessionId, int types);
  void messageSent(const QString &sessionId, const QString &message);
  void sendingError(const QString &sessionId, const QString &message, const QString &errorMessage);
  void chatStateChanged(const QString &sessionId, int chatState, const QString &contact);

  void callReady(const QString &);
  void callEnded(const QString &);

  void videoPreviewAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoAvailable(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoPreviewRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  void videoRemoved(const QString &sessionId, QGst::ElementPtr videoOutElemPtr);
  
  void startBeeping();
  void stopBeeping();  
  void startRinging();
  void stopRinging();
  void startDialing();
  void stopDialing();  

private:
  void stopBeeping(const QString &sessionId);
  void stopRinging(const QString &sessionId);
  void stopDialing(const QString &sessionId);
  
  virtual void openSession(SessionModelItem *);

  MaknetoBackend::TelepathyClient* _client;
  QList<SessionModelItem*> _items; // only visible sessions
  QMap<QString, SessionModelItem*> _itemsMap; // all sessions by ID
  QSet<QString> _beepingSessions;
  QSet<QString> _ringingSessions;
  QSet<QString> _dialingSessions;
  NotificationsModel *_notificationsModel;
};

#endif	/* SESSIONMODEL_H */

