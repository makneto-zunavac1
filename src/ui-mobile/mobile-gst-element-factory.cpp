/*
 * This file is based on Collabora example (QtGst-QmlSink)
 * 
 *   Copyright (C) 2011 Lukáš Karas <lukas.karas@centrum.cz>
 *                                     
 *   This program is free software; you can redistribute it and/or modify  
 *   it under the terms of the GNU General Public License as published by  
 *   the Free Software Foundation; either version 2 of the License, or     
 *   (at your option) any later version.                                   
 *                                                                         
 *   This program is distributed in the hope that it will be useful,       
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         
 *   GNU General Public License for more details.                          
 *                                                                         
 *   You should have received a copy of the GNU General Public License     
 *   along with this program; if not, write to the                         
 *   Free Software Foundation, Inc.,                                       
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             
 */

#include <QGst/Bin>
#include <QGst/ElementFactory>

#include <QtGui>
#include <QtDeclarative>
#include <QtOpenGL/QGLWidget>
#include <gst/gst.h>
#include <QtGstQmlSink/qmlvideosurfacegstsink.h>
#include <QtGstQmlSink/qmlgstvideoitem.h>

#include "mobile-gst-element-factory.h"

MobileGstElementFactory::MobileGstElementFactory(QDeclarativeView *qmlView) : _qmlView(qmlView) {
  
  g = new QGLWidget();
  _qmlView->setViewport(g);

}

MobileGstElementFactory::~MobileGstElementFactory() {
  if (g)
    g->deleteLater();
}

QGst::ElementPtr MobileGstElementFactory::makeVideoOutputElement() {
  qDebug() << "MobileGstElementFactory: creating QmlPainterVideoSurface for GST";

  // both the video sink and the video item need access to a
  // QmlPainterVideoSurface, which does accelerated drawing of video
  // frames
  QmlPainterVideoSurface *surface = new QmlPainterVideoSurface(this);
  surface->setGLContext((QGLContext *) g->context());
  
  //surface->moveToThread(new QThread(this));
  
  connect(surface, SIGNAL(frameChanged()), SLOT(onFrameChanged()));

  GstElement *vsnk = GST_ELEMENT(QmlVideoSurfaceGstSink::createSink(surface));
  QGst::ElementPtr ptr = QGst::ElementPtr::wrap(vsnk, true);

  _surfacesMap.insert(ptr, surface);
 // _glMap.insert(ptr, g);

  return ptr;
}

/**
 * just for debug...
 */
void MobileGstElementFactory::onFrameChanged() {
  //qDebug() << "MobileGstElementFactory: frame changed";
}

QmlPainterVideoSurface *MobileGstElementFactory::mapElementToSurface(QGst::ElementPtr element) {
  return _surfacesMap[ element ];
}

bool MobileGstElementFactory::forgetElement(QGst::ElementPtr element) {
  // FIXME: is safe remove surface and gl widget?
  QmlPainterVideoSurface *surface = _surfacesMap[ element ];
  if (surface)
    surface->deleteLater();
  return _surfacesMap.remove(element);
}

#include "mobile-gst-element-factory.moc"
