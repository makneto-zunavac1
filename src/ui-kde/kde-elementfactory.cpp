/* 
 * File:   KdeElementFactory.cpp
 * Author: karry
 * 
 * Created on 30. říjen 2011, 17:37
 */

#include "kde-elementfactory.h"
#include "phononintegration_p.h"
#include <phonon/ObjectDescription>
#include <QGst/Bin>
#include <QGst/ElementFactory>

#include <KDebug>
#include <KConfig>
#include <KConfigGroup>
#include <KGlobal>

 /**
   * for testing, try command 
   *    gst-launch autoaudiosrc ! audioconvert ! audioresample ! alsasink 
   *                                           ! lame bitrate=192 ! filesink location=test.mp3
   * 
   * @return 
   */
  QGst::ElementPtr KdeElementFactory::makeAudioCaptureElement() {
    QGst::ElementPtr element;

    /*
    // UNCOMENT FOR DEBUG ... test device (1kHz sinus)
    element = tryElement("audiotestsrc");
    if (element) {
      return element;
    }else{
      qWarning() << "audiotestsrc failed!";
    }
     */

    //allow overrides from the application's configuration file
    element = tryOverrideForKey("audiosrc");
    if (element) {
      return element;
    }

    //use gconf on non-kde environments
    if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
      element = tryElement("gconfaudiosrc");
      if (element)
        return element;
    }

    //for kde environments try to do what phonon does:
    //first try pulseaudio,
    element = tryElement("pulsesrc");
    if (element) {
      return element;
    }

    //then try alsa/oss devices reported by phononserver
    //in the order that they are configured in phonon
    QList<Phonon::DeviceAccessList> phononDeviceLists
      = PhononIntegration::readDevices(Phonon::AudioCaptureDeviceType, Phonon::CommunicationCategory);

    Q_FOREACH(const Phonon::DeviceAccessList & deviceList, phononDeviceLists) {

      Q_FOREACH(const Phonon::DeviceAccess & device, deviceList) {
        if (device.first == "alsa") {
          //skip x-phonon devices, we will use plughw which is always second in the list
          if (!device.second.startsWith("x-phonon")) {
            element = tryElement("alsasrc", device.second);
          }
        } else if (device.first == "oss") {
          element = tryElement("osssrc", device.second);
        }

        if (element) {
          return element;
        }
      }
    }

    //as a last resort, try gstreamer's autodetection
    element = tryElement("autoaudiosrc");
    return element;
  }

  QGst::ElementPtr KdeElementFactory::makeAudioOutputElement() {
    QGst::ElementPtr element;

    /*
    // UNCOMENT FOR DEBUG ... test output to file
      element = tryFileElement("filesink", "sink.out");
      if (element) {
        return element;
      }
     */


    //allow overrides from the application's configuration file
    element = tryOverrideForKey("audiosink");
    if (element) {
      return element;
    }

    //use gconf on non-kde environments
    if (qgetenv("KDE_FULL_SESSION").isEmpty()) {
      element = tryElement("gconfaudiosink");
      if (element) {
        element->setProperty("profile", 2 /*chat*/);
        return element;
      }
    }

    //for kde environments try to do what phonon does:
    //first try pulseaudio,
    element = tryElement("pulsesink");
    if (element) {
      //TODO set category somehow...
      return element;
    }

    //then try alsa/oss devices reported by phononserver
    //in the order that they are configured in phonon
    QList<Phonon::DeviceAccessList> phononDeviceLists
      = PhononIntegration::readDevices(Phonon::AudioOutputDeviceType, Phonon::CommunicationCategory);

    Q_FOREACH(const Phonon::DeviceAccessList & deviceList, phononDeviceLists) {

      Q_FOREACH(const Phonon::DeviceAccess & device, deviceList) {
        if (device.first == "alsa") {
          //use dmix instead of x-phonon, since we don't have phonon's alsa configuration file
          QString deviceString = device.second;
          deviceString.replace("x-phonon", "dmix");
          element = tryElement("alsasink", deviceString);
        } else if (device.first == "oss") {
          element = tryElement("osssink", device.second);
        }

        if (element) {
          return element;
        }
      }
    }

    //as a last resort, try gstreamer's autodetection
    element = tryElement("autoaudiosink");
    return element;
  }


QGst::ElementPtr KdeElementFactory::tryOverrideForKey(const char *keyName) {
  QGst::ElementPtr element;
  const KConfigGroup configGroup = KGlobal::config()->group("GStreamer");

  if (configGroup.hasKey(keyName)) {
    QString binDescription = configGroup.readEntry(keyName);
    element = QGst::Bin::fromDescription(binDescription);

    if (!element) {
      qDebug() << "Could not construct bin" << binDescription;
      return element;
    }

    if (!element->setState(QGst::StateReady)) {
      qDebug() << "Custom bin" << binDescription << "doesn't want to become ready";
      return QGst::ElementPtr();
    }

    qDebug() << "Using custom bin" << binDescription;
  }

  return element;
}
