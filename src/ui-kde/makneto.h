/*
 * makneto.h
 *
 * Copyright (C) 2008 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#ifndef MAKNETO_H
#define MAKNETO_H

#include "maknetomainwindow.h"
//#include "featurelist.h"

#include <QObject>

namespace MaknetoBackend {
	
	class TelepathyClient;
	class Session;
}

/**
 * This is Makneto class - central class of Makneto suite
 *
 * @short Makneto
 * @author Jaroslav Reznik <rezzabuh@gmail.com>
 * @version 0.1
 */

//class MaknetoContactList;
class MaknetoMainWindow;

class Makneto : public QObject
{
	Q_OBJECT

public:
	
    static Makneto* Instance();
	virtual ~Makneto();
	
	MaknetoMainWindow *getMaknetoMainWindow() { return m_mainwindow; }

public Q_SLOTS:
//	void contactNewSession(QAction *action);							//FIXME
//	void contactDetails(QAction *action);								//FIXME
//	void addUser(const XMPP::Jid &, const QString &, bool requestAuth);	//FIXME
  
	void setMaknetoMainWindow(MaknetoMainWindow *mainwindow) { m_mainwindow = mainwindow; }
    
	//vtheman
	void onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient *);
	
Q_SIGNALS:
	//void newSession(const QString &text, ChatType type, const QString &nick = QString());	//FIXME
	
	//vtheman
	void clientInitialised(MaknetoBackend::TelepathyClient *);
  
private:

	Makneto(QObject *parent=0);
	Q_DISABLE_COPY(Makneto);
	
	MaknetoMainWindow *m_mainwindow;
  
	MaknetoBackend::TelepathyClient *m_client;
	static Makneto *m_instance; 
};

#endif // MAKNETO_H
