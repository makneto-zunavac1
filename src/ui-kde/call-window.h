#include <QMainWindow>

class QAction;
class QLabel;

namespace MaknetoBackend{
	
	class Session;
}

class CallWindow : public QMainWindow {

	Q_OBJECT
	
public: 
    CallWindow(MaknetoBackend::Session *,QWidget* parent = 0);
	
Q_SIGNALS:
	void hangup();
	
public Q_SLOTS:
	void onCallEnded();
	void onCallReady();
	void onHangup();
	
private:
	QAction *m_hangupAction;
	QLabel *m_statusLabel;
	MaknetoBackend::Session *m_session;
};
