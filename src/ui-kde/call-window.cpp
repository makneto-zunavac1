#include <QAction>
#include <KIcon>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QToolButton>

#include "call-window.h"
#include "../backend/session.h"

CallWindow::CallWindow(MaknetoBackend::Session *session, QWidget* parent)
	: 	QMainWindow(parent),
		m_session(session)
{
	qDebug() << "CallWindow: Accepting incoming call";
	
	m_session->acceptCall();
	
//	QVBoxLayout *mainLayout = new QVBoxLayout();
//	QHBoxLayout *volumeLayout = new QHBoxLayout();
	setWindowTitle(QString("call with " + m_session->getName()));

	m_hangupAction = new QAction( KIcon("makneto-hang-up.png"), "Hang up", this);
	QToolButton *hangupButton = new QToolButton( this);
	setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	hangupButton->setDefaultAction(m_hangupAction);
	
	connect(hangupButton, SIGNAL(triggered(QAction*)),
			this, SLOT(onHangup()));
	
	connect(m_session, SIGNAL(callEnded(const QString &)),
			this, SLOT(onCallEnded()));
	
	connect(m_session, SIGNAL(callReady()),
			this, SLOT(onCallReady()));

	setFixedSize(250,150);
	show();
}

void CallWindow::onCallEnded()
{
	setStatusTip("Call has ended");
	hide();
	deleteLater();
}

void CallWindow::onCallReady()
{
	qDebug("Callwindow: Call is ready");
	//TODO figure out what else to do	
}

void CallWindow::onHangup()
{
	m_session->onHangup();
	//TODO figure out what else to do
	hide();
	deleteLater();
}


