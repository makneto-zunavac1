/*
 * Inspired by examples from Telepathy-Qt4 by Collabora.co.uk
 * Copyright (C) 2011 Vojta Kulicka <vojtechkulicka@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QObject>

#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/PendingReady>
#include <TelepathyQt4/AccountSet>

#include "../backend/flat-model-proxy.h"
#include "../backend/telepathy-client.h"
#include "../backend/accounts-model-item.h"
#include "../backend/accounts-model.h"
//#include <TelepathyQt4/Models/AvatarImageProvider>

#include <QAbstractItemModel>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTreeView>
#include <QAction>
#include <QPoint>
#include <QMenu>
#include <QInputDialog>
#include <qt4/QtGui/qmessagebox.h>

#include "roster-widget.h"
#include "account-presence-button.h"
#include "makneto.h"
#include "maknetoview.h"
#include "sessiontabmanager.h"

RosterWidget::RosterWidget(QWidget *parent)
: QWidget(parent) {
  QVBoxLayout *layout = new QVBoxLayout(this);
  m_hlayout = new QHBoxLayout(this);

  m_view = new QTreeView(this);
  m_view->setIndentation(5);
  m_view->setUniformRowHeights(true);
  m_view->alternatingRowColors();
  m_view->setHeaderHidden(true);

  layout->addLayout(m_hlayout);
  layout->addWidget(m_view);

  setLayout(layout);
  show();
  //  mView->engine()->addImageProvider(
  //  QString::fromLatin1("avatars"),
  //  new Tp::AvatarImageProvider(am));
}

void RosterWidget::onInitializationFinished(MaknetoBackend::TelepathyClient *client) {
  //QAbstractItemModel *contactModel =	new MaknetoLib::FlatModelProxy(client->accountsModel());

  MaknetoBackend::AccountsModel* accountsModel = client->accountsModel();
  m_view->setModel(accountsModel);
  m_view->setContextMenuPolicy(Qt::CustomContextMenu);

  //send a signal to sessionTabManager about new session request
  connect(this, SIGNAL(textChatRequested(QModelIndex)),
    Makneto::Instance()->getMaknetoMainWindow()->getMaknetoView()->getSessionTabManager(), //sessionTabManager
    SLOT(onTextChatRequested(QModelIndex)));

  //neccesary for context menu to work
  connect(m_view, SIGNAL(customContextMenuRequested(QPoint)),
    this, SLOT(onCustomContextMenuRequested(QPoint)));

  connect(accountsModel, SIGNAL(accountConnectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)),
    this, SLOT(onAccountStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)));
  
  qDebug() << "Telepathy initialization has finished";
  //qDebug() << "Available accounts by ID: ";
  /*	foreach(Tp::AccountPtr account, client->accountManager()->enabledAccounts()->accounts()){
      qDebug() << account->uniqueIdentifier();
      m_hlayout->addWidget(new AccountPresenceButton(account,this));
    }*/
}

void RosterWidget::onStartTextChat() {
  qDebug() << "RosterWidget: on start text chat";

  emit textChatRequested(m_view->currentIndex());
}

void RosterWidget::onAccountStatusChanged(const Tp::AccountPtr &account, Tp::ConnectionStatus status){
  qDebug() << "on account status changed (" << account->uniqueIdentifier() << ", " << status << ")";
  
  if (status == Tp::ConnectionStatusDisconnected) {
    QString connectionError = account->connectionError();

    // ignore user disconnect
    if (connectionError != "org.freedesktop.Telepathy.Error.Cancelled") {
      handleConnectionError(account, account->connectionStatusReason());
    }
  }
}

void RosterWidget::handleConnectionError(const Tp::AccountPtr &account, Tp::ConnectionStatusReason reason){
  if (!account->isEnabled()){
    QMessageBox::critical(this, tr("Error"), tr("Could not connect %1. Account is disabled!").arg( account->displayName()) );    
  }else if (reason == Tp::ConnectionStatusReasonAuthenticationFailed) {
    QMessageBox::critical(this, tr("Error"), tr("Could not connect %1. Authentication failed (is your password correct?)").arg( account->displayName()) );
  } else if (reason == Tp::ConnectionStatusReasonNetworkError) {
    QMessageBox::critical(this, tr("Error"), tr("Could not connect %1. There was a network error, check your connection").arg( account->displayName()) );
  } else {
    // other errors
    QMessageBox::critical(this, tr("Error"), tr("An unexpected error has occurred with %1: '%2'").arg( account->displayName()).arg(account->connectionError()) );
  }
}

void RosterWidget::onStartAudioChat() {
  // FIXME: implement this
  qDebug() << "on start audio chat";
}

void RosterWidget::onStartVideoChat(){
  // FIXME: implement this
  qDebug() << "on start video chat";
}

void RosterWidget::onCustomContextMenuRequested(QPoint point) {
  Q_UNUSED(point);

  QModelIndex index = m_view->currentIndex();

  QScopedPointer<QMenu> contextMenu(new QMenu());
  QMenu *presenceSubMenu;
  QAction *action;

  //context menu for an account
  if (index.data(MaknetoBackend::AccountsModel::AliasRole) == QVariant()) {

    connect(contextMenu.data(), SIGNAL(triggered(QAction *)),
      this, SLOT(onChangePresence(QAction *)));


    contextMenu->setTitle(index.data(MaknetoBackend::AccountsModel::DisplayNameRole).toString());
    presenceSubMenu = contextMenu->addMenu("Set presence");
    action = presenceSubMenu->addAction("Available");
    action->setIcon(KIcon("maknetoonline"));
    action->setData(MaknetoBackend::TelepathyClient::Available);

    action = presenceSubMenu->addAction("Away");
    action->setIcon(KIcon("maknetoaway"));
    action->setData(MaknetoBackend::TelepathyClient::Away);

    action = presenceSubMenu->addAction("Busy");
    action->setIcon(KIcon("maknetodnd"));
    action->setData(MaknetoBackend::TelepathyClient::Busy);

    action = presenceSubMenu->addAction("Invisible");
    action->setIcon(KIcon("maknetoinvisible"));
    action->setData(MaknetoBackend::TelepathyClient::Invisible);

    action = presenceSubMenu->addAction("Offline");
    action->setIcon(KIcon("maknetooffline"));
    action->setData(MaknetoBackend::TelepathyClient::Offline);

    m_actionMUC = contextMenu->addAction("Start/Join MUC");

  }//context menu for contact
  else {
    contextMenu->setTitle(index.data(MaknetoBackend::AccountsModel::AliasRole).toString());
    action = contextMenu->addAction("Text Chat");
    connect(action, SIGNAL(triggered(bool)),
      this, SLOT(onStartTextChat()));
    action = contextMenu->addAction("Start Audio Chat");
    connect(action, SIGNAL(triggered(bool)),
      this, SLOT(onStartAudioChat()));
    action = contextMenu->addAction("Start Video Chat");
    connect(action, SIGNAL(triggered(bool)),
      this, SLOT(onStartVideoChat()));
  }


  //TODO figure out why the damn title does not show!!!!
  contextMenu->exec(QCursor::pos());
}

void RosterWidget::onChangePresence(QAction *action) {
  bool ok;
  //start MUC
  if (action == m_actionMUC) {

    QString chatRoomName = QInputDialog::getText(this, tr("QInputDialog::getText()"),
      tr("Chatroom name:"), QLineEdit::Normal,
      tr(""), &ok);
    if (ok)
      MaknetoBackend::TelepathyClient::Instance()->onNewTextChatRoomRequested(m_view->currentIndex(), chatRoomName);
  }//set presence
  else {
    QString statusMsg = QString();
    if (action->data() != MaknetoBackend::TelepathyClient::Offline) {
      statusMsg = QInputDialog::getText(this, tr("QInputDialog::getText()"),
        tr("Status message:"), QLineEdit::Normal,
        tr(""), &ok);
    }
    MaknetoBackend::TelepathyClient::Instance()->setAccountPresence(m_view->currentIndex(), action->data().toInt(), statusMsg);
  }
}

