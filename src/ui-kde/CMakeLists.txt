cmake_minimum_required(VERSION 2.6)

project(makneto-kde)

SET(CMAKE_BUILD_TYPE Debug)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_HOME_DIRECTORY}/CMakeMod")
list(APPEND CMAKE_MODULE_PATH "../../CMakeMod")

find_package(Qt4 COMPONENTS QtGui QtCore REQUIRED)
find_package(QtGStreamer REQUIRED)
find_package(TelepathyQt4 REQUIRED)
find_package(TelepathyQt4Farsight REQUIRED)
find_package(TpFarsight REQUIRED)
find_package(Farsight2 REQUIRED)

set(KDE4_FIND_QUIETLY false)
find_package(KDE4 REQUIRED)
find_package(QCA2 REQUIRED)

include(${QT_USE_FILE})
include (KDE4Defaults)

message(STATUS "  Module path: ${CMAKE_MODULE_PATH}")

include_directories(${KDE4_INCLUDES} 
					${KDE4_PHONON_INCLUDES}  
					${KDE4_KDECORE_INCLUDES} 
					${QT_INCLUDES} 
					${QCA2_INCLUDE_DIR} 
					${TELEPATHY_QT4_INCLUDE_DIR}
					${TPFARSIGHT_INCLUDE_DIR}
					${QTGSTREAMER_INCLUDE_DIR}
			
)

set(makneto_kde_SRCS 
	main.cpp
	maknetomainwindow.cpp
	maknetoview.cpp
	sidebarwidget.cpp
	makneto.cpp
	wbwidget.cpp
	wbscene.cpp
	wbitems.cpp
	sessionview.cpp
	sessiontabmanager.cpp
	maknetohtmlbrowser.cpp
	mediaplayer.cpp
	addcontactdialog.cpp
	contactdetaildialog.cpp
	chatinput.cpp
	chatoutput.cpp
	palettewidget.cpp
	wbforeign.cpp
	qgraphicswidgetitem.cpp

#plugins
	plugins/plugin.cpp
	plugins/pollplugin.cpp
	plugins/pollplugincreate.cpp

	call-window.cpp
	roster-widget.cpp
	account-presence-button.cpp
	kde-elementfactory.cpp
	phononintegration.cpp
)

# enable warnings
ADD_DEFINITIONS( -Wall )

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${QTGSTREAMER_FLAGS} ${KDE4_ENABLE_EXCEPTIONS}")

kde4_add_ui_files(	makneto_kde_SRCS 
					maknetoview_base.ui 
					prefs_base.ui 
					add_contact.ui 
					contact_detail.ui)

kde4_add_kcfg_files(makneto_kde_SRCS 
					settings.kcfgc )

kde4_add_executable(makneto-kde
					${makneto_kde_SRCS})

SET_TARGET_PROPERTIES(makneto-kde PROPERTIES COMPILE_FLAGS "-g")

# Make sure the linker can find the makneto library once it is built. 
link_directories (${makneto_BINARY_DIR})

target_link_libraries(	makneto-kde 
						${KDE4_KDEUI_LIBS} 
						${QT_LIBRARIES} 
						${KDE4_KHTML_LIBS} 
						${KDE4_PHONON_LIBS}
						${TELEPATHY_QT4_FARSIGHT_LIBRARIES}
                        ${TPFARSIGHT_LIBRARIES} 
						${FARSIGHT2_LIBRARIES} 
						${QTGSTREAMER_UI_LIBRARIES}
 						${QCA2_LIBRARIES} 
						${TELEPATHY_QT4_LIBRARIES}
						makneto
						)
						
#install(TARGETS makneto DESTINATION ${BIN_INSTALL_DIR} )
install(TARGETS makneto-kde ${INSTALL_TARGETS_DEFAULT_ARGS} )

message(SYSTEM " DATA INSTALL DIR ${DATA_INSTALL_DIR}")

########### install files ###############
install( FILES makneto.desktop  DESTINATION  ${XDG_APPS_INSTALL_DIR} )
install( FILES makneto.kcfg  DESTINATION  ${KCFG_INSTALL_DIR} )
install( FILES maknetoui.rc  DESTINATION  ${DATA_INSTALL_DIR}/makneto )
install( FILES ../backend/voip/codec-preferences.ini DESTINATION ${DATA_INSTALL_DIR}/makneto)

add_subdirectory( icons )
