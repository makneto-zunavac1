/*
 * sessionview.h
 *
 * Copyright (C) 2008 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#ifndef SESSIONVIEW_H
#define SESSIONVIEW_H

#include <QtGui/QMainWindow>
#include <QtGui/QWidget>
#include <QtCore/QByteArray>

#include <QGst/Ui/VideoWidget>

#include "mediaplayer.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QPushButton;
class QTextEdit;
class WbWidget;
class QSplitter;
class QDomElement;
class KToolBar;
class QBuffer;
class QFrame;
class FTStream;
class Makneto;
class PaletteWidget;
class ChatOutput;


#include "settings.h"
#include "wbwidget.h"
#include "../backend/session.h"

class CallWindow;

namespace Phonon {
  class ByteStream;
  class VideoPlayer;
}

/**
 * This is session view widget for Makneto
 *
 * @short Session view widget
 * @author Jaroslav Reznik <rezzabuh@gmail.com>
 * @version 0.1
 */

class SessionView : public QWidget {
  Q_OBJECT
public:
  /**
   * Default constructor
   * @param type Type of the session 0 = Chat, 1 = MUC
   */
  SessionView(QWidget *parent, const QString &jid, const int id, int type = 0, const QString &nick = QString());

  //vtheman
  SessionView(QWidget *parent, MaknetoBackend::Session *session);

  /**
   * Destructor
   */
  virtual ~SessionView();

  QString sessionId() {
    return m_session->getUniqueName();
  }

  void createToolBar();
  void infoMessage(const QString &text);

  //MUCControl *getMUCControl(void) { return m_muccontrol; }
  void showHideChat();
  bool closeRequest();
  void setEnabled(bool enabled);

  public
Q_SLOTS:
  void sendClicked();
  void onMessageReceived(const QString &text, const QString &contact);
  void sendWhiteboard(const QDomElement &wb);
  void setMode(QAction *);
  //void transferRead(const QByteArray &a);
  void actionSendFileTriggered();
  void actionCreatePollTriggered();
  void fgColorChanged(const QColor &c);
  void bgColorChanged(const QColor &c);
  void penSizeChanged(int size);
  void modeChanged(WbWidget::Mode);
  //vtheman
  void onIncomingCall();
  void onCallReady();
  void onCallRequested();
  void onVideoCallRequested();
  //void onTextChatReady(QStringList *);
  void onIncomingFileTransfer(const QString &filename);

  // video calls...
  void onVideoPreviewAvailable(QGst::ElementPtr videoOutElemPtr);
  void onVideoAvailable(QGst::ElementPtr videoOutElemPtr);
  void onVideoPreviewRemoved(QGst::ElementPtr videoOutElemPtr);
  void onVideoRemoved(QGst::ElementPtr videoOutElemPtr);

Q_SIGNALS:
  void sendMessage(const QString &); //vtheman

private:
  bool acceptCallQuery();

  //vtheman
  MaknetoBackend::Session *m_session;
  CallWindow *m_callWindow;

  QMainWindow *m_videoWindow;
  QMainWindow *m_videoPreviewWindow;

  Makneto *m_makneto;
  QVBoxLayout *m_topLayout;
  QWidget *m_leftWidget, *m_chatWidget;
  PaletteWidget *m_paletteWidget;
  QVBoxLayout *m_leftLayout;
  QSplitter *m_leftSplitter;
  QSplitter *m_topSplitter;
  QStringList messages;

  QVBoxLayout *m_mainlayout;
  QVBoxLayout *m_chatlayout;
  QHBoxLayout *m_bottomlayout;

  KToolBar *m_wbtoolbar;

  QSplitter *m_mainSplitter;
  QSplitter *m_chatSplitter;

  QFrame *m_chatFrame;

  WbWidget *m_wbwidget;

  ChatOutput *m_chatoutput;
  QTextEdit *m_chatinput;

  QPushButton *m_sendmsg;

  QString m_jid;
  int m_type;
  QString m_nick;
  KAction *actionSelect;


  // TODO: TEST ONLY!
  QBuffer *m_testbuffer;
  Phonon::VideoPlayer *player;
  int bytes;
  QByteArray *ba;
  MediaPlayer *mediap;
  FTStream *m_ftstream;
  
  QWeakPointer<QGst::Ui::VideoWidget> _videoWidget;
  QWeakPointer<QGst::Ui::VideoWidget> _videoPreviewWidget;
};

#endif // SESSIONVIEW_H

