/*
 * sessionview.cpp
 *
 * Copyright (C) 2007 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#include "sessionview.h"
#include "mediaplayer.h"
#include "kiconloader.h"
#include "klocale.h"
#include "wbwidget.h"
#include "ktoolbar.h"
#include "kaction.h"
//#include "ftstream.h"
#include "maknetoview.h"
//#include "mucview.h"

#include <iostream>

#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QTextEdit>
#include <QtGui/QSplitter>
#include <QtXml/QDomElement>
#include <QtCore/QSignalMapper>
#include <QtCore/QBuffer>
#include <QtCore/QByteArray>
#include <QtGui/QFrame>
#include <QtCore/QDebug>
#include <QtGui/QMessageBox>
#include <QtCore/QSettings>
#include <QtGui/QGraphicsProxyWidget>
#include <QStringList>
#include <QToolButton>
#include <QGst/Ui/VideoWidget>

#include "sessiontabmanager.h"
#include "makneto.h"
#include "chatinput.h"
#include "chatoutput.h"
#include "palettewidget.h"
#include "plugins/pollplugin.h"
#include "wbforeign.h"

#include <Phonon/VideoPlayer>

//vtheman
#include "call-window.h"

SessionView::SessionView(QWidget *parent, MaknetoBackend::Session *session)
: QWidget(parent),
m_session(session),
m_videoWindow(NULL),
m_videoPreviewWindow(NULL) {
  qDebug() << "SessionView created";

  m_makneto = Makneto::Instance();
  m_topLayout = new QVBoxLayout(this);
  m_topSplitter = new QSplitter(this);
  m_topLayout->addWidget(m_topSplitter);
  m_topSplitter->setOrientation(Qt::Horizontal);
  m_leftWidget = new QWidget(this);

  m_leftLayout = new QVBoxLayout(m_leftWidget);
  m_leftWidget->setLayout(m_leftLayout);

  m_leftLayout->setMargin(0);
  m_leftLayout->setSpacing(0);

  // Add Toolbar to the layout
  createToolBar();
  QHBoxLayout *m_toolLayout = new QHBoxLayout();
  m_toolLayout->addWidget(m_wbtoolbar);

  // Create WhiteBoard widget
  m_wbwidget = new WbWidget("s1", session->getMyId(), QSize(300, 400), this);
  // Create palette widget
  m_paletteWidget = new PaletteWidget(this);

  connect(m_paletteWidget,
    SIGNAL(fgColorChanged(const QColor &)),
    SLOT(fgColorChanged(const QColor &)));

  connect(m_paletteWidget,
    SIGNAL(bgColorChanged(const QColor &)),
    SLOT(bgColorChanged(const QColor &)));

  connect(m_paletteWidget,
    SIGNAL(penSizeChanged(int)),
    SLOT(penSizeChanged(int)));

  m_paletteWidget->setFgColor(QColor(0, 0, 0));
  m_paletteWidget->setBgColor(Qt::transparent);
  m_paletteWidget->setPenSize(1);
  //end of palette setup

  m_toolLayout->addWidget(m_paletteWidget);

  //vtheman - add call button
  QToolButton *callButton = new QToolButton(this);
  callButton->setIcon(KIcon("makneto-call.png"));
  callButton->setIconSize(QSize(32, 32));
  callButton->setText("Call");
  callButton->setToolTip(QString("Call " + m_session->getName()));
  m_toolLayout->addWidget(callButton);
  connect(callButton, SIGNAL(clicked(bool)),
    this, SLOT(onCallRequested()));

  QToolButton *videoCallButton = new QToolButton(this);
  videoCallButton->setIcon(KIcon("makneto-video-call.png"));
  videoCallButton->setIconSize(QSize(32, 32));
  videoCallButton->setText("Video Call");
  videoCallButton->setToolTip(QString("Video Call " + m_session->getName()));
  m_toolLayout->addWidget(videoCallButton);
  connect(videoCallButton, SIGNAL(clicked(bool)),
    this, SLOT(onVideoCallRequested()));


  if (m_session->isMUC()) {
    callButton->setEnabled(false);
    videoCallButton->setEnabled(false);
  }
  //TODO set enabled only if the peer supports voice

  m_leftLayout->addLayout(m_toolLayout);

  m_leftSplitter = new QSplitter(Qt::Vertical, m_leftWidget);
  m_leftLayout->addWidget(m_leftSplitter);

  m_leftSplitter->addWidget(m_wbwidget);

  connect(m_wbwidget,
    SIGNAL(newWb(QDomElement)),
    SLOT(sendWhiteboard(QDomElement))); //TODO look into this

  connect(m_wbwidget,
    SIGNAL(modeChanged(WbWidget::Mode)),
    SLOT(modeChanged(WbWidget::Mode)));

  m_wbwidget->setMode(WbWidget::DrawPath);
  m_wbwidget->setMinimumSize(300, 400);

  m_chatSplitter = new QSplitter(Qt::Vertical, m_leftSplitter);

  m_leftSplitter->addWidget(m_chatSplitter);

  m_chatoutput = new ChatOutput(m_chatSplitter);

  m_chatinput = new ChatInput(m_chatSplitter);
  m_sendmsg = new QPushButton("&Send", m_leftWidget);
  connect(m_sendmsg, SIGNAL(clicked()), this, SLOT(sendClicked()));
  connect(m_chatinput, SIGNAL(send()), this, SLOT(sendClicked()));

  // output chat text edit props
  m_chatoutput->setReadOnly(true);
  //	m_chatoutput->setNick(m_session->getName());

  m_chatSplitter->addWidget(m_chatoutput);
  m_chatSplitter->addWidget(m_chatinput);

  m_leftLayout->addWidget(m_sendmsg);

  m_topSplitter->addWidget(m_leftWidget);

  // TODO: test only!!!
  ba = new QByteArray;
  m_testbuffer = new QBuffer(ba, this);
  m_testbuffer->open(QIODevice::ReadWrite);

  /*
  connect(m_session, SIGNAL(textChatReady(QStringList*)), //deprecated, get rid of
    this, SLOT(onTextChatReady(QStringList*)));
   */
  connect(m_session, SIGNAL(messageReceived(const QString &, const QString &)),
    this, SLOT(onMessageReceived(const QString &, const QString &)));
  connect(m_session, SIGNAL(incomingCall()),
    this, SLOT(onIncomingCall()));
  connect(m_session, SIGNAL(callReady()),
    this, SLOT(onCallReady()));
  connect(m_session, SIGNAL(incomingFileTransfer(QString)),
    this, SLOT(onIncomingFileTransfer(QString)));

  connect(m_session, SIGNAL(videoPreviewAvailable(QGst::ElementPtr)),
    this, SLOT(onVideoPreviewAvailable(QGst::ElementPtr)));
  connect(m_session, SIGNAL(videoAvailable(QGst::ElementPtr)),
    this, SLOT(onVideoAvailable(QGst::ElementPtr)));
  connect(m_session, SIGNAL(videoPreviewRemoved(QGst::ElementPtr)),
    this, SLOT(onVideoPreviewRemoved(QGst::ElementPtr)));
  connect(m_session, SIGNAL(videoRemoved(QGst::ElementPtr)),
    this, SLOT(onVideoRemoved(QGst::ElementPtr)));
}



//IS OK

SessionView::~SessionView() {
  m_session->deleteLater();
  QSettings settings;
  settings.setValue("m_topSplitter", m_topSplitter->saveState());
}


//IS OK

void SessionView::createToolBar() {
  // wb toolbar
  m_wbtoolbar = new KToolBar(this);
  m_wbtoolbar->setIconDimensions(16);
  m_wbtoolbar->setToolButtonStyle(Qt::ToolButtonIconOnly);

  QActionGroup *groupMode = new QActionGroup(this);
  connect(groupMode, SIGNAL(triggered(QAction*)), this, SLOT(setMode(QAction*)));

  actionSelect = new KAction(KIcon("select-rectangular"), i18n("Selection"), groupMode);
  m_wbtoolbar->addAction(actionSelect);
  actionSelect->setData(QVariant(WbWidget::Select));

  KAction *actionTransformMove = new KAction(KIcon("transform-move"), i18n("Transform - move"), groupMode);
  m_wbtoolbar->addAction(actionTransformMove);
  actionTransformMove->setData(QVariant(WbWidget::Translate));

  KAction *actionRotate = new KAction(KIcon("transform-rotate"), i18n("Transform - rotate"), groupMode);
  m_wbtoolbar->addAction(actionRotate);
  actionRotate->setData(QVariant(WbWidget::Rotate));

  KAction *actionScale = new KAction(KIcon("zoom-fit-best"), i18n("Transform - scale"), groupMode);
  m_wbtoolbar->addAction(actionScale);
  actionScale->setData(QVariant(WbWidget::Scale));

  KAction *actionErase = new KAction(KIcon("draw-eraser"), i18n("Erase"), groupMode);
  m_wbtoolbar->addAction(actionErase);
  actionErase->setData(QVariant(WbWidget::Erase));

  KAction *actionPencil = new KAction(KIcon("draw-freehand"), i18n("Freehand"), groupMode);
  m_wbtoolbar->addAction(actionPencil);
  actionPencil->setData(QVariant(WbWidget::DrawPath));

  KAction *actionLine = new KAction(KIcon("draw-line"), i18n("Line"), groupMode);
  m_wbtoolbar->addAction(actionLine);
  actionLine->setData(QVariant(WbWidget::DrawLine));

  KAction *actionRectangle = new KAction(KIcon("draw-rectangle"), i18n("Rectangle"), groupMode);
  m_wbtoolbar->addAction(actionRectangle);
  actionRectangle->setData(QVariant(WbWidget::DrawRectangle));

  KAction *actionEllipse = new KAction(KIcon("draw-ellipse"), i18n("Ellipse"), groupMode);
  m_wbtoolbar->addAction(actionEllipse);
  actionEllipse->setData(QVariant(WbWidget::DrawEllipse));

  KAction *actionCircle = new KAction(KIcon("draw-circle"), i18n("Circle"), groupMode);
  m_wbtoolbar->addAction(actionCircle);
  actionCircle->setData(QVariant(WbWidget::DrawCircle));

  // 	KAction *actionPolyline = new KAction(KIcon("draw-polyline"), i18n("Polyline"), groupMode);
  // 	m_wbtoolbar->addAction(actionPolyline);
  // 	actionPolyline->setData(QVariant(WbWidget::DrawPolyline));
  // 
  KAction *actionText = new KAction(KIcon("insert-text"), i18n("Text"), groupMode);
  m_wbtoolbar->addAction(actionText);
  actionText->setData(QVariant(WbWidget::DrawText));
  actionText->setCheckable(true);

  KAction *actionImage = new KAction(KIcon("insert-image"), i18n("Image"), groupMode);
  m_wbtoolbar->addAction(actionImage);
  actionImage->setData(QVariant(WbWidget::DrawImage));

  KAction *actionSendFile = new KAction(KIcon("mail-send"), i18n("Send file"), this);
  m_wbtoolbar->addAction(actionSendFile);
  connect(actionSendFile, SIGNAL(triggered()), this, SLOT(actionSendFileTriggered()));

  KAction *actionPoll = new KAction(KIcon("maknetopoll"), i18n("Create poll"), this);
  m_wbtoolbar->addAction(actionPoll);
  connect(actionPoll, SIGNAL(triggered()), this, SLOT(actionCreatePollTriggered()));
}






//VTHEMAN

void SessionView::sendClicked() {
  QString text = m_chatinput->toPlainText();
  if (text == QString())
    return;

  //TODO figure out how to treat MUC
  //can be used as a slot
  m_session->sendMessage(text);

  //TODO Show message in chat window only in chat, not in groupchat
  m_chatoutput->myMessage(text);
  m_chatinput->setText("");
}


//will be deleted soon

/*
void SessionView::onTextChatReady(QStringList* messages) {
  qDebug() << "SessionView: Text chat is ready";

  foreach(QString message, *messages) {
    //m_chatoutput->incomingMessage(message);
  }
}
 */
void SessionView::onMessageReceived(const QString& text, const QString &contact) {
  QDomDocument doc;
  doc.setContent(text);
  //QDomElement e = doc.elementsByTagNameNS("http://jabber.org/protocol/svgwb", "wb").item(0).toElement();
  QDomElement e = doc.elementsByTagName("wb").item(0).toElement();
  if (!e.isNull())
    m_wbwidget->processWb(e);
  else {
    //TODO figure out what to do in case of MUC
    m_chatoutput->incomingMessage(text, contact);
  }
}


//TODO set resource Makneto and disable the wbwidget if peer does not have makneto

void SessionView::sendWhiteboard(const QDomElement &wb) {
  QString message;

  QTextStream ts(&message);
  wb.save(ts, 0);
  //qDebug() << "wb:" << endl << message << endl << endl;
  //TODO check the resource
  m_session->sendMessage(ts.readAll());
}

//this is the same and onCallReady and essentially might be blended into
//one. The session object offers two signals, one for when the user with
//an existing session requests an audio call, and second for when the peer
//requests a call and in GUI there might popup a dialog asking if the user
//wishes to accept the call.

void SessionView::onIncomingCall() {
  qDebug() << "SessionView: Incoming call";
  if (acceptCallQuery()) {
    m_callWindow = new CallWindow(m_session, this);
    m_callWindow->show();
  } else {
    m_session->rejectCall();
  }
}

void SessionView::onCallReady() {
  qDebug() << "SessionView: Call Ready";
  m_callWindow = new CallWindow(m_session, this);
  m_callWindow->show();
}

void SessionView::onCallRequested() {
  qDebug() << "SessionView: Call requested";
  m_session->startMediaCall(false);
}

void SessionView::onVideoCallRequested() {
  qDebug() << "SessionView: Video Call requested";
  m_session->startMediaCall(true);
}

bool SessionView::acceptCallQuery() {
  // FIXME: SessionType flags is not setup yet!
  QString type = (m_session->getSessionType().testFlag(MaknetoBackend::Session::SessionTypeVideo) ? "Video" : "Audio");
  qDebug() << "SessionView: query for " + type + " call";
  if (QMessageBox::question(this, "Makneto", QString("Incoming " + type + " Call from " + m_session->getName() + ".\nAccept?"),
    QMessageBox::Yes | QMessageBox::No)
    == QMessageBox::Yes) {
    return true;
  } else
    return false;

  return true;
}

void SessionView::onVideoPreviewAvailable(QGst::ElementPtr videoOutElemPtr) {
  _videoPreviewWidget = new QGst::Ui::VideoWidget;
  _videoPreviewWidget.data()->setVideoSink(videoOutElemPtr);

  if (m_videoPreviewWindow) {
    qWarning() << "video preview window is already opened!";
    return;
  }

  m_videoPreviewWindow = new QMainWindow();
  m_videoPreviewWindow->setWindowTitle("You");
  m_videoPreviewWindow->setCentralWidget(_videoPreviewWidget.data());
  m_videoPreviewWindow->show();
}

void SessionView::onVideoAvailable(QGst::ElementPtr videoOutElemPtr) {

  _videoWidget = new QGst::Ui::VideoWidget;
  _videoWidget.data()->setVideoSink(videoOutElemPtr);

  if (m_videoWindow) {
    qWarning() << "video window is already opened!";
    return;
  }

  m_videoWindow = new QMainWindow();
  m_videoWindow->setWindowTitle("Video of " + sessionId());
  m_videoWindow->setCentralWidget(_videoWidget.data());
  m_videoWindow->show();
}

void SessionView::onVideoPreviewRemoved(QGst::ElementPtr videoOutElemPtr) {
  if (!m_videoPreviewWindow)
    return;

  m_videoPreviewWindow->hide();
  m_videoPreviewWindow->deleteLater();
  m_videoPreviewWindow = NULL;
  _videoPreviewWidget.clear();
}

void SessionView::onVideoRemoved(QGst::ElementPtr videoOutElemPtr) {
  if (!m_videoWindow)
    return;

  m_videoWindow->hide();
  m_videoWindow->deleteLater();
  m_videoWindow = NULL;
  _videoWidget.clear();
}

//FIXME This has to be adapted to Tp!!!!

/*
void SessionView::fileTransfer(FileTransfer *ft)
{
  FileTransfer *transfer = ft;

  QString text;

  text = "<b>" + transfer->peer().bare()+"</b> Incoming file '" + transfer->fileName() +"'";

  m_chatoutput->incomingMessage(text);

  connect(transfer, SIGNAL(readyRead(const QByteArray &)), this, SLOT(transferRead(const QByteArray &)));

  transfer->accept();

  m_ftstream = new FTStream(m_testbuffer, transfer->fileSize(), this);

  mediap = new MediaPlayer(this);
  mediap->show();
  mediap->setCurrentSource(m_ftstream);
	
  //mediap->setBuffer(m_testbuffer);

  bytes = 0;
  //player->load(m_testbuffer);
  //player->play();
}
 */



void SessionView::infoMessage(const QString &text) {
  m_chatoutput->infoMessage(text);
}

//IS OK

void SessionView::showHideChat() {
  QList<int> l = m_leftSplitter->sizes();
  if (l[1] == 0) {
    QSettings settings;
    l[1] = settings.value("m_leftSplitter_size").toInt();
  } else {
    QSettings settings;
    settings.setValue("m_leftSplitter_size", l[1]);
    l[1] = 0;
  }
  m_leftSplitter->setSizes(l);
}

bool SessionView::closeRequest() {
  //TODO create const char* for the message
  if (QMessageBox::question(this, "Makneto",
    tr("Do you really want to close the tab?"),
    QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
    //FIXME m_session->leaveGroupChat
    return true;
  } else {
    return false;
  }
  return true;
}

void SessionView::modeChanged(WbWidget::Mode mode) {
  switch (mode) {
    case WbWidget::Select:
      actionSelect->setChecked(true);
      break;
    case WbWidget::DrawText:
      //actionDrawText->setChecked(true);
      break;
    default:
      break;
  }
}


//FIXME adapt to Tp
/*
void SessionView::transferRead(const QByteArray &a)
{
  ba->append(a);
//	std::cout << "a.size=" << a.size() << std::endl;
  bytes=bytes+a.size();

//	std::cout << "written=" << m_testbuffer->write(a.data()) << std::endl;
//	m_testbuffer->waitForBytesWritten(1000);
 //	std::cout << "buffer.size()=" << m_testbuffer->size() << std::endl;
// 	std::cout << "bytes=" << bytes << std::endl;
// 	std::cout << "ba.size()=" << ba->size() << std::endl;

// 	if (bytes==183173120)
// 	{
// 		m_testbuffer->open(QIODevice::ReadOnly);
// 		//m_testbuffer->seek(0);
// 		mediap->setCurrentSource(m_testbuffer);
// 		mediap->show();
// 	}

  if (bytes==4456448)
    mediap->setCurrentSource(m_ftstream);

}
 */


//TODO if there is time, implement

void SessionView::actionSendFileTriggered() {
  QString fileName = QFileDialog::getOpenFileName(this, tr("Choose file to send"));
  qDebug() << "SessionView: File to send: " << fileName;
  m_session->startFileTransfer(fileName);
}

void SessionView::onIncomingFileTransfer(const QString& filename) {
  if (QMessageBox::question(this,
    "Makneto",
    QString("Incoming file transfer from " + m_session->getName() + ".\nFile: " + filename + "\nAccept?"),
    QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
    QString localFileName = QFileDialog::getSaveFileName(this, tr("Save file"));
    m_session->onAcceptFileTransfer(true, localFileName);
  } else {
    m_session->onAcceptFileTransfer(false);
  }

}



//IS OK

void SessionView::setEnabled(bool enabled) {
  m_sendmsg->setEnabled(enabled);
}


//IS OK

void SessionView::setMode(QAction *action) {
  m_wbwidget->setMode(WbWidget::Mode(action->data().toInt()));
  // If user selects freehand pen, we should disable the brush
  if (WbWidget::Mode(action->data().toInt()) == WbWidget::DrawPath)
    m_wbwidget->setFillColor(Qt::transparent);
  else
    m_wbwidget->setFillColor(m_paletteWidget->bgColor());
}


//IS OK

void SessionView::fgColorChanged(const QColor &c) {
  m_wbwidget->setStrokeColor(c);
}


//IS OK

void SessionView::bgColorChanged(const QColor &c) {
  m_wbwidget->setFillColor(c);
}

//IS OK

void SessionView::penSizeChanged(int size) {
  m_wbwidget->setStrokeWidth(size);
}


//Seems to be ok

void SessionView::actionCreatePollTriggered() {
  QDomElement dom;
  PollPlugin *plugin = new PollPlugin();
  if (!plugin->getQuestions())
    return;

  WbForeign *wbf = new WbForeign(plugin, dom, m_wbwidget->scene->newId(), m_wbwidget->scene->newIndex(), QString("root"), static_cast<QGraphicsScene *> (m_wbwidget->scene));
  //scene->update(wbf->graphicsItem()->boundingRect());
  m_wbwidget->scene->queueNew(wbf->id(), wbf->index(), wbf->svg());
  m_wbwidget->scene->addWbItem(wbf);
  plugin->graphicsItem()->ensureVisible(QRectF(), 0, 0);
  setMode(actionSelect);
  actionSelect->setChecked(true);
}
