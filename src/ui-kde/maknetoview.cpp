/*
 * maknetoview.cpp
 *
 * Copyright (C) 2007 Jaroslav Reznik <rezzabuh@gmail.com>
 */
#include "maknetoview.h"
#include "sidebarwidget.h"
//#include "connectionview.h"
//#include "roasterview.h"
#include "sessionview.h"
#include "sessiontabmanager.h"
#include "kiconloader.h"
#include "klocale.h"
#include "makneto.h"
//#include "maknetocontactlist.h"
//#include "mucview.h"

#include <klocale.h>
#include <QtGui/QLabel>
#include <QtGui/QHBoxLayout>
#include <kmultitabbar.h>
#include <QtGui/QSplitter>

//vtheman
#include "../backend/telepathy-client.h"
#include "roster-widget.h"

MaknetoView::MaknetoView(QWidget *, Makneto *makneto)
{

	QHBoxLayout *layout = new QHBoxLayout;

	// prepare sidebar
	m_sidebar = new SidebarWidget(this);

//	m_muc = new MUCView(this, makneto);
  
	//vtheman 
	RosterWidget *rosterWidget = new RosterWidget(this);
	connect(makneto,
			SIGNAL(clientInitialised(MaknetoBackend::TelepathyClient *)),
			rosterWidget, 
			SLOT(onInitializationFinished(MaknetoBackend::TelepathyClient *)));
	
//	m_sidebar->appendTabWidget(m_muc, KIconLoader::global()->loadIcon("goto", KIconLoader::Toolbar, KIconLoader::SizeSmall), 2, i18n("MUC"));
	
	m_sidebar->appendTabWidget(rosterWidget, KIconLoader::global()->loadIcon("goto", KIconLoader::Toolbar, KIconLoader::SizeSmall), 3, i18n("Contact list"));

	
	m_sidebar->setCurrentIndex(0);
	m_sidebar->setMaximumWidth(300); 

	// session manager
	m_sessiontabmanager = new SessionTabManager(makneto, this);

	// add to layout
	layout->addWidget(m_sidebar);
	layout->addWidget(m_sessiontabmanager);

	setLayout(layout);
}

MaknetoView::~MaknetoView()
{

}

void MaknetoView::settingsChanged()
{

}

#include "maknetoview.moc"
