/*
 * sessiontabmanager.h
 *
 * Copyright (C) 2008 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#ifndef SESSIONTABMANAGER_H
#define SESSIONTABMANAGER_H

#include "makneto.h"
#include "../backend/session.h"

#include <QtGui/QWidget>

class QStackedWidget;
class QVBoxLayout;
class KTabBar;
class SessionView;

/**
 * This is session tab manager widget for Makneto
 *
 * @short Session tab manager widget
 * @author Jaroslav Reznik <rezzabuh@gmail.com>
 * @version 0.1
 */

class SessionTabManager : public QWidget
{
	Q_OBJECT
	
public:
	/**
	* Default constructor
	*/
	SessionTabManager(Makneto *makneto, QWidget *parent);

	/**
	* Destructor
	*/
	virtual ~SessionTabManager();

//	SessionView *newSessionTab(const QString &text, ChatType type, const QString &nick = QString());
	SessionView *findSession(const QString &jid);
	void bringToFront(SessionView *session);
	Makneto *makneto() { return m_makneto; }
	SessionView *activeSession();

	//vtheman
	//SessionView* newSessionTab(MaknetoLib::Session *);
	
Q_SIGNALS:
	void newSessionRequested(const QModelIndex &, MaknetoBackend::Session::SessionType);
	void callRequested(const QModelIndex &);
	
public Q_SLOTS:
	void closeTab(int tabIndex);
	
	//vtheman
	void onTextChatRequested(const QModelIndex &);
	void onCallRequested(const QModelIndex &);
	void onNewSession(MaknetoBackend::Session *, bool, bool);
	
private:
	QVBoxLayout *m_mainlayout;

	KTabBar *m_tab;
	QStackedWidget *m_widgets;
	Makneto *m_makneto;
};

#endif // SESSIONTABMANAGER_H

