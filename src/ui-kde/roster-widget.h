/*
 * This file is part of TelepathyQt4
 *
 * Copyright (C) 2010 Collabora Ltd. <http://www.collabora.co.uk/>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MAKNETO_ROSTER_WIDGET
#define MAKNETO_ROSTER_WIDGET

#include <QWidget>
#include <TelepathyQt4/Types>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/PendingReady>
#include <TelepathyQt4/AccountSet>

class QTreeView;
class TelepathyInitializer;
class QHBoxLayout;
class QPoint;
class QModelIndex;
class QAction;

namespace MaknetoBackend {
  class TelepathyClient;
}

class RosterWidget : public QWidget {
  Q_OBJECT

public:

  RosterWidget(QWidget *parent = 0);
private:
  void handleConnectionError(const Tp::AccountPtr &account, Tp::ConnectionStatusReason reason);
  
  public 
Q_SLOTS:
  void onInitializationFinished(MaknetoBackend::TelepathyClient *client);
  void onStartTextChat();
  void onStartAudioChat();
  void onStartVideoChat();
  void onCustomContextMenuRequested(QPoint);
  void onChangePresence(QAction *);
  void onAccountStatusChanged(const Tp::AccountPtr &account, Tp::ConnectionStatus status);

Q_SIGNALS:
  void textChatRequested(QModelIndex);

private:
  QAction *m_actionMUC;
  QHBoxLayout *m_hlayout;
  //QDeclarativeView *m_view;
  QTreeView *m_view;

};

#endif // MAKNETO_ROSTER_WIDGET

