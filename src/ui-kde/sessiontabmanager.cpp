/*
 * sessiontabmanager.cpp
 *
 * Copyright (C) 2008 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#include "sessiontabmanager.h"
#include "sessionview.h"
#include "maknetohtmlbrowser.h"

#include "ktabbar.h"
#include "kiconloader.h"


#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <QtGui/QStackedWidget>
#include <QtGui/QFrame>
#include <QDebug>

//vtheman
#include "../backend/telepathy-client.h"

SessionTabManager::SessionTabManager(Makneto *makneto, QWidget *) : m_makneto(makneto) {
  m_mainlayout = new QVBoxLayout(this);
  m_mainlayout->setMargin(5);
  m_mainlayout->setSpacing(5);

  m_tab = new KTabBar(this);
  m_tab->setHoverCloseButton(true);

  m_widgets = new QStackedWidget();

  m_mainlayout->addWidget(m_tab);
  m_mainlayout->addWidget(m_widgets);

  // connect tab bar with widgets
  connect(m_tab, SIGNAL(currentChanged(int)), m_widgets, SLOT(setCurrentIndex(int)));

  // Close tab on request
  connect(m_tab, SIGNAL(closeRequest(int)), this, SLOT(closeTab(int)));

  // TODO: and status (from contact list?)

  // KHTMLpart widget for "Home tab"
  MaknetoHTMLBrowser *homeTabBrowser = new MaknetoHTMLBrowser(this);

  m_tab->addTab("Home");
  m_widgets->addWidget(homeTabBrowser);

  setLayout(m_mainlayout);
}

SessionTabManager::~SessionTabManager() {

}



//
// SessionView* SessionTabManager::newSessionTab(const QString &text, ChatType type, const QString &nick)
// {
// 	// There shouldn't be the same tab twice, so we won't create it
// 	SessionView *session = findSession(text);
// 	if (session)
// 		return session;
// 
// 	// add new tab
// 	m_tab->addTab(text);
// 	
// 	// create new session view and add to widgets
// 	session = new SessionView(this, text, m_tab->count()-1, type, nick);
// 	m_widgets->addWidget(session);
// 
// 	connect(session, SIGNAL(sendMessage(const Message &)), m_makneto->getConnection(), SLOT(sendMessage(const Message &)));
// 
// 	return session;
// }




/**
 *	@param 
 *	@return session
 */
//FIXME - possibly can be left as is

SessionView* SessionTabManager::findSession(const QString &sessionId) {
  SessionView *sessionView;

  // From 1!!! First widget is home tab! 
  for (int i = 1; i < m_widgets->count(); i++) {
    sessionView = dynamic_cast<SessionView*> (m_widgets->widget(i));

    if (sessionView->sessionId() == sessionId)
      return sessionView;
  }

  return 0;
}

//IS OK

void SessionTabManager::bringToFront(SessionView *session) {
  qDebug() << "SessionTabManager: BringToFront(" << session->sessionId() << ")";

  SessionView *sessionView;
  for (int i = 1; i < m_widgets->count(); i++) {
    sessionView = dynamic_cast<SessionView*> (m_widgets->widget(i));

    if (sessionView->sessionId() == session->sessionId())
      m_tab->setCurrentIndex(i);
  }
}

//IS OK

SessionView *SessionTabManager::activeSession() {
  return dynamic_cast<SessionView *> (m_widgets->currentWidget());
}

//IS OK

void SessionTabManager::closeTab(int tabIndex) {
  qDebug() << "SessionTabManager: Close tab";
  // If closing tab has class SessionView, closeRequest method will be called
  if (QString(m_widgets->widget(tabIndex)->metaObject()->className()).compare("SessionView") != 0
    || dynamic_cast<SessionView *> (m_widgets->widget(tabIndex))->closeRequest()) {
    m_widgets->removeWidget(m_widgets->widget(tabIndex));
    m_tab->removeTab(tabIndex);
  }
}

/*
SessionView* SessionTabManager::newSessionTab(MaknetoLib::Session *session)
{
  // There shouldn't be the same tab twice, so we won't create it
  SessionView *sessionView = findSession(session->getUniqueName());
  if (sessionView)
    return sessionView;

  // add new tab
  m_tab->addTab(session->getUniqueName());
  // create new session view and add to widgets
  sessionView = new SessionView(this, session, m_tab->count()-1);
  m_widgets->addWidget(sessionView);

  //most likely will be elsewhere
  //connect(sessionView, SIGNAL(sendMessage(const Message &)), m_makneto->getConnection(), SLOT(sendMessage(const Message &)));

  return sessionView;
}
 */

//vtheman
//The following two methods request new Session from TelepathyClient, specifing the type using session flags 

void SessionTabManager::onTextChatRequested(const QModelIndex &contactIndex) {

  //TODO find session, if it exist do not create it and make the tab active
  SessionView *sessionView = findSession(MaknetoBackend::TelepathyClient::Instance()->sessionIdforIndex(contactIndex));
  if (sessionView) {
    bringToFront(sessionView);
    return;
  }
  qDebug() << "SessionTabManager: New text chat requested";
  emit newSessionRequested(contactIndex, MaknetoBackend::Session::SessionTypeText);
}

//TODO figure out if this should be done from contactlist context menu

void SessionTabManager::onCallRequested(const QModelIndex &contactIndex) {
  SessionView *sessionView = findSession(MaknetoBackend::TelepathyClient::Instance()->sessionIdforIndex(contactIndex));
  if (sessionView) {
    bringToFront(sessionView);
    return;
  }
  qDebug() << "SessionTabManager: New audio call requested";
  emit newSessionRequested(contactIndex, MaknetoBackend::Session::SessionTypeAudio);
}

void SessionTabManager::onNewSession(MaknetoBackend::Session* session, bool upgradeExisting, bool requested) {
  SessionView *sessionView = findSession(session->getUniqueName());
  if (sessionView) {
    bringToFront(sessionView);
    return;
  }
  // add new tab
  m_tab->addTab(session->getName());
  // create new session view and add to widgets
  sessionView = new SessionView(this, session);
  m_widgets->addWidget(sessionView);
  bringToFront(sessionView);

  qDebug() << "SessionTabManager: New session of name: " << session->getUniqueName() << " has been created.";
}


