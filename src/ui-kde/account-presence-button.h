#ifndef MAKNETO_ACCOUNT_PRESENCE_BUTTON
#define MAKNETO_ACCOUNT_PRESENCE_BUTTON


#include <TelepathyQt4/Account>
#include <TelepathyQt4/PendingOperation>

#include <QToolButton>
#include <QAction>

class AccountPresenceButton : public QToolButton 
{
	Q_OBJECT
	
	public:
		AccountPresenceButton(Tp::AccountPtr, QWidget *parent=0);
		~AccountPresenceButton(){}
	
		QString accountId();
	
	public Q_SLOTS:
		void setAccountStatus(QAction *action);
		void presenceChanged(const Tp::Presence &presence);
		
	private:
		Tp::AccountPtr m_account;
	
};

#endif //MAKNETO_ACCOUNT_PRESENCE_BUTTON