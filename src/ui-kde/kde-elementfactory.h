/* 
 * File:   KdeElementFactory.h
 * Author: karry
 *
 * Created on 30. říjen 2011, 17:37
 */

#ifndef KDEELEMENTFACTORY_H
#define	KDEELEMENTFACTORY_H

#include <QtCore/QObject>
#include "../backend/voip/element-factory/defaultelementfactory.h"
#include "phononintegration_p.h"

class KdeElementFactory : public MaknetoBackend::DefaultElementFactory {

  Q_OBJECT
public:
  inline KdeElementFactory() {};

  inline ~KdeElementFactory() {
  };
  virtual QGst::ElementPtr makeAudioCaptureElement();
  virtual QGst::ElementPtr makeAudioOutputElement();
  /*
  virtual QGst::ElementPtr makeVideoCaptureElement();
protected:
  virtual QGst::ElementPtr tryFileElement(const char *name, const QString & file = QString());
  virtual QGst::ElementPtr tryElement(const char *name, const QString & device = QString());
   */
  virtual QGst::ElementPtr tryOverrideForKey(const char *keyName);
};

#endif	/* KDEELEMENTFACTORY_H */

