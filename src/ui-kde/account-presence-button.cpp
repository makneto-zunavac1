#include <QIcon>

#include <TelepathyQt4/Account>
#include <TelepathyQt4/PendingOperation>

#include "account-presence-button.h"

AccountPresenceButton::AccountPresenceButton(Tp::AccountPtr account, QWidget* parent)
	: 	QToolButton(parent),
		m_account(account)
{
	QString iconPath = account->iconName();
	qDebug() << "ICON NAME: " << iconPath;
	setIcon(QIcon(iconPath));
	
	setAutoRaise(true);
	setPopupMode(QToolButton::InstantPopup);
	
	QActionGroup *presenceActions = new QActionGroup(this);
	presenceActions->setExclusive(true);
	
	QAction *availableAction = 	new QAction(QString("available"), this);
	QAction *awayAction =		new QAction(QString("away"),this);
	QAction *dndAction =		new QAction(QString("DND"),this);
	QAction *invisibleAction =	new QAction(QString("invisible"),this);
	QAction *offlineAction =	new QAction(QString("offline"),this);
	
	availableAction->setData(qVariantFromValue(Tp::Presence::available()));
    awayAction->setData(qVariantFromValue(Tp::Presence::away()));
    dndAction->setData(qVariantFromValue(Tp::Presence::busy()));
    invisibleAction->setData(qVariantFromValue(Tp::Presence::hidden()));
    offlineAction->setData(qVariantFromValue(Tp::Presence::offline()));
	
	presenceActions->addAction(availableAction);
	presenceActions->addAction(awayAction);
	presenceActions->addAction(dndAction);
	presenceActions->addAction(invisibleAction);
	presenceActions->addAction(offlineAction);
	
	addActions(presenceActions->actions());
	
	foreach (QAction *action, actions()) {
        action->setCheckable(true);

        if (m_account->currentPresence().status() == qVariantValue<Tp::Presence>(action->data()).status()) {
            action->setChecked(true);
        }
    }
    
    connect(this, SIGNAL(triggered(QAction*)),
            this, SLOT(setAccountStatus(QAction*)));
	connect(m_account.data(), SIGNAL(currentPresenceChanged(const Tp::Presence &)),
            this, SLOT(presenceChanged(const Tp::Presence &)));
    
}

QString AccountPresenceButton::accountId()
{
    return m_account->uniqueIdentifier();
}


void AccountPresenceButton::setAccountStatus(QAction *action)
{
    Tp::SimplePresence presence;
    presence.type = qVariantValue<Tp::Presence>(action->data()).type();
  //  presence.status = qVariantValue<Tp::Presence>(action->data()).status();

    Q_ASSERT(!m_account.isNull());

    Tp::PendingOperation* presenceRequest = m_account->setRequestedPresence(presence);

    connect(presenceRequest, SIGNAL(finished(Tp::PendingOperation*)),
            this, SLOT(updateToolTip()));

}

void AccountPresenceButton::presenceChanged(const Tp::Presence &presence)
{
    foreach (QAction *action, actions()) {
        if (presence.status() == qVariantValue<Tp::Presence>(action->data()).status()) {
            action->setChecked(true);
            break;
        }
    }
}


