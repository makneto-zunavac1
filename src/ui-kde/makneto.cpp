/*
 * makneto.cpp
 *
 * Copyright (C) 2008 Jaroslav Reznik <rezzabuh@gmail.com>
 */

#include "makneto.h"

#include <QObject>

//Vtheman
#include "../backend/telepathy-initializer.h"
#include "../backend/telepathy-client.h"
#include "../backend/session.h"
#include "../backend/voip/element-factory/defaultelementfactory.h"
#include "../backend/voip/element-factory/deviceelementfactory.h"

#include "sessiontabmanager.h"
#include "maknetoview.h"
#include "kde-elementfactory.h"


Makneto* Makneto::m_instance = NULL;

Makneto* Makneto::Instance()
{
	if(!m_instance)
		m_instance = new Makneto;
	
	return m_instance;
}


Makneto::Makneto(QObject *parent) : QObject(parent)
{

	MaknetoBackend::DeviceElementFactory::installFactory(new KdeElementFactory());
  
	MaknetoBackend::TelepathyInitializer *tpInit = new MaknetoBackend::TelepathyInitializer();
	
	connect(tpInit, 
			SIGNAL(finished(MaknetoBackend::TelepathyClient *)), 
			SLOT(onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient *)));
	
	tpInit->createClient("MaknetoKDE");
	
}

Makneto::~Makneto()
{
  MaknetoBackend::DeviceElementFactory::destroyAllFactories();
}

//vtheman
void Makneto::onTelepathyInitializerFinished(MaknetoBackend::TelepathyClient* client)
{	
	m_client = client;
	
	//connect SessionTabManager with TelepathyClient for session creation
	connect(m_mainwindow->getMaknetoView()->getSessionTabManager(),
			SIGNAL(newSessionRequested(QModelIndex, MaknetoBackend::Session::SessionType)),
			MaknetoBackend::TelepathyClient::Instance(),
			SLOT(onSessionRequested(QModelIndex, MaknetoBackend::Session::SessionType)));
	
	connect(MaknetoBackend::TelepathyClient::Instance(),
			SIGNAL(sessionCreated(MaknetoBackend::Session *, bool, bool)),
			m_mainwindow->getMaknetoView()->getSessionTabManager(),
			SLOT(onNewSession(MaknetoBackend::Session *, bool, bool)));
	
	qDebug() << "Makneto: Telepathy has been initialised";
	emit clientInitialised(m_client);
}

