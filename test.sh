#!/bin/bash

rm -rf mybuild
mkdir mybuild
cd mybuild
# export QMAKESPEC=linux-g++
#cmake -DCMAKE_BUILD_TYPE=debugfull -DCMAKE_INSTALL_PREFIX=/usr/ -DCMAKE_CXX_FLAGS=-pg ..
cmake -DCMAKE_BUILD_TYPE=debugfull -DCMAKE_INSTALL_PREFIX=/usr/ ..
make -j 4
#make
## for proffiling: we need huge stack for QtWebKit...
#  valgrind --main-stacksize=2000000000 --tool=callgrind  ./src/ui-mobile/makneto-mobile 
# kcachegrind callgrind.out.*

